var gulp       = require('gulp');
var less       = require('gulp-less');

gulp.task('default', function() {
  gulp.src(['src/main/resources/static/assets/css/less/app.less'])
    .pipe(less())
    .pipe(gulp.dest('src/main/resources/static/assets/css/'));
});