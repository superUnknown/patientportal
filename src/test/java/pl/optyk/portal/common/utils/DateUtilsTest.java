package pl.optyk.portal.common.utils;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testy narządzia do przetwarzania daty.
 * Created by superunknown on 11.11.2016.
 */
public class DateUtilsTest {

    @Test
    public void withoutTime() throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");


        assertThat(DateUtils.withoutTime(formatter.parse("23.10.2015 17:54:12"))).isEqualTo(formatter.parse("23.10.2015 00:00:00"));
        assertThat(DateUtils.withoutTime(formatter.parse("14.05.2015 11:00:12"))).isEqualTo(formatter.parse("14.05.2015 00:00:00"));

    }

}