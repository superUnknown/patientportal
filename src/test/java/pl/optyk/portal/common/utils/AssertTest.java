package pl.optyk.portal.common.utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * Testy dodatkowych asserów.
 *
 * Created by superunknown on 07.11.2016.
 */
public class AssertTest {

    @Test
    public void isNullOrEmpty() throws Exception {

        try {
            Assert.isNullOrEmpty("", "empty");
            assertThat(false).isTrue();
        } catch (IllegalArgumentException e) {
            assertThat(true).isTrue();
        }

        try {
            Assert.isNullOrEmpty(null, "empty");
            assertThat(false).isTrue();
        } catch (IllegalArgumentException e) {
            assertThat(true).isTrue();
        }

        Assert.isNullOrEmpty("fsdf", "not_empty");

    }

    @Test
    public void valueNotMultipledByOrNotInRange() throws Exception {
        Assert.valueNotMultipledByOrNotInRange(NumberUtils.round(30.000000000000007), 0.1,  20.0,  40.0, "Poza zakresem.");
//        try {
//            Assert.valueNotMultipledByOrNotInRange(9.5, 0.1, 4.5, 9.4, "Poza zakresem.");
//            assertThat(false).isTrue();
//        } catch (IllegalArgumentException e) {
//            assertThat(true).isTrue();
//        }
//
//        try {
//            Assert.valueNotMultipledByOrNotInRange(9.5, 0.2, 4.4, 9.6, "Mod != 0.");
//            assertThat(false).isTrue();
//        } catch (IllegalArgumentException e) {
//            assertThat(true).isTrue();
//        }
//
//        try {
//            Assert.valueNotMultipledByOrNotInRange(9.5, 0.25, 9.6, 10, "Poza zakresem.");
//            assertThat(false).isTrue();
//        } catch (IllegalArgumentException e) {
//            assertThat(true).isTrue();
//        }
//
//        try {
//            Assert.valueNotMultipledByOrNotInRange(9.58, 0.25, 4.5, 19.4, "Poza zakresem.");
//            assertThat(false).isTrue();
//        } catch (IllegalArgumentException e) {
//            assertThat(true).isTrue();
//        }
//
//        Assert.valueNotMultipledByOrNotInRange(9.5, 0.5, 4.5, 19.4, "Poza zakresem.");
//        Assert.valueNotMultipledByOrNotInRange(9.5, 0.1, 9.5, 19.5, "Poza zakresem.");
//        Assert.valueNotMultipledByOrNotInRange(19.5, 0.5, 9.5, 19.5, "Poza zakresem.");

    }

}