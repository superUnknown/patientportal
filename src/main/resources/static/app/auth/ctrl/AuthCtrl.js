'use strict';

/* AuthCtrl */
app.controller('AuthCtrl', ['$rootScope', '$scope', '$state', 'Auth', '$window', 'User', '$exceptionHandler', '$location', function($rootScope, $scope, $state, Auth, $window, User, $exceptionHandler, $location) {

    $scope.login = function(credentials) {
        Auth.login(credentials);
    };

    $scope.resetPassword = function(email) {
        $rootScope.loading = true;

        User.resetPassword(email).$promise.then(function(response) {
            $rootScope.loading = false;
            $state.go('auth.login', {}, { reload: true });

        }, function(error) {
            $exceptionHandler({ title: 'reset_password_request_error', error: error });
            $rootScope.loading = false;

        });

    };

    $scope.finalizeRegistration = function(password) {
        $rootScope.loading = true;
        User.finalizeRegistration(password).$promise.then(function(response) {
            $window.location.href = '/ui/portal/welcome';
            $rootScope.loading = false;
        }, function(error) {
            $exceptionHandler({ title: 'finalize_registration_error', error: error });
            $rootScope.loading = false;
        });
    };

    var token;
    $scope.verifyPasswordResetToken = function() {
        token = $location.search().token;
        $location.search('token', null);

        $rootScope.loading = true;

        User.verifyPasswordResetToken(token).$promise.then(function(response) {
            $rootScope.loading = false;

        }, function(error) {
            $rootScope.loading = false;
            $exceptionHandler({ title: 'verify_password_reset_error', error: error });
            $state.go('auth.login', {}, { reload: true });

        });

    };

    $scope.finalizePasswordReset = function(password) {
        $rootScope.loading = true;
        User.finalizePasswordReset(token, password).$promise.then(function(response) {
            $window.location.href = '/ui/portal/welcome';
            $rootScope.loading = false;
        }, function(error) {
            $rootScope.loading = false;
            $exceptionHandler({ title: 'verify_password_reset_error', error: error });
        });
    };

}]);