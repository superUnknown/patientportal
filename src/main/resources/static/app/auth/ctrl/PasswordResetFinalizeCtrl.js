'use strict';

/* PasswordResetFinalizeCtrl */
app.controller('PasswordResetFinalizeCtrl', ['$rootScope', '$scope', '$state', 'Auth', '$window', 'User', '$exceptionHandler', '$location', function($rootScope, $scope, $state, Auth, $window, User, $exceptionHandler, $location) {

    var token;
    (function() {
        token = $location.search().token;
        $location.search('token', null);

        $rootScope.loading = true;

        User.verifyPasswordResetToken(token).$promise.then(function(response) {
            $rootScope.loading = false;

        }, function(error) {
            $rootScope.loading = false;
            $exceptionHandler({ title: 'verify_password_reset_error', error: error });
            $state.go('auth.login', {}, { reload: true });

        });

    })();

    $scope.finalizePasswordReset = function(password) {
        $rootScope.loading = true;
        User.finalizePasswordReset(token, password).$promise.then(function(response) {
            $window.location.href = '/ui/portal/welcome';
            $rootScope.loading = false;
        }, function(error) {
            $rootScope.loading = false;
            $exceptionHandler({ title: 'verify_password_reset_error', error: error });
        });
    };

}]);