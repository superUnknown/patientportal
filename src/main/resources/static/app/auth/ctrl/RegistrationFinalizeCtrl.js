'use strict';

/* RegistrationFinalizeCtrl */
app.controller('RegistrationFinalizeCtrl', ['$rootScope', '$scope', '$state', 'Auth', '$window', 'User', '$exceptionHandler', '$location', function($rootScope, $scope, $state, Auth, $window, User, $exceptionHandler, $location) {

    var token;
    (function() {
        token = $location.search().token;
        $location.search('token', null);

        $rootScope.loading = true;

        User.verifyRegistrationToken(token).$promise.then(function(response) {
            $rootScope.loading = false;

        }, function(error) {
            $rootScope.loading = false;
            $exceptionHandler({ title: 'confirm_registration_error', error: error });
            $state.go('auth.login', {}, { reload: true });

        });

    })();

    $scope.finalizeRegistration = function(password) {
        $rootScope.loading = true;
        User.finalizeRegistration(token, password).$promise.then(function(response) {
            $window.location.href = '/ui/portal/welcome';
            $rootScope.loading = false;
        }, function(error) {
            $exceptionHandler({ title: 'finalize_registration_error', error: error });
            $rootScope.loading = false;
        });
    };

}]);