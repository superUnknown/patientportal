'use strict';

/* AuthModalCtrl */
app.controller('AuthModalCtrl', ['$rootScope', '$scope', '$state', 'Auth', '$window', 'User', '$exceptionHandler', '$location', '$uibModalInstance', function($rootScope, $scope, $state, Auth, $window, User, $exceptionHandler, $location, $uibModalInstance) {

    $scope.modal = $uibModalInstance;

    $scope.refreshSession = function(credentials) {
        Auth.refreshSession(credentials, $scope.modal);
    };

}]);