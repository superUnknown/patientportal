'use strict';

/* Auth service */

app.factory('Auth', ['$http', '$window', '$state', '$exceptionHandler', 'User', '$rootScope', function AuthFactory($http, $window, $state, $exceptionHandler, User, $rootScope) {

    var auth = {
        me: null,
        isAuthenticated: false
    };

    auth.login = function(credentials) {
        $rootScope.loading = true;

        $http(createLoginPayload(credentials)).then(function(response) {
            $window.location.href = '/ui/portal/welcome';
            $rootScope.loading = false;
        }, function(error) {
            $exceptionHandler({ title: 'login_failed_error', error: {
                status: 400,
                data: {
                    code: 'bad_credentials'
                }
            }});
            $rootScope.loading = false;
        });

    };

    auth.refreshSession = function(credentials, modal) {
        $rootScope.loading = true;

        $http(createLoginPayload(credentials)).then(function(response) {
            modal.close();
            $rootScope.loading = false;
        }, function(error) {
            $exceptionHandler({ title: 'login_failed_error', error: {
                status: 400,
                data: {
                    code: 'bad_credentials'
                }
            }});
            $rootScope.loading = false;
        });

    };

    auth.logout = function() {
        $rootScope.loading = true;
	    $http.post('/logout', {}).finally(function() {
	        $window.location.reload();
	        $rootScope.loading = false;
	    });
    };

    auth.setAuthentication = function(user) {
        auth.me = user;
        auth.isAuthenticated = user ? true : false;

    };

    auth.authenticate = function() {
        User.getCurrentUser().$promise.then(function(response) {
            auth.setAuthentication(response);

        }, function(error) {
            auth.setAuthentication(null);

        });

    };

    auth.hasAuthority = function(authority) {
        return auth.me && auth.me.authorities && auth.me.authorities.indexOf(authority) != -1;
    };

    auth.getHighestAuthority = function(authorities) {
        var authority;
        angular.forEach($rootScope.AUTHORITY, function(key, val) {
            if(!authority && authorities.indexOf(key) != -1) {
                authority = key;
            }
        });

        return authority;
    };

    var createLoginPayload = function(credentials) {
        return {
            method: 'POST',
            url: '/ui/auth/login',
            data: 'username=' + credentials.username + '&password=' + credentials.password + (credentials.rememberMe ? '&remember-me=' + credentials.rememberMe : ''),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };
    };

    return auth;
}]);