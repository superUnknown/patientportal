'use strict';

app.controller('ApplicationCtrl', ['$rootScope', 'Auth', 'AUTHORITY', '$state', '$location', '$exceptionHandler', '$timeout', 'EYE_DISEASES', 'GENERAL_DISEASES', 'UNIT_BN_BS', 'UNIT_BD_BG', 'PLUS_MINUS_FLAG', 'CORRECTION_CATEGORY', 'CORRECTION_BASE', '$filter', 'Utils', function($rootScope, Auth, AUTHORITY, $state, $location, $exceptionHandler, $timeout, EYE_DISEASES, GENERAL_DISEASES, UNIT_BN_BS, UNIT_BD_BG, PLUS_MINUS_FLAG, CORRECTION_CATEGORY, CORRECTION_BASE, $filter, Utils) {

    $rootScope.utils = Utils;

    $rootScope.$state = $state;

    $rootScope.loading = false;

    $rootScope.auth = Auth;

    $rootScope.AUTHORITY = AUTHORITY;

    $rootScope.EYE_DISEASES = EYE_DISEASES;

    $rootScope.GENERAL_DISEASES = GENERAL_DISEASES;

    $rootScope.UNIT_BN_BS = UNIT_BN_BS;

    $rootScope.UNIT_BD_BG = UNIT_BD_BG;

    $rootScope.PLUS_MINUS_FLAG = PLUS_MINUS_FLAG;

    $rootScope.CORRECTION_CATEGORY = CORRECTION_CATEGORY;

    $rootScope.CORRECTION_BASE = CORRECTION_BASE;

    $rootScope.app = {
        name: 'Zielonka Optyk'
    };

    var accessControl = function(event, toState) {

        if(toState.name == 'auth.login' && Auth.isAuthenticated) {
            event.preventDefault();
            $state.go('app.welcome', {}, { reload: true });
            return;
        }

        var access = toState.data ? toState.data.access : null;
        if(!access) {
            return;
        }

        if(Auth.isAuthenticated) {
            if(access.status && Auth.me.status == access.status) {
                return;
            }
            if(access.authority && Auth.hasAuthority(access.authority)) {
                return;
            }

        }

        event.preventDefault();
        $state.go(Auth.isAuthenticated ? 'app.welcome' : 'auth.login', {}, { reload: true });

    };

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        accessControl(event, toState);

    });
}]);
