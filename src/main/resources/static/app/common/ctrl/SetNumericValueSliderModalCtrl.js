'use strict';

/* SetNumericValueSliderModalCtrl */
app.controller('SetNumericValueSliderModalCtrl', ['$scope', 'value', '$uibModalInstance', function($scope, value, $uibModalInstance) {

    $scope.value = value;

    if($scope.value.model == null) {
        $scope.value.model = value.initial;
    }

    $scope.modal = $uibModalInstance;

    $scope.submit = function(value) {
        $scope.modal.close(value.model);
    };

}]);