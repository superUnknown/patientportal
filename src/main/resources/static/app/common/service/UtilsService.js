'use strict';

/* UtilsService */

app.factory('Utils', ['CORRECTION_BASE', '$filter', function UtilsFactory(CORRECTION_BASE, $filter) {

    var service = {
        correctionBasePredictions: (function() {
            var predictions = [];
            for(var i = 0; i < CORRECTION_BASE.length; i++) {
                if(CORRECTION_BASE[i] != 'INT') {
                    predictions.push({
                        key: CORRECTION_BASE[i],
                        value: null
                    });
                }

            }

            var degrees = $filter('rangeDecimal')([], { step: 1, min: 0 , max: 180 });
            for(var i = 0; i < degrees.length; i++) {
                predictions.push({
                    key: 'INT',
                    value: degrees[i]
                });
            }

            return predictions;

        })()
    };

    service.deleteProperty = function(object, node) {
        delete object[node];

    };

    return service;
}]);