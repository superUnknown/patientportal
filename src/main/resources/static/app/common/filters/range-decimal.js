app.filter('rangeDecimal', function() {
  return function(input, specs) {
    var step = specs.step;
    var min = specs.min;
    var max = specs.max;

    var value = min;
    while(value <= max) {
        input.push(value);
        value += step;
    }

    return input;
  };
});