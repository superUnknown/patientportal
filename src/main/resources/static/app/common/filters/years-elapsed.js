app.filter('yearsElapsed', function() {
    return function(date) {
        if(!date) {
            return null;
        }

        return new Date().getFullYear() - new Date(date).getFullYear();
    };
});