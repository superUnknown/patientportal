angular.module('app').directive('unsavedChangesAlert', ['$filter', function($filter) {

    var warning = $filter('translate')('unsaved_changes_alert');

    return {
        link: function($scope, elem, attrs) {
            window.onbeforeunload = function(){
                if ($scope.form.$dirty) {
                    return warning;
                }
            }

            $scope.$on('$stateChangeStart', function (event, toState, toParams) {
                if ($scope.form.$dirty) {
                    if(!confirm(warning)) {
                        event.preventDefault();
                    }
                }

            });
        }
    };

}]);