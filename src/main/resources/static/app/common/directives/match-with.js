app.directive("matchWith", function() {
    return {
          require: "ngModel",
          scope: {
                otherModelValue: "=matchWith"
          },
          link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
          }
    };
});