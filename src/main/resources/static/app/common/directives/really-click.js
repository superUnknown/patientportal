/**
 * A generic confirmation for risky actions.
 * Usage: Add attributes: ng-really-message="Are you sure"? ng-really-click="takeAction()" function
 */
angular.module('app')

.directive('ngReallyClick', ['$uibModal', function($uibModal) {

    var ModalInstanceCtrl = function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

      };

    return {
        restrict: 'A',
        scope:{
            ngReallyClick:"&"
        },
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;

                var modalHtml = '' +
                    '<div class="modal-header text-center no-border wrapper">' +
                    '    <h3 class="font-bold m-t-n">{{ "confirmation_modal_title" | translate }}</h3>' +
                    '</div>' +
                    '<div class="text-center p-a">' +
                    '   <div class="lead">' + message + '</div>' +
                    '   <div class="btn-toolbar">' +
                    '       <button ng-click="ok()" type="button" class="pull-right btn btn-default btn-sm font-bold bg-transparent w-xxs">' +
                    '           <i class="fa fa-check"></i>' +
                    '       </button>' +
                    '       <button ng-click="cancel()" type="button" class="pull-right btn btn-default btn-sm font-bold w-xxs">' +
                    '           <i class="fa fa-close"></i>' +
                    '       </button>' +
                    '   </div>' +
                    '</div>';

                var modalInstance = $uibModal.open({
                    animation: true,
                    template: modalHtml,
                    size: 'md',
                    backdrop: 'static',
                    controller: ModalInstanceCtrl
                });

                modalInstance.result.then(function() {
                    scope.ngReallyClick();
                }, function() {

                });

            });
        }
    }
}]);