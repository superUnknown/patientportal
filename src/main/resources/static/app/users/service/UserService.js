'use strict';

/* User service */

app.factory('User', ['$resource', function UserFactory($resource) {

    var service = {};

    service.getCurrentUser = function() {
        var conn = $resource( '/api/users/me', {},{});

        return conn.get();
    };

    service.getUser = function(username) {
        var conn = $resource( '/api/users/:username', { username: username },{});

        return conn.get();
    };

    service.findUsers = function(query, page, qty) {
        var conn = $resource( '/api/users', {
                query : query,
                page : page,
                qty : qty
            },
            {}
        );

        return conn.get();
    };

    service.createUser = function(user) {
        var conn = $resource( '/api/users', {},{});

        return conn.save(user);
    };

    service.modifyUser = function(user) {
        var conn = $resource( '/api/users/:username', { username: user.username },{
            modify: { method:'PUT' }
        });

        return conn.modify(user);
    };

    service.deactivateUser = function(username) {
        var conn = $resource( '/api/users/:username/deactivate', { username: username },{
            deactivate: { method:'PUT' }
        });

        return conn.deactivate();
    };

    service.reactivateUser = function(username) {
        var conn = $resource( '/api/users/:username/reactivate', { username: username },{
            reactivate: { method:'PUT' }
        });

        return conn.reactivate();
    };

    service.deleteUser = function(username) {
        var conn = $resource( '/api/users/:username', { username: username },{});

        return conn.remove();
    };

    service.verifyRegistrationToken = function(token) {
        var conn = $resource( '/api/verify_registration', { token: token },{});

        return conn.get();
    };

    service.finalizeRegistration = function(token, password) {
        var conn = $resource( '/api/verify_registration', {},{});

        return conn.save({
            token: token,
            password: password
        });
    };

    service.resetPassword = function(email) {
        var conn = $resource( '/api/reset_password', {},{});

        return conn.save({email: email});
    };

    service.verifyPasswordResetToken = function(token) {
        var conn = $resource( '/api/reset_password', { token: token },{});

        return conn.get();
    };

    service.finalizePasswordReset = function(token, password) {
        var conn = $resource( '/api/reset_password', {},{
            finalize: { method:'PUT' }
        });

        return conn.finalize({
            token: token,
            password: password
        });
    };

    return service;
}]);