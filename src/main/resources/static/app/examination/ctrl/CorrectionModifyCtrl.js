'use strict';

/* CorrectionModifyCtrl */
app.controller('CorrectionModifyCtrl', ['$rootScope', '$scope', '$state', 'User', '$exceptionHandler', 'Patient', '$filter', '$stateParams', 'Examination', '$uibModal', function($rootScope, $scope, $state, User, $exceptionHandler, Patient, $filter, $stateParams, Examination, $uibModal) {

    var examinationId = $stateParams.examinationId;

    $scope.examinationService = Examination;

    Examination.getExamination(examinationId, true).$promise.then(function(response) {
        $scope.corrections = response.corrections;

    });

    $scope.modifyCorrection = function(corrections, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({ title: 'form_invalid', error: {
                status: 400,
                data: {
                    code: 'form_invalid_msg'
                }
            }});
            return;
        }
        $rootScope.loading = true;

        Examination.modifyCorrection(examinationId, angular.copy(corrections)).$promise.then(function(response) {
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.go('app.patient_card.examination.preview', {}, { reload: true });

        }, function(error) {
            $exceptionHandler({ title: 'modify_correction_error', error: error });
            $rootScope.loading = false;

        });

    };

}]);