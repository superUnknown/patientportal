'use strict';

/* PrintExaminationPreviewCtrl */
app.controller('PrintExaminationPreviewCtrl', ['$rootScope', '$scope', '$state', 'Patient', '$exceptionHandler', '$uibModalInstance', 'image', 'filename', function($rootScope, $scope, $state, Patient, $exceptionHandler, $uibModalInstance, image, filename) {

    $scope.modal = $uibModalInstance;
    $scope.image = angular.copy(image);

    $scope.download = function() {
        var link = document.createElement('a');
        document.body.appendChild(link);
        link.setAttribute("type", "hidden"); // make it hidden if needed
        link.download = filename;
        link.href = $scope.image.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.click();
        link.remove();
    }

}]);