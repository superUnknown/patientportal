'use strict';

/* SetCorrectionBaseValueModalCtrl */
app.controller('SetCorrectionBaseValueModalCtrl', ['$scope', '$uibModalInstance', 'value', function($scope, $uibModalInstance, value) {

    $scope.base = {
        key: value.base,
        value: value.baseValue
    };

    $scope.modal = $uibModalInstance;

    $scope.submit = function(base) {
        $scope.modal.close({
            base: base.key,
            baseValue: base.value
        });
    };

}]);