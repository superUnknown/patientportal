'use strict';

/* ExaminationModifyCtrl */
app.controller('ExaminationModifyCtrl', ['$rootScope', '$scope', '$state', 'User', '$exceptionHandler', 'Patient', '$filter', '$stateParams', 'Examination', '$uibModal', function($rootScope, $scope, $state, User, $exceptionHandler, Patient, $filter, $stateParams, Examination, $uibModal) {

    var examinationId = $stateParams.examinationId;

    $scope.examinationService = Examination;

    $scope.sections = {
        basicInformation: {
            collapsed: false
        },
        binocularVision: {
            collapsed: true,
            loaded: false
        },
        synoptoforTest: {
            collapsed: true
        },
        accommodation: {
            collapsed: true
        },
        correction: {
            collapsed: true
        },
        documents: {
            collapsed: true
        }
    };

    Examination.getExamination(examinationId, true).$promise.then(function(response) {
        $scope.examinationForm = response;
        $scope.examinationForm.basicInformation.lastExaminationDate = {
            date: $scope.examinationForm.basicInformation.lastExaminationDate,
            absent: $scope.examinationForm.basicInformation.lastExaminationDate == null
        };

        var eyeDiseases = $scope.examinationForm.basicInformation.eyeDiseases;
        $scope.examinationForm.basicInformation.eyeDiseases = {
            'OTHERS': []
        };
        angular.forEach(eyeDiseases, function(disease) {
            if($rootScope.EYE_DISEASES.indexOf(disease) != -1) {
                $scope.examinationForm.basicInformation.eyeDiseases[disease] = true;

            } else {
                $scope.examinationForm.basicInformation.eyeDiseases['OTHERS'].push({"text":disease});
            }

        });

        var generalDiseases = $scope.examinationForm.basicInformation.generalDiseases;
        $scope.examinationForm.basicInformation.generalDiseases = {
            'OTHERS': []
        };
        angular.forEach(generalDiseases, function(disease) {
            if($rootScope.GENERAL_DISEASES.indexOf(disease) != -1) {
                $scope.examinationForm.basicInformation.generalDiseases[disease] = true;

            } else {
                $scope.examinationForm.basicInformation.generalDiseases['OTHERS'].push({"text":disease});
            }

        });

        if($scope.examinationForm.binocularVision.worthsTestId) {
            $scope.examinationForm.binocularVision.worthsTest = Examination.getWorthsTest(
                $scope.examinationForm.binocularVision.worthsTestId
            );

            delete $scope.examinationForm.binocularVision['worthsTestId'];

        }

        if($scope.examinationForm.binocularVision.schobersTestId) {
            $scope.examinationForm.binocularVision.schobersTest = Examination.getSchobersTest(
                $scope.examinationForm.binocularVision.schobersTestId
            );

            delete $scope.examinationForm.binocularVision['schobersTestId'];

        }

        if($scope.examinationForm.accommodation.flipper.value && $scope.examinationForm.accommodation.flipper.problem == null) {
            $scope.examinationForm.accommodation.flipper.problem = 'UNDEFINED';
        }

    });

    $scope.toggleSection = function(sectionName) {
        $scope.sections[sectionName].collapsed = !$scope.sections[sectionName].collapsed;
        if(!$scope.sections[sectionName].loaded && !$scope.sections[sectionName].collapsed) {
            $scope.sections[sectionName].loaded = true;
        }

    };

    $scope.modifyExamination = function(form, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({
                title: 'form_invalid',
                error: {
                    status: 400,
                    data: {
                        code: 'form_invalid_msg'
                    }
                },
                missingFields: f.$error.required.map(function(a) {return a.$name;})
            });
            return;
        }
        $rootScope.loading = true;

        var examination = angular.copy(form);
        Examination.modifyExamination(examinationId, examination).$promise.then(function(response) {
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.go('app.patient_card.examination.preview', {}, { reload: true });

        }, function(error) {
            $exceptionHandler({ title: 'modify_examination_error', error: error });
            $rootScope.loading = false;

        });

    };

    $scope.toggleCorrectionOnPreview = function(correction) {
        correction.inPreview = correction.inPreview ? false : true;

    };

    $scope.$watch('examinationForm', function(newVal, oldVal) {
        $scope.updateExaminationPreview(newVal);

    }, true);

}]);