'use strict';

/* ExaminationCtrl */
app.controller('ExaminationCtrl', ['$rootScope', '$scope', '$state', '$exceptionHandler', '$stateParams', 'Examination', '$uibModal', '$filter', function($rootScope, $scope, $state, $exceptionHandler, $stateParams, Examination, $uibModal, $filter) {

    var printableSections = ['patient_data_section', 'basic_data_section', 'correction_section'];

    var examinationId = $stateParams.examinationId;

    $scope.examinationService = Examination;

    $scope.examinationDataToCopy = {
        corrections: []
    };

    $scope.copyExamination = function(data) {
        $state.go('app.patient_card.create_examination', {
            copyData: angular.copy(data)
        });
    };

    Examination.getExamination(examinationId, true).$promise.then(function(response) {
        $scope.examination = response;

        var diseases = $scope.examination.basicInformation.eyeDiseases;
        $scope.examination.basicInformation.eyeDiseases = [];
        for(var i = 0; i < diseases.length; i+=2) {
            $scope.examination.basicInformation.eyeDiseases.push([diseases[i], diseases[i + 1]]);
        }

        diseases = $scope.examination.basicInformation.generalDiseases;
        $scope.examination.basicInformation.generalDiseases = [];
        for(var i = 0; i < diseases.length; i+=2) {
            $scope.examination.basicInformation.generalDiseases.push([diseases[i], diseases[i + 1]]);
        }

        if($scope.examination.binocularVision.worthsTestId) {
            $scope.examination.binocularVision.worthsTest = Examination.getWorthsTest(
                $scope.examination.binocularVision.worthsTestId
            );

            delete $scope.examination.binocularVision['worthsTestId'];

        }

        if($scope.examination.binocularVision.schobersTestId) {
            $scope.examination.binocularVision.schobersTest = Examination.getSchobersTest(
                $scope.examination.binocularVision.schobersTestId
            );

            delete $scope.examination.binocularVision['schobersTestId'];

        }

        if($scope.examination.accommodation.flipper.value && $scope.examination.accommodation.flipper.problem == null) {
            $scope.examination.accommodation.flipper.problem = 'UNDEFINED';
        }

    }, function(error) {
        $exceptionHandler({ title: 'get_examination_error', error: error });
        $state.go('app.404');
    });

    $scope.printExamination = function(printAll) {
        $rootScope.loading = true;

        var preview = document.getElementById("examination_preview").cloneNode(true);
        preview.id = "examination_preview_tmp";
        preview.setAttribute("style","background-color:white");

        if(printAll) {
            for(var i = 0; i < preview.childNodes.length; i++) {
                if(preview.childNodes[i].id == 'patient_data_section') {
                    preview.childNodes[i].removeAttribute("hidden");
                } else if(preview.childNodes[i].id == 'documents') {
                    preview.childNodes[i].setAttribute("hidden", true);
                }
            }
        } else {
            for(var i = 0; i < preview.childNodes.length; i++) {
                if(printableSections.indexOf(preview.childNodes[i].id) != -1) {
                    preview.childNodes[i].removeAttribute("hidden");
                } else {
                    if(preview.childNodes[i].setAttribute) {
                        preview.childNodes[i].setAttribute("hidden", true);
                    }
                }
            }
        }

        document.body.appendChild(preview);

        html2canvas(preview, {
            onrendered: function(canvas) {
                $scope.$apply(function () {
                    $rootScope.loading = false;
                });

                openPrintExaminationPreview(
                    canvas.toDataURL("image/png"),
                    $scope.patient.surname + '_' + $scope.patient.firstname + '_' + $filter('date')($scope.examination.basicInformation.date, "dd_MM_yyyy")
                );
                preview.remove();
            }
        });
    }

    var openPrintExaminationPreview = function(image, filename) {
        $uibModal.open({
            animation: true,
            size: 'lg',
            backdrop: 'static',
            templateUrl: 'app/examination/partials/print_examination_preview_modal.html',
            controller: 'PrintExaminationPreviewCtrl',
            scope: $scope,
            resolve: {
                image: function() {
                    return image;
                },
                filename: function() {
                    return filename;
                }
            }
        });
    };

    $scope.previewDocument = function(doc) {
        $uibModal.open({
            animation: true,
            size: 'lg',
            backdrop: 'static',
            templateUrl: 'app/examination/partials/preview_document_modal.html',
            controller: function(doc, $uibModalInstance) {
                $scope.modal = $uibModalInstance;
                $scope.preview = angular.copy(doc);
            },
            scope: $scope,
            resolve: {
                doc: function() {
                    return doc;
                }
            }
        });
    };

}]);