'use strict';

/* CorrectionCreateCtrl */
app.controller('CorrectionCreateCtrl', ['$rootScope', '$scope', '$state', 'User', '$exceptionHandler', 'Patient', '$filter', '$stateParams', 'Examination', function($rootScope, $scope, $state, User, $exceptionHandler, Patient, $filter, $stateParams, Examination) {

    $scope.corrections = [
        { opDesc: {}, olDesc: {} }
    ];

    $scope.examinationService = Examination;

    $scope.createCorrection = function(corrections, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({ title: 'form_invalid', error: {
                status: 400,
                data: {
                    code: 'form_invalid_msg'
                }
            }});
            return;
        }

        $rootScope.loading = true;

        var patientId = $stateParams.patientId;
        Examination.createCorrection(patientId, angular.copy(corrections)).$promise.then(function(response) {
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.go('app.patient_card.examinations');

        }, function(error) {
            $exceptionHandler({ title: 'create_correction_error', error: error });
            $rootScope.loading = false;

        });

    };

}]);