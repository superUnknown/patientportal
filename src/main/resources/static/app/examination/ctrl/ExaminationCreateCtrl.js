'use strict';

/* ExaminationCreateCtrl */
app.controller('ExaminationCreateCtrl', ['$rootScope', '$scope', '$state', 'User', '$exceptionHandler', 'Patient', '$filter', '$stateParams', 'Examination', function($rootScope, $scope, $state, User, $exceptionHandler, Patient, $filter, $stateParams, Examination) {

    // domyślny formularz
    $scope.examinationForm = {
        basicInformation: {
            date: new Date(),
            lastExaminationDate: Examination.getPatientsLastExaminationDate($stateParams.patientId)
        },
        corrections: $stateParams.copyData ? $stateParams.copyData.corrections : [],
        documents: []
    };

    $scope.examinationService = Examination;

    $scope.sections = {
        basicInformation: {
            collapsed: false
        },
        binocularVision: {
            collapsed: true,
            loaded: false
        },
        synoptoforTest: {
            collapsed: true
        },
        accommodation: {
            collapsed: true
        },
        correction: {
            collapsed: true
        },
        documents: {
            collapsed: true
        }
    };

    $scope.createExamination = function(form, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({
                title: 'form_invalid',
                error: {
                    status: 400,
                    data: {
                        code: 'form_invalid_msg'
                    }
                },
                missingFields: f.$error.required.map(function(a) {return a.$name;})
            });
            return;
        }

        $rootScope.loading = true;

        var examination = angular.copy(form);
        var patientId = $stateParams.patientId;
        Examination.createExamination(patientId, examination).$promise.then(function(response) {
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.go('app.patient_card.examination.preview', {
                patientId: patientId,
                examinationId: response.examinationId
            });

        }, function(error) {
            $exceptionHandler({ title: 'create_examination_error', error: error });
            $rootScope.loading = false;

        });

    };

    $scope.toggleSection = function(sectionName) {
        $scope.sections[sectionName].collapsed = !$scope.sections[sectionName].collapsed;
        if(!$scope.sections[sectionName].loaded && !$scope.sections[sectionName].collapsed) {
            $scope.sections[sectionName].loaded = true;
        }
    };

    $scope.toggleCorrectionOnPreview = function(correction) {
        correction.inPreview = correction.inPreview ? false : true;

    };

    $scope.$watch('examinationForm', function(newVal, oldVal) {
        $scope.updateExaminationPreview(newVal);

    }, true);

}]);