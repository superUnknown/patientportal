'use strict';

/* PatientExaminationsCtrl */
app.controller('PatientExaminationsCtrl', ['$rootScope', '$scope', '$stateParams', '$exceptionHandler', 'Examination', 'User', function($rootScope, $scope, $stateParams, $exceptionHandler, Examination, User) {

    var patientId = $stateParams.patientId;
    $scope.examinations = {
        last: false,
        number: -1, // pierwsze wywołanie podbije do 0
        content: []
    };

    $scope.fetchPatientExaminations = function() {
        if($scope.examinations.last) {
            return;
        }

        Examination.getPatientExaminations(patientId, ++$scope.examinations.number).$promise.then(function(response) {
            var hits = $scope.examinations.content;
            $scope.examinations = response;
            angular.forEach($scope.examinations.content, function(examination) {
                examination.createdBy = User.getUser(examination.createdBy);
                examination.modifiedBy = examination.modifiedBy ? User.getUser(examination.modifiedBy) : null;
            });

            $scope.examinations.content = hits.concat($scope.examinations.content);

        }, function(error) {
            $exceptionHandler({ title: 'get_patient_examinations_error', error: error });

        });

    };

    $scope.correctionsVisible = false;
    $scope.toggleCorrections = function() {
        if($scope.correctionsVisible) {
            angular.forEach($scope.examinations.content, function(examination) {
                delete examination.corrections;
            });
        } else {
            angular.forEach($scope.examinations.content, function(examination) {
                examination.corrections = Examination.getExaminationCorrections(examination.examinationId);
            });
        }

        $scope.correctionsVisible = !$scope.correctionsVisible;
    };

    $scope.fetchPatientExaminations();

}]);