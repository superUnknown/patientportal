'use strict';

/* AddDocumentModalCtrl */
app.controller('AddDocumentModalCtrl', ['$scope', '$uibModalInstance', 'Examination', function($scope, $uibModalInstance, Examination) {

    $scope.modal = $uibModalInstance;

    $scope.documentNames = Examination.getAllDocumentNames();

    $scope.doc = {};

    $scope.submit = function(doc) {
        $scope.modal.close(doc);
    };

    $scope.handlePictureSelect = function(element) {
        var reader = new FileReader();

        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.doc.content = evt.target.result;
            });
        };

        reader.readAsDataURL(element.files[0]);

    };

}]);