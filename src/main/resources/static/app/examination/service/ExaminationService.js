'use strict';

/* Examination Service */

app.factory('Examination', ['$resource', 'PLUS_MINUS_FLAG', '$uibModal', function ExaminationFactory($resource, PLUS_MINUS_FLAG, $uibModal) {

    var flipperPredictions = angular.copy(PLUS_MINUS_FLAG);
    flipperPredictions['UNDEFINED'] = 'UNDEFINED';

    var service = {
        flipperPredictions: flipperPredictions,
        dateConfig: {
            minDate: new Date(1900, 0, 1),
            maxDate: new Date(),
            startingDay: 1
        },
        lastExaminationDateConfig: {
            minDate: new Date(1900, 0, 1),
            maxDate: new Date(),
            startingDay: 1
        },
        schobersTests: $resource('/api/schobers_tests', {}, {isArray: true}).query(),
        worthsTests: $resource('/api/worths_tests', {}, {isArray: true}).query()
    };

    service.getAllDocumentNames = function() {
        var conn = $resource('api/documents/names', {}, {
            'query': { method:'GET', isArray: true }
        });

        return conn.query();
    };

    service.getExamination = function(examinationId, extended) {
        var conn = $resource( '/api/examinations/:examinationId', {
            examinationId: examinationId,
            extended: extended
        });

        return conn.get();
    };

    service.createExamination = function(patientId, examination) {
        var conn = $resource( '/api/patients/:patientId/examinations', { patientId: patientId },{});

        return conn.save(processForm(examination));
    };

    service.modifyExamination = function(examinationId, examination) {
        var conn = $resource( '/api/examinations/:examinationId', { examinationId: examinationId },{
            modify: { method:'PUT' }
        });

        return conn.modify(processForm(examination));
    };

    service.createCorrection = function(patientId, corrections) {
        var conn = $resource( '/api/patients/:patientId/corrections', { patientId: patientId },{});

        return conn.save(corrections);
    };

    service.modifyCorrection = function(examinationId, corrections) {
        var conn = $resource( '/api/corrections/:examinationId', { examinationId: examinationId },{
            modify: { method:'PUT' }
        });

        return conn.modify(corrections);
    };

    service.getPatientExaminations = function(patientId, page, qty) {
        var conn = $resource( '/api/patients/:patientId/examinations', {
                patientId : patientId,
                page : page,
                qty : qty
            },
            {}
        );

        return conn.get();
    };

    service.getExaminationCorrections = function(examinationId) {
        var conn = $resource( '/api/examinations/:examinationId/corrections',
            { examinationId: examinationId },
            { isArray: true }
        );

        return conn.query();
    };

    service.getPatientsLastExaminationDate = function(patientId) {
        var conn = $resource( '/api/patients/:patientId/examinations/last_examination_date', {
            patientId: patientId
        });

        return conn.get();
    };

    service.getSchobersTest = function(id) {
        var conn = $resource( '/api/schobers_tests/:id', { id: id });

        return conn.get();
    };

    service.getWorthsTest = function(id) {
        var conn = $resource( '/api/worths_tests/:id', { id: id });

        return conn.get();
    };

    service.numericValueFromModal = function(name, object, field, sliderConf) {
        var modal = $uibModal.open({
            animation: true,
            size: 'lg',
            backdrop: 'static',
            templateUrl: 'app/common/partials/set_numeric_value_slider_modal.html',
            controller: 'SetNumericValueSliderModalCtrl',
            resolve: {
                value: function() {
                    return {
                        name: name,
                        model: object[field],
                        step: sliderConf.step,
                        min: sliderConf.min,
                        max: sliderConf.max,
                        initial: sliderConf.initial
                    };
                }
            }
        });

        modal.result.then(function (value) {
            if (typeof value != 'undefined') {
                object[field] = value;
            }

        });
    };

    service.openSetCorrectionBaseValueModal = function(object, field) {
        var modal = $uibModal.open({
            animation: true,
            size: 'md',
            backdrop: 'static',
            templateUrl: 'app/examination/partials/set_correction_base_value_modal.html',
            controller: 'SetCorrectionBaseValueModalCtrl',
            resolve: {
                value: {
                    base: object[field].base,
                    baseValue: object[field].baseValue
                }
            }
        });

        modal.result.then(function (value) {
            if (typeof value != 'undefined') {
                object[field].base = value.base;
                object[field].baseValue = value.baseValue;
            }

        });
    };

    service.openAddDocumentModal = function(documents) {
        var modal = $uibModal.open({
            animation: true,
            size: 'md',
            backdrop: 'static',
            templateUrl: 'app/examination/partials/add_document_modal.html',
            controller: 'AddDocumentModalCtrl'
        });

        modal.result.then(function (document) {
            if (typeof document != 'undefined') {
                documents.push(document);

            }

        });
    };

    var processForm = function(form) {
        var examination = angular.copy(form);

        var eyeDiseases = examination.basicInformation.eyeDiseases;
        examination.basicInformation.eyeDiseases = [];
        angular.forEach(eyeDiseases, function(key, val) {
            if(val == 'OTHERS' && (key && key.length > 0)) {
                for(var i = 0; i < key.length; i++) {
                    examination.basicInformation.eyeDiseases.push(key[i].text);
                }
            } else if(key == true) {
                examination.basicInformation.eyeDiseases.push(val);
            }
        });

        var generalDiseases = examination.basicInformation.generalDiseases;
        examination.basicInformation.generalDiseases = [];
        angular.forEach(generalDiseases, function(key, val) {
            if(val == 'OTHERS' && (key && key.length > 0)) {
                for(var i = 0; i < key.length; i++) {
                    examination.basicInformation.generalDiseases.push(key[i].text);
                }
            } else if(key == true) {
                examination.basicInformation.generalDiseases.push(val);
            }
        });

        var lastExaminationDate = examination.basicInformation.lastExaminationDate;
        examination.basicInformation.lastExaminationDate = lastExaminationDate.absent ? null : lastExaminationDate.date;

        if(examination.binocularVision && examination.binocularVision.worthsTest) {
            examination.binocularVision.worthsTestId = examination.binocularVision.worthsTest.worthsTestId;
            delete examination.binocularVision['worthsTest'];
        }

        if(examination.binocularVision && examination.binocularVision.schobersTest) {
            examination.binocularVision.schobersTestId = examination.binocularVision.schobersTest.schobersTestId;
            delete examination.binocularVision['schobersTest'];
        }

        if(examination.accommodation && examination.accommodation.flipper && examination.accommodation.flipper.problem == 'UNDEFINED') {
            examination.accommodation.flipper.problem = null;
        }

        if(examination.documents.length != 0) {
            angular.forEach(examination.documents, function(doc) {
                doc.content = window.btoa(doc.content);
            });
        }

        return examination;
    };

    return service;
}]);