'use strict';

/* PatientCreateCtrl */
app.controller('PatientCreateCtrl', ['$rootScope', '$scope', '$state', 'User', '$exceptionHandler', 'Address', 'Patient', function($rootScope, $scope, $state, User, $exceptionHandler, Address, Patient) {

    $scope.dateOfBirthConfig = {
        minDate: new Date(1900, 0, 1),
        maxDate: new Date(),
        startingDay: 1
    };

    $scope.cities = Address.getAllCities();
    $scope.countries = Address.getAllCountries();
    $scope.representative = {
        collapsed: true
    };

    $scope.createPatient = function(form, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({ title: 'form_invalid', error: {
                status: 400,
                data: {
                    code: 'form_invalid_msg'
                }
            }});
            return;
        }
        $rootScope.loading = true;

        var patient = angular.copy(form);

        Patient.createPatient(patient).$promise.then(function(response) {
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.go('app.patient_card.examinations', {patientId: response.patientId});

        }, function(error) {
            $exceptionHandler({ title: 'create_patient_error', error: error });
            $rootScope.loading = false;

        });

    };

    $scope.toggleRepresentative = function() {
        $scope.representative.collapsed = !$scope.representative.collapsed;
        if($scope.representative.collapsed) {
            $scope.patient.representative = null;
        }
    };

    $scope.copyAddressFromPatient = function(patient) {
        if(!patient.representative) {
            patient.representative = {};
        }
        patient.representative.address = angular.copy(patient.address);
    };

}]);