'use strict';

/* PatientCardCtrl */
app.controller('PatientCardCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'User', '$exceptionHandler', 'Address', 'Patient', '$uibModal', function($rootScope, $scope, $state, $stateParams, User, $exceptionHandler, Address, Patient, $uibModal) {

    $scope.patientId = $stateParams.patientId;

    $scope.state = $state;

    $scope.examinationPreview = {
        basicInformation: {}
    };

    Patient.getPatient($scope.patientId).$promise.then(function(response) {
        $scope.patient = response;

    }, function(error) {
        $exceptionHandler({ title: 'get_patient_card_error', error: error });
        $state.go('app.404');
    });

    $scope.updateExaminationPreview = function(examination) {
        if(!examination) return;
        $scope.examinationPreview.basicInformation.visitReason = examination.basicInformation.visitReason;
        $scope.examinationPreview.basicInformation.otherAilments = examination.basicInformation.otherAilments;
        $scope.examinationPreview.basicInformation.visionCorrectionHistory = examination.basicInformation.visionCorrectionHistory;

        var eyeDiseases = [];
        angular.forEach(examination.basicInformation.eyeDiseases, function(key, val) {
            if(val == 'OTHERS' && (key && key.length > 0)) {
                for(var i = 0; i < key.length; i++) {
                    eyeDiseases.push(key[i].text);
                }
            } else if(key == true) {
                eyeDiseases.push(val);
            }
        });

        $scope.examinationPreview.basicInformation.eyeDiseases = [];
        for(var i = 0; i < eyeDiseases.length; i+=2) {
            $scope.examinationPreview.basicInformation.eyeDiseases.push([eyeDiseases[i], eyeDiseases[i + 1]]);
        }

        var generalDiseases = [];
        angular.forEach(examination.basicInformation.generalDiseases, function(key, val) {
            if(val == 'OTHERS' && (key && key.length > 0)) {
                for(var i = 0; i < key.length; i++) {
                    generalDiseases.push(key[i].text);
                }
            } else if(key == true) {
                generalDiseases.push(val);
            }
        });

        $scope.examinationPreview.basicInformation.generalDiseases = [];
        for(var i = 0; i < generalDiseases.length; i+=2) {
            $scope.examinationPreview.basicInformation.generalDiseases.push([generalDiseases[i], generalDiseases[i + 1]]);
        }

        $scope.examinationPreview.corrections = [];
        angular.forEach(examination.corrections, function(correction) {
            if(correction.inPreview) {
                $scope.examinationPreview.corrections.push(correction);
            }
        });

    };

    $scope.openModifyPatient = function(selectedPatient) {
        $uibModal.open({
            animation: true,
            size: 'md',
            backdrop: 'static',
            templateUrl: 'app/patients/partials/modify_patient_modal.html',
            controller: 'PatientModifyCtrl',
            scope: $scope,
            resolve: {
                patient: function() {
                    return selectedPatient;
                }
            }
        });
    };

}]);