'use strict';

/* PatientModifyCtrl */
app.controller('PatientModifyCtrl', ['$rootScope', '$scope', '$state', 'Patient', '$exceptionHandler', '$uibModalInstance', 'patient', 'Address', function($rootScope, $scope, $state, Patient, $exceptionHandler, $uibModalInstance, patient, Address) {

    $scope.modal = $uibModalInstance;
    $scope.patientForm = angular.copy(patient);
    $scope.cities = Address.getAllCities();
    $scope.countries = Address.getAllCountries();
    $scope.representative = {
        collapsed: (patient.representative == null)
    };

    $scope.modifyPatient = function(form, f) {
        if(f.$invalid) {
            f.$setSubmitted();
            $exceptionHandler({ title: 'form_invalid', error: {
                status: 400,
                data: {
                    code: 'form_invalid_msg'
                }
            }});
            return;
        }

        $rootScope.loading = true;

        var patient = angular.copy(form);

        Patient.modifyPatient(patient.patientId, patient).$promise.then(function(response) {
            $scope.modal.close();
            $rootScope.loading = false;
            $scope.form.$setPristine();
            $state.reload();

        }, function(error) {
            $exceptionHandler({ title: 'modify_patient_error', error: error });
            $rootScope.loading = false;

        });

    };

    $scope.toggleRepresentative = function() {
        $scope.representative.collapsed = !$scope.representative.collapsed;
        if($scope.representative.collapsed) {
            $scope.patientForm.representative = null;
        }
    };

    $scope.copyAddressFromPatient = function(patient) {
        if(!patient.representative) {
            patient.representative = {};
        }
        patient.representative.address = angular.copy(patient.address);
    };

}]);