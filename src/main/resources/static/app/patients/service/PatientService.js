'use strict';

/* Patient Service */

app.factory('Patient', ['$resource', function PatientFactory($resource) {

    var service = {};

    service.getPatient = function(patientId) {
        var conn = $resource( '/api/patients/:patientId', { patientId: patientId },{});

        return conn.get();
    };

    service.createPatient = function(patient) {
        var conn = $resource( '/api/patients', {},{});

        return conn.save(patient);
    };

    service.modifyPatient = function(patientId, patient) {
        var conn = $resource( '/api/patients/:patientId', { patientId: patientId },{
            modify: { method:'PUT' }
        });

        return conn.modify(patient);
    };

    service.findPatients = function(query, page, qty) {
        var conn = $resource( '/api/patients', {
                query : query,
                page : page,
                qty : qty
            },
            {}
        );

        return conn.get();
    };

    return service;
}]);