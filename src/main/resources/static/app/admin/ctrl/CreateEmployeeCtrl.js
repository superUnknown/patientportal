'use strict';

/* CreateEmployeeCtrl */
app.controller('CreateEmployeeCtrl', ['$rootScope', '$scope', 'User', '$exceptionHandler', '$uibModalInstance', function($rootScope, $scope, User, $exceptionHandler, $uibModalInstance) {

    $scope.modal = $uibModalInstance;

    $scope.createEmployee = function(form) {
        $rootScope.loading = true;

        var employee = angular.copy(form);

        User.createUser(employee).$promise.then(function(response) {
            $scope.employees.last = false;
            $scope.employees.number = -1;
            $scope.employees.content = [];

            $scope.findUsers();
            $scope.modal.close();
            $rootScope.loading = false;

        }, function(error) {
            $exceptionHandler({ title: 'create_employee_error', error: error });
            $rootScope.loading = false;

        });

    };

}]);