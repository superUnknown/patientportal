'use strict';

/* ModifyEmployeeCtrl */
app.controller('ModifyEmployeeCtrl', ['$rootScope', '$scope', 'User', '$exceptionHandler', '$uibModalInstance', 'employee', function($rootScope, $scope, User, $exceptionHandler, $uibModalInstance, employee) {

    $scope.modal = $uibModalInstance;

    $scope.employee = angular.copy(employee);

    $scope.modifyEmployee = function(form) {
        $rootScope.loading = true;

        var employee = angular.copy(form);

        User.modifyUser(employee).$promise.then(function(response) {
            for(var i = 0; i < $scope.employees.content.length; i++) {
                if($scope.employees.content[i].username == response.username) {
                    $scope.employees.content[i] = response;
                    break;
                }
            }

            $scope.modal.close();
            $rootScope.loading = false;

        }, function(error) {
            $exceptionHandler({ title: 'modify_employee_error', error: error });
            $rootScope.loading = false;

        });

    };

}]);