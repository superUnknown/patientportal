'use strict';

/* EmployeesCtrl */
app.controller('EmployeesCtrl', ['$rootScope', '$scope', 'User', '$exceptionHandler', '$uibModal', function($rootScope, $scope, User, $exceptionHandler, $uibModal) {

    $scope.query = '';

    $scope.employees = {
        last: false,
        number: -1, // pierwsze wywołanie podbije do 0
        content: []
    };

    $scope.openCreateEmployeeModal = function() {
        $uibModal.open({
            animation: true,
            size: 'md',
            backdrop: 'static',
            templateUrl: 'app/admin/partials/create_employee_modal.html',
            controller: 'CreateEmployeeCtrl',
            scope: $scope
        });
    };

    $scope.openModifyEmployeeModal = function(selectedEmployee) {
        $uibModal.open({
            animation: true,
            size: 'md',
            backdrop: 'static',
            templateUrl: 'app/admin/partials/modify_employee_modal.html',
            controller: 'ModifyEmployeeCtrl',
            scope: $scope,
            resolve: {
                employee: function() {
                    return selectedEmployee;
                }
            }
        });
    };

    $scope.findUsers = function() {
        if($scope.employees.last) {
            return;
        }

        User.findUsers($scope.query, ++$scope.employees.number).$promise.then(function(response) {
            var hits = $scope.employees.content;

            $scope.employees = response;
            $scope.employees.content = hits.concat($scope.employees.content);

        }, function(error) {
            $exceptionHandler({ title: 'find_users_error', error: error });

        });

    }

    $scope.disableEmployee = function(employee) {
        $rootScope.loading = true;

        User.deactivateUser(employee.username).$promise.then(function(response) {
            for(var i = 0; i < $scope.employees.content.length; i++) {
                if($scope.employees.content[i].username == response.username) {
                    $scope.employees.content[i] = response;
                    break;
                }
            }

            $rootScope.loading = false;

        }, function(error) {
            $exceptionHandler({ title: 'deactivate_employee_error', error: error });
            $rootScope.loading = false;

        });

    }

    $scope.reactivateEmployee = function(employee) {
        $rootScope.loading = true;

        User.reactivateUser(employee.username).$promise.then(function(response) {
            for(var i = 0; i < $scope.employees.content.length; i++) {
                if($scope.employees.content[i].username == response.username) {
                    $scope.employees.content[i] = response;
                    break;
                }
            }

            $rootScope.loading = false;

        }, function(error) {
            $exceptionHandler({ title: 'reactivate_employee_error', error: error });
            $rootScope.loading = false;

        });

    }

    $scope.deleteEmployee = function(employee) {
        $rootScope.loading = true;

        User.deleteUser(employee.username).$promise.then(function() {
            for(var i = 0; i < $scope.employees.content.length; i++) {
                if($scope.employees.content[i].username == employee.username) {
                    $scope.employees.content.splice(i, 1);
                    break;
                }
            }

            $rootScope.loading = false;

        }, function(error) {
            $exceptionHandler({ title: 'delete_employee_error', error: error });
            $rootScope.loading = false;

        });

    }

    $scope.$watch('query', function(newVal, oldVal) {
        $scope.employees = {
            last: false,
            number: -1,
            content: []
        };

        $scope.findUsers();

    }, true);

}]);