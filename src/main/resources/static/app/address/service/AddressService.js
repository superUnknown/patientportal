'use strict';

/* Address Service */

app.factory('Address', ['$resource', function AddressFactory($resource) {

    var service = {};

    service.getAllCities = function() {
        var conn = $resource('api/addresses/cities', {}, {
            'query': { method:'GET', isArray: true }
        });

        return conn.query();
    };

    service.getAllCountries = function() {
        var conn = $resource('api/addresses/countries', {}, {
            'query': { method:'GET', isArray: true }
        });

        return conn.query();
    };

    return service;
}]);