'use strict';

/* WelcomeCtrl */
app.controller('WelcomeCtrl', ['$rootScope', '$scope', '$exceptionHandler', 'Patient', function($rootScope, $scope, $exceptionHandler, Patient) {

    $scope.query = '';

    $scope.patients = {
        last: false,
        number: -1, // pierwsze wywołanie podbije do 0
        content: []
    };

    $scope.findPatients = function() {
        if($scope.patients.last) {
            return;
        }

        Patient.findPatients($scope.query, ++$scope.patients.number).$promise.then(function(response) {
            var hits = $scope.patients.content;

            $scope.patients = response;
            $scope.patients.content = hits.concat($scope.patients.content);

        }, function(error) {
            $exceptionHandler({ title: 'find_patients_error', error: error });

        });

    }

    $scope.$watch('query', function(newVal, oldVal) {
        if(newVal.length == 0) {
            $scope.patients.content = [];
            return;
        }

        $scope.patients = {
            last: false,
            number: -1,
            content: []
        };

        $scope.findPatients();

    }, true);

}]);