
app.factory('401Interceptor', ['$q', '$rootScope', '$injector', function($q, $rootScope, $injector) {
//app.factory('401Interceptor', ['$q', '$rootScope', '$uibModal', function($q, $rootScope, $uibModal) {
    var interceptor = {
        responseError: function(response) {
            // Session has expired
            if (response.status == 401){
                $rootScope.loading = false;
                $injector.get('$uibModal').open({
                    animation: true,
                    size: 'md',
                    backdrop: 'static',
                    templateUrl: 'app/auth/views/login_modal.html',
                    controller: 'AuthModalCtrl'
                });
                return;
            }
            return $q.reject(response);
        }
    };
    return interceptor;
}]);


/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/ui/portal/404');
    $urlRouterProvider.otherwise(function($injector, $location){
        var state = $injector.get('$state');
        if(['/', '/ui', '/ui/'].indexOf($location.path()) != -1) {
            state.go('app.welcome');
        } else {
            state.go('app.404');
        }

    });

    $httpProvider.interceptors.push('401Interceptor');

    $stateProvider
        .state('app', {
            url: '/ui/portal',
            abstract: true,
            templateUrl: 'app/common/views/app.html',
            data: {
                access: {
                    authority: 'READ_ONLY'
                }
            }
        })
        .state('app.welcome', {
            url: '/welcome',
            templateUrl: 'app/welcome/views/welcome.html',
            controller: 'WelcomeCtrl'
        })
        .state('app.create_patient', {
            url: '/create_patient',
            templateUrl: 'app/patients/views/create_patient.html',
            controller: 'PatientCreateCtrl',
            data: {
                access: {
                    authority: 'READ_WRITE'
                }
            }
        })
        .state('app.patient_card', {
            abstract: true,
            url: '/patient_card/:patientId',
            templateUrl: 'app/patients/views/patient_card.html',
            controller: 'PatientCardCtrl'
        })
        .state('app.patient_card.examinations', {
            url: '',
            templateUrl: 'app/examination/views/examinations.html',
            controller: 'PatientExaminationsCtrl'
        })
        .state('app.patient_card.create_examination', {
            url: '/create_examination',
            templateUrl: 'app/examination/views/create_examination.html',
            controller: 'ExaminationCreateCtrl',
            data: {
                access: {
                    authority: 'READ_WRITE'
                }
            },
            params: {
                copyData: null
            }
        })
        .state('app.patient_card.create_correction', {
            url: '/create_correction',
            templateUrl: 'app/examination/views/create_correction.html',
            controller: 'CorrectionCreateCtrl',
            data: {
                access: {
                    authority: 'READ_WRITE'
                }
            }
        })
        .state('app.patient_card.examination', {
            abstract: true,
            url: '/examination/:examinationId',
            templateUrl: 'app/examination/views/examination.html',
            controller: 'ExaminationCtrl'
        })
        .state('app.patient_card.examination.preview', {
            url: '',
            templateUrl: 'app/examination/views/examination_preview.html'
        })
        .state('app.patient_card.examination.modify', {
            url: '/modify',
            templateUrl: 'app/examination/views/examination_modify.html',
            controller: 'ExaminationModifyCtrl'
        })
        .state('app.patient_card.examination.modify_correction', {
            url: '/modify_correction',
            templateUrl: 'app/examination/views/correction_modify.html',
            controller: 'CorrectionModifyCtrl'
        })
        .state('app.admin', {
            url: '/admin',
            abstract: true,
            templateUrl: 'app/admin/views/admin.html',
            data: {
                access: {
                    authority: 'SUPER_USER'
                }
            }
        })
        .state('app.admin.employees', {
            url: '/employees',
            templateUrl: 'app/admin/views/employees.html',
            controller: 'EmployeesCtrl'
        })
        .state('app.404', {
            url: '/404',
            templateUrl: 'app/common/views/404.html'
        })
        .state('auth', {
            url: '/ui/auth',
            abstract: true,
            templateUrl: 'app/auth/views/auth.html',
            controller: 'AuthCtrl'
        })
        .state('auth.login', {
            url: '/login',
            templateUrl: 'app/auth/views/login.html'
        })
        .state('auth.finalize_registration', {
            url: '/finalize_registration',
            templateUrl: 'app/auth/views/finalize_registration.html',
            controller: 'RegistrationFinalizeCtrl'
        })
        .state('auth.finalize_password_reset', {
            url: '/password_reset',
            templateUrl: 'app/auth/views/finalize_password_reset.html',
            controller: 'PasswordResetFinalizeCtrl'
        })
        .state('auth.forgot_pwd', {
            url: '/forgot_pwd',
            templateUrl: 'app/auth/views/forgot_pwd.html'
        });

}]);

/**
 * Messages Config
 */
app.config(['$translateProvider', function($translateProvider){

    $translateProvider.useStaticFilesLoader({
      prefix: 'l10n/',
      suffix: '.js'
    });

    $translateProvider.preferredLanguage('pl_PL');

}]);

/**
 * Exception Handler Config
 */
app.config(['$provide', function($provide){

    $provide.decorator("$exceptionHandler", function ($delegate, $injector) {
        return function (exception, cause) {
            var $rootScope = $injector.get("$rootScope");
            var $filter = $injector.get("$filter");
            var toaster = $injector.get("toaster");

            $rootScope.loading = false;

            console.error('Exception occurred: ', exception);
            console.error('Cause: ', cause);

            if(!exception.error) {
                exception = {
                    title: 'internal_server_error',
                    error: {
                        status: 500,
                        data: {
                            code: {
                                message: exception.message,
                                error: exception
                            }
                        }
                    }
                };
            }

            var status = exception.error.status;
            var msg = assembleMessage($filter, 'exception.msg.' + ((status === -1) ? 'connection_lost' : exception.error.data.code), exception.error.data.code);

            if(exception.missingFields) {
                msg = msg + ': ';
                for(var i = 0; i < exception.missingFields.length; i++) {
                    msg += ($filter('translate')(
                        exception.missingFields[i].replace(/\.?([A-Z]+)/g, function (x,y){
                            return "_" + y.toLowerCase()
                        }).replace(/^_/, "")
                    ) + ', ');
                }

                msg = msg.substring(0, msg.length - 2);

            }

            if(exception.title && exception.error) {
                toaster.pop(
                    'error',
                    $filter('translate')('exception.title.' + exception.title),
                    msg
                );
            }
        };
    });

    function assembleMessage($filter, code, defaultVal) {
        var msg = $filter('translate')(code);
        if(msg === code) {
            msg = defaultVal;
        }
        return msg;
    }

}]);

app.filter("htmlSafe", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    };
}]);