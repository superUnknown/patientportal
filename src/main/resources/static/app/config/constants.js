'use strict';

app.constant('WEEK_DAY', {
    0: 'SUN',
    1: 'MON',
    2: 'TUE',
    3: 'WED',
    4: 'THU',
    5: 'FRI',
    6: 'SAT'
})
.constant('EYE_DISEASES', [
    'OP_CATARACT', 'OL_CATARACT', 'OP_GLAUCOMA', 'OL_GLAUCOMA'
])
.constant('GENERAL_DISEASES', [
    'DIABETES', 'HYPERTENSION', 'HYPERTHYROIDISM', 'HYPOTHYROIDISM', 'RZS'
])
.constant('UNIT_BN_BS', [
    'DEGREES', 'PDPTR_BN', 'PDPTR_BS'
])
.constant('UNIT_BD_BG', [
    'DEGREES', 'PDPTR_BD', 'PDPTR_BG'
])
.constant('CORRECTION_CATEGORY', [
    'GLASSES', 'CONTACT_LENSES', 'RGP_LENSES', 'AUTOREFRAKTOMETR'
])
.constant('CORRECTION_BASE', [
    'BN', 'BS', 'BG', 'BD', 'INT'
])
.constant('PLUS_MINUS_FLAG', {
    true: 'PLUS',
    false: 'MINUS'
})
.constant('AUTHORITY', {    // kolejność jest WAŻNA (im bliżej 0, tym user ma więcej uprawnień)
    0: 'SUPER_USER',
    1: 'READ_WRITE',
    2: 'READ_ONLY'
});