'use strict';

var app = angular.module('app', [
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap',
    'ngResource',
    'ui.router',
    'ngTouch',
    'pascalprecht.translate',
    'toaster',
    'ngMessages',
    'ui.select',
    'infinite-scroll',
    'ngTagsInput',
    'ui.bootstrap-slider'
]);

var currentUser = null;

angular.element(document).ready(function () {
    var injector = angular.injector(["ng"]);
    var $http = injector.get("$http");

    $http.get("/api/users/me").then(function(response) {
        currentUser = response.data;
        angular.bootstrap(document, ['app']);

    }, function(error) {
        currentUser = null;
        angular.bootstrap(document, ['app']);

    });

});

app.run(['$rootScope', 'Auth', function ($rootScope, Auth) {

    Auth.setAuthentication(currentUser);

}]);