CREATE TABLE `PasswordResetToken` (
  `token` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `verified` bit(1) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `VerificationToken` (
  `token` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `verified` bit(1) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `User` (
  `username` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `passwordResetTokenId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `verificationTokenId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`username`),
  FOREIGN KEY (`passwordResetTokenId`) REFERENCES `PasswordResetToken` (`token`),
  FOREIGN KEY (`verificationTokenId`) REFERENCES `VerificationToken` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Amplitude` (
  `amplitudeId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `ol` double DEFAULT NULL,
  `op` double DEFAULT NULL,
  `ou` double DEFAULT NULL,
  PRIMARY KEY (`amplitudeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Flipper` (
  `flipperId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `problem` bit(1) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`flipperId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Accommodation` (
  `accommodationId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `fcc` double DEFAULT NULL,
  `nra` double DEFAULT NULL,
  `pra` double DEFAULT NULL,
  `amplitudeId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `flipperId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`accommodationId`),
  FOREIGN KEY (`flipperId`) REFERENCES `Flipper` (`flipperId`),
  FOREIGN KEY (`amplitudeId`) REFERENCES `Amplitude` (`amplitudeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Address` (
  `addressId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `houseNumber` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `postalCode` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Authority` (
  `User` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `authorities` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  FOREIGN KEY (`User`) REFERENCES `User` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `NoCorrectionTest` (
  `noCorrectionTestId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `visusOL` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `visusOP` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `visusOU` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`noCorrectionTestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `CorrectionDesc` (
  `correctionDescId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `add` double DEFAULT NULL,
  `axis` int(11) DEFAULT NULL,
  `base` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `baseValue` int(11) DEFAULT NULL,
  `cyl` double DEFAULT NULL,
  `pd` double DEFAULT NULL,
  `pryzm` double DEFAULT NULL,
  `sf` double DEFAULT NULL,
  `visus` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`correctionDescId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `EsoExo` (
  `Id` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `FusionalVergence` (
  `fusionalVergenceId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `haze` double DEFAULT NULL,
  `reconstruction` double DEFAULT NULL,
  `rupture` double DEFAULT NULL,
  PRIMARY KEY (`fusionalVergenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `HyperHypo` (
  `Id` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `KrimskyTest` (
  `Id` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Maddox` (
  `maddoxId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `esoExoId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `hyperHypoId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`maddoxId`),
  FOREIGN KEY (`esoExoId`) REFERENCES `EsoExo` (`id`),
  FOREIGN KEY (`hyperHypoId`) REFERENCES `HyperHypo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Patient` (
  `patientId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `addressId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `createdBy` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `modifiedBy` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`patientId`),
  FOREIGN KEY (`addressId`) REFERENCES `Address` (`addressId`),
  FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`username`),
  FOREIGN KEY (`createdBy`) REFERENCES `User` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Pbk` (
  `pbkId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `denominator` int(11) DEFAULT NULL,
  `numerator` int(11) DEFAULT NULL,
  PRIMARY KEY (`pbkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `SynoptoforTest` (
  `synoptoforTestId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `f` bit(1) DEFAULT NULL,
  `jp` bit(1) DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `objectiveAngle` int(11) DEFAULT NULL,
  `st` bit(1) DEFAULT NULL,
  `subjectiveAngle` int(11) DEFAULT NULL,
  PRIMARY KEY (`synoptoforTestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `VonGraefeScore` (
  `vonGraefeScoreId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `esoExoId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `hyperHypoId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `nfvId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `pfvId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`vonGraefeScoreId`),
  FOREIGN KEY (`esoExoId`) REFERENCES `EsoExo` (`id`),
  FOREIGN KEY (`pfvId`) REFERENCES `FusionalVergence` (`fusionalVergenceId`),
  FOREIGN KEY (`hyperHypoId`) REFERENCES `HyperHypo` (`id`),
  FOREIGN KEY (`nfvId`) REFERENCES `FusionalVergence` (`fusionalVergenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `VonGraefeTest` (
  `vonGraefeTestId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `farScoreId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `nearScoreId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`vonGraefeTestId`),
  FOREIGN KEY (`farScoreId`) REFERENCES `VonGraefeScore` (`vonGraefeScoreId`),
  FOREIGN KEY (`nearScoreId`) REFERENCES `VonGraefeScore` (`vonGraefeScoreId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `WorthsTest` (
  `worthsTestId` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `content` longblob,
  PRIMARY KEY (`worthsTestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `SchobersTest` (
  `schobersTestId` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `content` longblob,
  PRIMARY KEY (`schobersTestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `BinocularVision` (
  `binocularVisionId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `ct` bit(1) DEFAULT NULL,
  `cut` bit(1) DEFAULT NULL,
  `hirschbergTest` bit(1) DEFAULT NULL,
  `nineDirectionsGlance` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `others` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `worthsTestId` VARCHAR(255) DEFAULT NULL,
  `schobersTestId` VARCHAR(255) DEFAULT NULL,
  `krimskyTestId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `maddoxCrossId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `maddoxWingId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `pbkId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `vonGraefeTestId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`binocularVisionId`),
  FOREIGN KEY (`krimskyTestId`) REFERENCES `KrimskyTest` (`id`),
  FOREIGN KEY (`maddoxCrossId`) REFERENCES `Maddox` (`maddoxId`),
  FOREIGN KEY (`pbkId`) REFERENCES `Pbk` (`pbkId`),
  FOREIGN KEY (`vonGraefeTestId`) REFERENCES `VonGraefeTest` (`vonGraefeTestId`),
  FOREIGN KEY (`maddoxWingId`) REFERENCES `Maddox` (`maddoxId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `BasicInformation` (
  `basicInformationId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `diagnosis` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `eyeDiseaseHistory` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `lastExaminationDate` datetime DEFAULT NULL,
  `medications` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `otherAilments` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `overallHealth` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `recommendations` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `visionCorrectionHistory` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `visitReason` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `noCorrectionTestId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`basicInformationId`),
  FOREIGN KEY (`noCorrectionTestId`) REFERENCES `NoCorrectionTest` (`noCorrectionTestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `GeneralDisease` (
  `BasicInformation` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `generalDiseases` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  FOREIGN KEY (`BasicInformation`) REFERENCES `BasicInformation` (`basicInformationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `EyeDisease` (
  `BasicInformation` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `eyeDiseases` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
FOREIGN KEY (`BasicInformation`) REFERENCES `BasicInformation` (`basicInformationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Examination` (
  `examinationId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `accommodationId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `basicInformationId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `binocularVisionId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `createdBy` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `modifiedBy` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `patientId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `synoptoforTestId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`examinationId`),
  FOREIGN KEY (`synoptoforTestId`) REFERENCES `SynoptoforTest` (`synoptoforTestId`),
  FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`username`),
  FOREIGN KEY (`createdBy`) REFERENCES `User` (`username`),
  FOREIGN KEY (`accommodationId`) REFERENCES `Accommodation` (`accommodationId`),
  FOREIGN KEY (`basicInformationId`) REFERENCES `BasicInformation` (`basicInformationId`),
  FOREIGN KEY (`patientId`) REFERENCES `Patient` (`patientId`),
  FOREIGN KEY (`binocularVisionId`) REFERENCES `BinocularVision` (`binocularVisionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

CREATE TABLE `Correction` (
  `correctionId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `vd` int(11) DEFAULT NULL,
  `visusOu` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `olDescId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `opDescId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  `examinationId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`correctionId`),
  FOREIGN KEY (`examinationId`) REFERENCES `Examination` (`examinationId`),
  FOREIGN KEY (`opDescId`) REFERENCES `CorrectionDesc` (`correctionDescId`),
  FOREIGN KEY (`olDescId`) REFERENCES `CorrectionDesc` (`correctionDescId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

ALTER TABLE `PatientPortal`.`Patient` ADD INDEX (`firstname` ASC);
ALTER TABLE `PatientPortal`.`Patient` ADD INDEX (`surname` ASC);
ALTER TABLE `PatientPortal`.`Address` ADD INDEX (`country` ASC);
ALTER TABLE `PatientPortal`.`Address` ADD INDEX (`city` ASC);

alter table Patient add column personalNumber varchar(255);

CREATE TABLE `Representative` (
  `representativeId` varchar(36) COLLATE utf8_polish_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `addressId` varchar(36) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`representativeId`),
  FOREIGN KEY (`addressId`) REFERENCES `Address` (`addressId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;


ALTER TABLE Patient ADD COLUMN representativeId varchar(36) COLLATE utf8_polish_ci;
ALTER TABLE Patient ADD CONSTRAINT fk_patient_representative_id FOREIGN KEY (representativeId) REFERENCES Representative(representativeId);