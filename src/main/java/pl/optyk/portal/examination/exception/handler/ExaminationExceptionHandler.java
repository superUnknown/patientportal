package pl.optyk.portal.examination.exception.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.optyk.portal.common.exception.handler.RestException;
import pl.optyk.portal.examination.exception.ExaminationNotCorrectionException;
import pl.optyk.portal.examination.exception.ExaminationNotFoundException;

/**
 * Handler RESTowy wyjątków badań pacjentów.
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExaminationExceptionHandler {
    private static final Logger log = LogManager.getLogger(ExaminationExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(ExaminationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestException examinationNotFoundExceptionHandler(ExaminationNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.NOT_FOUND.value(), "examination_not_found", "Required examination not found.");
    }

    @ResponseBody
    @ExceptionHandler(ExaminationNotCorrectionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public RestException examinationNotCorrectionExceptionHandler(ExaminationNotCorrectionException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.CONFLICT.value(), "examination_not_a_correction", "Given examination is not a correction.");
    }

}
