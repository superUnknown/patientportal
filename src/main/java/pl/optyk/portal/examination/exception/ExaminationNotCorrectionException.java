package pl.optyk.portal.examination.exception;

public class ExaminationNotCorrectionException extends Exception {
    public ExaminationNotCorrectionException(String msg) {
        super(msg);
    }
}
