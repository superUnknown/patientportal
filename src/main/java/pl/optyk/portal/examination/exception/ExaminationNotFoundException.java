package pl.optyk.portal.examination.exception;

/**
 * Created by superUnknown on 15.10.2016.
 */
public class ExaminationNotFoundException extends Exception {
    public ExaminationNotFoundException(String msg) {
        super(msg);
    }
}
