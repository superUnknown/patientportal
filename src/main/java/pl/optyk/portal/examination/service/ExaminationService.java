package pl.optyk.portal.examination.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.examination.dto.*;
import pl.optyk.portal.examination.exception.ExaminationNotCorrectionException;
import pl.optyk.portal.examination.exception.ExaminationNotFoundException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Usługa do zarządzania badaniami pacjentów.
 * Created by superUnknown on 15.10.2016.
 */
@Component
public interface ExaminationService {

    /**
     * Wyciąganie badania po id. Flaga extended oznacza, czy wyciągnąć
     * wszystkie informacje o badaniu.
     *
     * @param id
     * @param extended
     * @return
     */
    <T extends ExaminationDto> T getExamination(UUID id, boolean extended) throws ExaminationNotFoundException;

    /**
     * Tworzenie nowego badania dla podanego pacjenta. Dostęp do operacji posiada
     * zalogowany użytkownik z prawami zapisu.
     *
     * @param patientId
     * @param dto
     * @return
     */
    ExaminationDto createExamination(UUID patientId, ExaminationSaveDto dto) throws PatientNotFoundException, ImageProcessingException;

    /**
     * Modyfikacja podanego badania pacjenta. Dostęp do operacji posiada
     * zalogowany użytkownik z prawami zapisu.
     *
     * @param examinationId
     * @param dto
     * @return
     */
    ExaminationDto modifyExamination(UUID examinationId, ExaminationSaveDto dto) throws ExaminationNotFoundException, ImageProcessingException;

    /**
     * Pobieranie badań pacjenta.
     *
     * @param patientId
     * @param page
     * @param qty
     * @return
     */
    Page<ExaminationDto> getPatientExaminations(UUID patientId, Integer page, Integer qty) throws PatientNotFoundException;

    /**
     * Pobieranie korekcji badania.
     *
     * @param examinationId
     * @return
     */
    List<CorrectionDto> getExaminationCorrections(UUID examinationId) throws ExaminationNotFoundException;

    /**
     * Pobiera datę ostatniego badania pacjenta. Jeżeli pacjent nie ma żadnych badań, zwracany jest null.
     *
     * @param patientId
     * @return
     */
    Date getPatientsLastExaminationDate(UUID patientId) throws PatientNotFoundException;

    /**
     * Wyciąga test Schober'a po id.
     *
     * @param uuid
     * @return
     */
    SchobersTestDto getSchobersTest(UUID uuid);

    /**
     * Wyciąga test Worth'a po id.
     *
     * @param uuid
     * @return
     */
    WorthsTestDto getWorthsTest(UUID uuid);

    /**
     * Pobiera listę dostępnych testów Schober'a.
     *
     * @return
     */
    List<SchobersTestDto> getSchobersTests();

    /**
     * Pobiera listę dostępnych testów Worth'a.
     *
     * @return
     */
    List<WorthsTestDto> getWorthsTests();

    /**
     * Tworzenie nowego badania dla podanego pacjenta typu korekcja. Dostęp do operacji posiada
     * zalogowany użytkownik z prawami zapisu. Korekcja może zostać utworzona przed wypełnieniem
     * pełnego badania. Następnie może zostać przekształcone w normalne badanie (przez wywołanie
     * modyfikacji badania).
     *
     * @param patientId
     * @param corrections
     * @return
     */
    ExaminationDto createCorrection(UUID patientId, List<CorrectionDto> corrections) throws PatientNotFoundException;

    /**
     * Modyfikacja badania typu korekcja. Dostęp do operacji posiada zalogowany użytkownik z prawami zapisu.
     *
     * @param examinationId
     * @param corrections
     * @return
     */
    ExaminationDto modifyCorrection(UUID examinationId, List<CorrectionDto> corrections) throws ExaminationNotFoundException, ExaminationNotCorrectionException;

    /**
     * Pobiera listę podpowiedzi nazw dokumentów, które wcześniej były dodane.
     *
     * @return
     */
    List<String> getAllDocumentNames();

}
