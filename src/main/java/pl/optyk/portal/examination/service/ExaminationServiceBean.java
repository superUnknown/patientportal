package pl.optyk.portal.examination.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.examination.dto.*;
import pl.optyk.portal.examination.exception.ExaminationNotCorrectionException;
import pl.optyk.portal.examination.exception.ExaminationNotFoundException;
import pl.optyk.portal.examination.mapper.CorrectionMapper;
import pl.optyk.portal.examination.mapper.ExaminationMapper;
import pl.optyk.portal.examination.model.*;
import pl.optyk.portal.examination.repository.ExaminationRepository;
import pl.optyk.portal.examination.utils.ExaminationValidator;
import pl.optyk.portal.patient.exception.PatientNotFoundException;
import pl.optyk.portal.patient.model.Patient;
import pl.optyk.portal.patient.repository.PatientRepository;
import pl.optyk.portal.user.model.User;
import pl.optyk.portal.user.repository.UserRepository;

import java.util.*;

@Service
public class ExaminationServiceBean implements ExaminationService {

    private static final Logger log = LogManager.getLogger(ExaminationServiceBean.class);

    private final ExaminationRepository examinationRepository;

    private final PatientRepository patientRepository;

    private final UserRepository userRepository;

    @Autowired
    public ExaminationServiceBean(ExaminationRepository examinationRepository, PatientRepository patientRepository, UserRepository userRepository) {
        this.examinationRepository = examinationRepository;
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public <T extends ExaminationDto> T getExamination(UUID id, boolean extended) throws ExaminationNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam dane badania id: {}, extended: {}", id, extended);

        if (id == null) {
            throw new IllegalArgumentException("Required examination id is missing.");
        }

        Examination examination = examinationRepository.getExamination(id);
        if (examination == null) {
            throw new ExaminationNotFoundException("Required examination not found.");
        }

        return (T) (extended ?
                ExaminationMapper.entity2Dto(examination, new ExaminationExtendedDto()) :
                ExaminationMapper.entity2Dto(examination, new ExaminationDto()));

    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ExaminationDto createExamination(UUID patientId, ExaminationSaveDto dto) throws PatientNotFoundException, ImageProcessingException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Tworzenie nowego badania przez {} dla pacjenta id: {}: {}", currentUser.getUsername(), patientId, dto);

        if (patientId == null) {
            throw new IllegalArgumentException("Required patient id is missing.");
        }

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        Examination examination = ExaminationMapper.dto2Entity(dto, new Examination());

        setWorthsTest(examination, dto.getBinocularVision().getWorthsTestId());
        setSchobersTest(examination, dto.getBinocularVision().getSchobersTestId());

        ExaminationValidator.validate(examination);

        examination.setCreated(Calendar.getInstance().getTime());
        examination.setType(Examination.ExaminationType.EXAMINATION);
        examination.setCreatedBy(currentUser);
        examination.setPatient(patient);

        examinationRepository.save(examination);

        ExaminationDto result = ExaminationMapper.entity2Dto(examination, new ExaminationDto());

        log.info("Tworzenie nowego badania dla pacjenta id: {} zakończone poprawnie: {}", patientId, result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public ExaminationDto modifyExamination(UUID examinationId, ExaminationSaveDto dto) throws ExaminationNotFoundException, ImageProcessingException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Modyfikacja badania id: {} przez {}: {}", examinationId, currentUser.getUsername(), dto);

        if (examinationId == null) {
            throw new IllegalArgumentException("Required examination id is missing.");
        }

        Examination examination = examinationRepository.getExamination(examinationId);
        if (examination == null) {
            throw new ExaminationNotFoundException("Required examination not found.");
        }

        setWorthsTest(examination, dto.getBinocularVision().getWorthsTestId());
        setSchobersTest(examination, dto.getBinocularVision().getSchobersTestId());

        for (Correction correction : examination.getCorrections()) {
            examinationRepository.delete(correction);
        }

        for (Document document : examination.getDocuments()) {
            examinationRepository.delete(document);
        }

        ExaminationMapper.dto2Entity(dto, examination);
        ExaminationValidator.validate(examination);

        examination.setModified(Calendar.getInstance().getTime());
        examination.setType(Examination.ExaminationType.EXAMINATION);
        examination.setModifiedBy(currentUser);

        examinationRepository.save(examination);

        ExaminationDto result = ExaminationMapper.entity2Dto(examination, new ExaminationDto());

        log.info("Modyfikacja badania id: {} zakończone poprawnie: {}", examinationId, result);

        return result;
    }

    private void setWorthsTest(Examination examination, UUID worthsTestId) {
        if (worthsTestId == null) {
            examination.getBinocularVision().setWorthsTest(null);
            return;
        }

        WorthsTest worthsTest = examinationRepository.getWorthsTest(worthsTestId);
        if (worthsTest == null) {
            throw new IllegalArgumentException("Given Worth's Test not found.");
        }
        examination.getBinocularVision().setWorthsTest(worthsTest);

    }

    private void setSchobersTest(Examination examination, UUID schobersTestId) {
        if (schobersTestId == null) {
            examination.getBinocularVision().setSchobersTest(null);
            return;
        }

        SchobersTest schobersTest = examinationRepository.getSchobersTest(schobersTestId);
        if (schobersTest == null) {
            throw new IllegalArgumentException("Given Schober's Test not found.");
        }
        examination.getBinocularVision().setSchobersTest(schobersTest);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<ExaminationDto> getPatientExaminations(UUID patientId, Integer page, Integer qty) throws PatientNotFoundException {
        if (log.isDebugEnabled())
            log.debug("Wyszukiwanie badań pacjenta: { patientId: {}, page: {}, qty: {} }", patientId, page, qty);

        if (patientId == null) {
            throw new IllegalArgumentException("Patient id is missing.");
        }

        if (page == null || page < 0) {
            throw new IllegalArgumentException("Page number is missing.");
        }

        if (qty == null || qty < 1) {
            throw new IllegalArgumentException("Quantity number is missing.");
        }

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        Page<Examination> examinations = examinationRepository.getPatientExaminations(patient, page, qty);

        if (log.isDebugEnabled())
            log.debug("Znaleziono badań: { page: {}, qty: {}, found: {} }", page, qty, examinations.getTotalElements());


        if (examinations.hasContent()) {
            List<ExaminationDto> result = new ArrayList<>();
            for (Examination examination : examinations.getContent()) {
                result.add(ExaminationMapper.entity2Dto(examination, new ExaminationDto()));
            }

            return new PageImpl<>(result, new PageRequest(page, qty), examinations.getTotalElements());
        }

        return new PageImpl<>(Collections.<ExaminationDto>emptyList());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<CorrectionDto> getExaminationCorrections(UUID examinationId) throws ExaminationNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam korekcje badania id: {}.", examinationId);

        if (examinationId == null) {
            throw new IllegalArgumentException("Required examination id is missing.");
        }

        Examination examination = examinationRepository.getExamination(examinationId);
        if (examination == null) {
            throw new ExaminationNotFoundException("Required examination not found.");
        }

        if (examination.getCorrections().isEmpty()) {
            return Collections.emptyList();
        }

        List<CorrectionDto> result = new ArrayList<>();
        for (Correction correction : examination.getCorrections()) {
            result.add(CorrectionMapper.entity2Dto(correction, new CorrectionDto()));
        }

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Date getPatientsLastExaminationDate(UUID patientId) throws PatientNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam datę ostatniego badania pacjenta id: {}.", patientId);

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        List<Examination> examinations = examinationRepository.getPatientExaminations(patient, 0, 1).getContent();
        if (examinations.isEmpty()) {
            return null;
        }

        return examinations.get(0).getBasicInformation().getDate();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public SchobersTestDto getSchobersTest(UUID uuid) {
        if (log.isDebugEnabled()) log.debug("Wyciągam test Schober'a id: {}.", uuid);

        if (uuid == null) {
            throw new IllegalArgumentException("Required Schobers test id is missing.");
        }

        SchobersTest schobersTest = examinationRepository.getSchobersTest(uuid);
        if (schobersTest == null) {
            throw new IllegalArgumentException("Required Schobers test not found.");
        }

        return new SchobersTestDto(schobersTest.getSchobersTestId(), schobersTest.getContent());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public WorthsTestDto getWorthsTest(UUID uuid) {
        if (log.isDebugEnabled()) log.debug("Wyciągam test Worth'a id: {}.", uuid);

        if (uuid == null) {
            throw new IllegalArgumentException("Required Worths test id is missing.");
        }

        WorthsTest worthsTest = examinationRepository.getWorthsTest(uuid);
        if (worthsTest == null) {
            throw new IllegalArgumentException("Required Worths test not found.");
        }

        return new WorthsTestDto(worthsTest.getWorthsTestId(), worthsTest.getContent());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<SchobersTestDto> getSchobersTests() {
        if (log.isDebugEnabled()) log.debug("Wyciągam listę dostępnych testów Schober'a.");

        List<SchobersTestDto> tests = new ArrayList<>();
        for (SchobersTest test : examinationRepository.getSchobersTests()) {
            tests.add(new SchobersTestDto(test.getSchobersTestId(), test.getContent()));
        }

        return tests;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<WorthsTestDto> getWorthsTests() {
        if (log.isDebugEnabled()) log.debug("Wyciągam listę dostępnych testów Worth'a.");

        List<WorthsTestDto> tests = new ArrayList<>();
        for (WorthsTest test : examinationRepository.getWorthsTests()) {
            tests.add(new WorthsTestDto(test.getWorthsTestId(), test.getContent()));
        }

        return tests;
    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public ExaminationDto createCorrection(UUID patientId, List<CorrectionDto> corrections) throws PatientNotFoundException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Tworzenie nowego badania typu korekcja przez {} dla pacjenta id: {}: {}", currentUser.getUsername(), patientId, corrections);

        if (patientId == null) {
            throw new IllegalArgumentException("Required patient id is missing.");
        }

        if (corrections == null || corrections.isEmpty()) {
            throw new IllegalArgumentException("Required corrections are missing.");
        }

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        Examination examination = new Examination();
        for (CorrectionDto correction : corrections) {
            examination.getCorrections().add(CorrectionMapper.dto2Entity(correction, new Correction()));
        }

        ExaminationValidator.validateCorrections(examination.getCorrections());

        examination.setCreated(Calendar.getInstance().getTime());
        examination.setType(Examination.ExaminationType.CORRECTION);
        examination.setCreatedBy(currentUser);
        examination.setPatient(patient);

        examinationRepository.save(examination);

        ExaminationDto result = ExaminationMapper.entity2Dto(examination, new ExaminationDto());

        log.info("Tworzenie nowego badania typu korekcja dla pacjenta id: {} zakończone poprawnie: {}", patientId, result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public ExaminationDto modifyCorrection(UUID examinationId, List<CorrectionDto> corrections) throws ExaminationNotFoundException, ExaminationNotCorrectionException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Modyfikacja badania typu korekcja id: {} przez {}: {}", examinationId, currentUser.getUsername(), corrections);

        if (examinationId == null) {
            throw new IllegalArgumentException("Required examination id is missing.");
        }

        if (corrections == null || corrections.isEmpty()) {
            throw new IllegalArgumentException("Required corrections are missing.");
        }

        Examination examination = examinationRepository.getExamination(examinationId);
        if (examination == null) {
            throw new ExaminationNotFoundException("Required examination not found.");
        }

        if (!examination.getType().equals(Examination.ExaminationType.CORRECTION)) {
            throw new ExaminationNotCorrectionException("Given examination is not a correction.");
        }

        for (Correction correction : examination.getCorrections()) {
            examinationRepository.delete(correction);
        }

        examination.getCorrections().clear();
        for (CorrectionDto correction : corrections) {
            examination.getCorrections().add(CorrectionMapper.dto2Entity(correction, new Correction()));
        }

        ExaminationValidator.validateCorrections(examination.getCorrections());

        examination.setModified(Calendar.getInstance().getTime());
        examination.setModifiedBy(currentUser);

        examinationRepository.save(examination);

        ExaminationDto result = ExaminationMapper.entity2Dto(examination, new ExaminationDto());

        log.info("Modyfikacja badania typu korekcja id: {} zakończona poprawnie: {}", examinationId, result);

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllDocumentNames() {
        if (log.isDebugEnabled()) log.debug("Wyciągam listę podpowiedzi nazw dokumentów.");

        return examinationRepository.getAllDocumentNames();
    }

}
