package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.common.utils.ImageUtils;
import pl.optyk.portal.examination.dto.*;
import pl.optyk.portal.examination.model.Correction;
import pl.optyk.portal.examination.model.Document;
import pl.optyk.portal.examination.model.Examination;

/**
 * Mapper dla danych badania.
 */
public class ExaminationMapper {

    public static <T extends ExaminationDto> T entity2Dto(final Examination entity, final T dto) {
        if (entity == null) {
            return null;
        }

        dto.setExaminationId(entity.getExaminationId());
        dto.setCreated(entity.getCreated());
        dto.setType(entity.getType());
        dto.setCreatedBy(entity.getCreatedBy().getUsername());
        dto.setModified(entity.getModified());
        dto.setBasicInformation(BasicInformationMapper.entity2Dto(entity.getBasicInformation(), new BasicInformationDto()));

        if (entity.getModifiedBy() != null) dto.setModifiedBy(entity.getModifiedBy().getUsername());

        if (dto instanceof ExaminationExtendedDto) {

            if (!entity.getCorrections().isEmpty()) {
                for (Correction correction : entity.getCorrections()) {
                    ((ExaminationExtendedDto) dto).getCorrections().add(CorrectionMapper.entity2Dto(correction, new CorrectionDto()));
                }
            }

            if (!entity.getDocuments().isEmpty()) {
                for (Document document : entity.getDocuments()) {
                    ((ExaminationExtendedDto) dto).getDocuments().add(new DocumentDto(document.getName(), document.getContent()));
                }
            }

            ((ExaminationExtendedDto) dto).setBinocularVision(
                    BinocularVisionMapper.entity2Dto(entity.getBinocularVision(), new BinocularVisionDto())
            );

            ((ExaminationExtendedDto) dto).setSynoptoforTest(
                    SynoptoforTestMapper.entity2Dto(entity.getSynoptoforTest(), new SynoptoforTestDto())
            );

            ((ExaminationExtendedDto) dto).setAccommodation(
                    AccommodationMapper.entity2Dto(entity.getAccommodation(), new AccommodationDto())
            );

        }

        return dto;
    }

    public static Examination dto2Entity(final ExaminationSaveDto dto, final Examination entity) throws ImageProcessingException {
        if (dto == null) {
            return null;
        }

        BasicInformationMapper.dto2Entity(dto.getBasicInformation(), entity.getBasicInformation());
        BinocularVisionMapper.dto2Entity(dto.getBinocularVision(), entity.getBinocularVision());
        SynoptoforTestMapper.dto2Entity(dto.getSynoptoforTest(), entity.getSynoptoforTest());
        AccommodationMapper.dto2Entity(dto.getAccommodation(), entity.getAccommodation());

        entity.getCorrections().clear();
        for (CorrectionDto correction : dto.getCorrections()) {
            entity.getCorrections().add(CorrectionMapper.dto2Entity(correction, new Correction()));
        }

        entity.getDocuments().clear();
        for (DocumentSaveDto document : dto.getDocuments()) {
            entity.getDocuments().add(new Document(document.getName(), ImageUtils.decodeImage(document.getContent()), entity));
        }

        return entity;
    }

}
