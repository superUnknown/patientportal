package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.examination.utils.UnitNumber;
import pl.optyk.portal.examination.utils.UnitNumberDto;

public class UnitNumberMapper {

    static <T extends UnitNumber> void dto2Entity(UnitNumberDto dto, final T entity) {
        if (dto == null) {
            dto = new UnitNumberDto();
        }

        entity.setValue(dto.getValue());
        entity.setUnit(dto.getValue() != null ? dto.getUnit() : null);

    }

    static <T extends UnitNumberDto> void entity2Dto(final UnitNumber entity, final T dto) {
        if (entity == null) {
            return;
        }

        dto.setValue(entity.getValue());
        dto.setUnit(entity.getValue() != null ? entity.getUnit() : null);

    }

}
