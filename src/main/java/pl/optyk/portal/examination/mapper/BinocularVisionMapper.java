package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.examination.dto.BinocularVisionDto;
import pl.optyk.portal.examination.dto.VonGraefeScoreDto;
import pl.optyk.portal.examination.model.BinocularVision;
import pl.optyk.portal.examination.model.VonGraefeScore;

public class BinocularVisionMapper {

    static BinocularVision dto2Entity(BinocularVisionDto dto, final BinocularVision entity) throws ImageProcessingException {
        if (dto == null) {
            dto = new BinocularVisionDto();
        }

        entity.setCt(dto.getCt());
        entity.setCut(dto.getCut());
        entity.setHirschbergTest(dto.getHirschbergTest());
        entity.setNineDirectionsGlance(dto.getNineDirectionsGlance());
        entity.setOthers(dto.getNotes());

        VonGraefeScoreDto farScore = dto.getVonGraefeTest().getFarScore();
        UnitNumberMapper.dto2Entity(farScore.getEsoExo(), entity.getVonGraefeTest().getFarScore().getEsoExo());
        UnitNumberMapper.dto2Entity(farScore.getHyperHypo(), entity.getVonGraefeTest().getFarScore().getHyperHypo());
        entity.getVonGraefeTest().getFarScore().getNfv().setHaze(farScore.getNfv().getHaze());
        entity.getVonGraefeTest().getFarScore().getNfv().setRupture(farScore.getNfv().getRupture());
        entity.getVonGraefeTest().getFarScore().getNfv().setReconstruction(farScore.getNfv().getReconstruction());
        entity.getVonGraefeTest().getFarScore().getPfv().setHaze(farScore.getPfv().getHaze());
        entity.getVonGraefeTest().getFarScore().getPfv().setRupture(farScore.getPfv().getRupture());
        entity.getVonGraefeTest().getFarScore().getPfv().setReconstruction(farScore.getPfv().getReconstruction());

        VonGraefeScoreDto nearScore = dto.getVonGraefeTest().getNearScore();
        UnitNumberMapper.dto2Entity(nearScore.getEsoExo(), entity.getVonGraefeTest().getNearScore().getEsoExo());
        UnitNumberMapper.dto2Entity(nearScore.getHyperHypo(), entity.getVonGraefeTest().getNearScore().getHyperHypo());
        entity.getVonGraefeTest().getNearScore().getNfv().setHaze(nearScore.getNfv().getHaze());
        entity.getVonGraefeTest().getNearScore().getNfv().setRupture(nearScore.getNfv().getRupture());
        entity.getVonGraefeTest().getNearScore().getNfv().setReconstruction(nearScore.getNfv().getReconstruction());
        entity.getVonGraefeTest().getNearScore().getPfv().setHaze(nearScore.getPfv().getHaze());
        entity.getVonGraefeTest().getNearScore().getPfv().setRupture(nearScore.getPfv().getRupture());
        entity.getVonGraefeTest().getNearScore().getPfv().setReconstruction(nearScore.getPfv().getReconstruction());

        UnitNumberMapper.dto2Entity(dto.getMaddoxCross().getEsoExo(), entity.getMaddoxCross().getEsoExo());
        UnitNumberMapper.dto2Entity(dto.getMaddoxCross().getHyperHypo(), entity.getMaddoxCross().getHyperHypo());

        UnitNumberMapper.dto2Entity(dto.getMaddoxWing().getEsoExo(), entity.getMaddoxWing().getEsoExo());
        UnitNumberMapper.dto2Entity(dto.getMaddoxWing().getHyperHypo(), entity.getMaddoxWing().getHyperHypo());

        UnitNumberMapper.dto2Entity(dto.getKrimskyTest(), entity.getKrimskyTest());

        entity.getPbk().setNumerator(dto.getPbk().getNumerator());
        entity.getPbk().setDenominator(dto.getPbk().getDenominator());

        return entity;
    }

    static BinocularVisionDto entity2Dto(final BinocularVision entity, final BinocularVisionDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setCt(entity.getCt());
        dto.setCut(entity.getCut());
        dto.setHirschbergTest(entity.getHirschbergTest());
        dto.setNineDirectionsGlance(entity.getNineDirectionsGlance());
        dto.setNotes(entity.getOthers());
        dto.setSchobersTestId(entity.getSchobersTest() != null ? entity.getSchobersTest().getSchobersTestId() : null);
        dto.setWorthsTestId(entity.getWorthsTest() != null ? entity.getWorthsTest().getWorthsTestId() : null);

        VonGraefeScore farScore = entity.getVonGraefeTest().getFarScore();
        UnitNumberMapper.entity2Dto(farScore.getEsoExo(), dto.getVonGraefeTest().getFarScore().getEsoExo());
        UnitNumberMapper.entity2Dto(farScore.getHyperHypo(), dto.getVonGraefeTest().getFarScore().getHyperHypo());
        dto.getVonGraefeTest().getFarScore().getNfv().setHaze(farScore.getNfv().getHaze());
        dto.getVonGraefeTest().getFarScore().getNfv().setRupture(farScore.getNfv().getRupture());
        dto.getVonGraefeTest().getFarScore().getNfv().setReconstruction(farScore.getNfv().getReconstruction());
        dto.getVonGraefeTest().getFarScore().getPfv().setHaze(farScore.getPfv().getHaze());
        dto.getVonGraefeTest().getFarScore().getPfv().setRupture(farScore.getPfv().getRupture());
        dto.getVonGraefeTest().getFarScore().getPfv().setReconstruction(farScore.getPfv().getReconstruction());

        VonGraefeScore nearScore = entity.getVonGraefeTest().getNearScore();
        UnitNumberMapper.entity2Dto(nearScore.getEsoExo(), dto.getVonGraefeTest().getNearScore().getEsoExo());
        UnitNumberMapper.entity2Dto(nearScore.getHyperHypo(), dto.getVonGraefeTest().getNearScore().getHyperHypo());
        dto.getVonGraefeTest().getNearScore().getNfv().setHaze(nearScore.getNfv().getHaze());
        dto.getVonGraefeTest().getNearScore().getNfv().setRupture(nearScore.getNfv().getRupture());
        dto.getVonGraefeTest().getNearScore().getNfv().setReconstruction(nearScore.getNfv().getReconstruction());
        dto.getVonGraefeTest().getNearScore().getPfv().setHaze(nearScore.getPfv().getHaze());
        dto.getVonGraefeTest().getNearScore().getPfv().setRupture(nearScore.getPfv().getRupture());
        dto.getVonGraefeTest().getNearScore().getPfv().setReconstruction(nearScore.getPfv().getReconstruction());

        UnitNumberMapper.entity2Dto(entity.getMaddoxCross().getEsoExo(), dto.getMaddoxCross().getEsoExo());
        UnitNumberMapper.entity2Dto(entity.getMaddoxCross().getHyperHypo(), dto.getMaddoxCross().getHyperHypo());

        UnitNumberMapper.entity2Dto(entity.getMaddoxWing().getEsoExo(), dto.getMaddoxWing().getEsoExo());
        UnitNumberMapper.entity2Dto(entity.getMaddoxWing().getHyperHypo(), dto.getMaddoxWing().getHyperHypo());

        UnitNumberMapper.entity2Dto(entity.getKrimskyTest(), dto.getKrimskyTest());

        dto.getPbk().setNumerator(entity.getPbk().getNumerator());
        dto.getPbk().setDenominator(entity.getPbk().getDenominator());

        return dto;
    }

}
