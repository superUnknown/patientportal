package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.examination.dto.SynoptoforTestDto;
import pl.optyk.portal.examination.model.SynoptoforTest;

public class SynoptoforTestMapper {

    static SynoptoforTest dto2Entity(SynoptoforTestDto dto, final SynoptoforTest entity) {
        if (dto == null) {
            dto = new SynoptoforTestDto();
        }

        entity.setObjectiveAngle(dto.getObjectiveAngle());
        entity.setSubjectiveAngle(dto.getSubjectiveAngle());
        entity.setJp(dto.getJp());
        entity.setF(dto.getF());
        entity.setSt(dto.getSt());
        entity.setNotes(dto.getNotes());

        return entity;
    }

    public static SynoptoforTestDto entity2Dto(final SynoptoforTest entity, final SynoptoforTestDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setObjectiveAngle(entity.getObjectiveAngle());
        dto.setSubjectiveAngle(entity.getSubjectiveAngle());
        dto.setJp(entity.getJp());
        dto.setF(entity.getF());
        dto.setSt(entity.getSt());
        dto.setNotes(entity.getNotes());

        return dto;
    }

}
