package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.examination.dto.AccommodationDto;
import pl.optyk.portal.examination.model.Accommodation;

public class AccommodationMapper {

    static Accommodation dto2Entity(AccommodationDto dto, final Accommodation entity) {
        if (dto == null) {
            dto = new AccommodationDto();
        }

        entity.setFcc(dto.getFcc());
        entity.setPra(dto.getPra());
        entity.setNra(dto.getNra());
        entity.setNotes(dto.getNotes());

        entity.getFlipper().setValue(dto.getFlipper().getValue());
        entity.getFlipper().setProblem(dto.getFlipper().getValue() != null ? dto.getFlipper().getProblem() : null);

        entity.getAmplitude().setOl(dto.getAmplitude().getOl());
        entity.getAmplitude().setOp(dto.getAmplitude().getOp());
        entity.getAmplitude().setOu(dto.getAmplitude().getOu());

        return entity;
    }

    static AccommodationDto entity2Dto(final Accommodation entity, final AccommodationDto dto) {
        if (dto == null) {
            return null;
        }

        dto.setFcc(entity.getFcc());
        dto.setPra(entity.getPra());
        dto.setNra(entity.getNra());
        dto.setNotes(entity.getNotes());

        dto.getFlipper().setValue(entity.getFlipper().getValue());
        dto.getFlipper().setProblem(entity.getFlipper().getValue() != null ? entity.getFlipper().getProblem() : null);

        dto.getAmplitude().setOl(entity.getAmplitude().getOl());
        dto.getAmplitude().setOp(entity.getAmplitude().getOp());
        dto.getAmplitude().setOu(entity.getAmplitude().getOu());

        return dto;
    }

}
