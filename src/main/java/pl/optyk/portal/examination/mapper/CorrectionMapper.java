package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.examination.dto.CorrectionDto;
import pl.optyk.portal.examination.model.Correction;

public class CorrectionMapper {

    public static CorrectionDto entity2Dto(final Correction entity, final CorrectionDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setCategory(entity.getCategory());
        dto.setNotes(entity.getNotes());
        dto.setVd(entity.getVd());
        dto.setVisusOu(entity.getVisusOu());

        dto.getOpDesc().setSf(entity.getOpDesc().getSf());
        dto.getOpDesc().setCyl(entity.getOpDesc().getCyl());
        dto.getOpDesc().setAxis(entity.getOpDesc().getAxis());
        dto.getOpDesc().setAdd(entity.getOpDesc().getAdd());
        dto.getOpDesc().setPd(entity.getOpDesc().getPd());
        dto.getOpDesc().setPryzm(entity.getOpDesc().getPryzm());
        dto.getOpDesc().setBase(entity.getOpDesc().getBase());
        dto.getOpDesc().setBaseValue(entity.getOpDesc().getBaseValue());
        dto.getOpDesc().setVisus(entity.getOpDesc().getVisus());

        dto.getOlDesc().setSf(entity.getOlDesc().getSf());
        dto.getOlDesc().setCyl(entity.getOlDesc().getCyl());
        dto.getOlDesc().setAxis(entity.getOlDesc().getAxis());
        dto.getOlDesc().setAdd(entity.getOlDesc().getAdd());
        dto.getOlDesc().setPd(entity.getOlDesc().getPd());
        dto.getOlDesc().setPryzm(entity.getOlDesc().getPryzm());
        dto.getOlDesc().setBase(entity.getOlDesc().getBase());
        dto.getOlDesc().setBaseValue(entity.getOlDesc().getBaseValue());
        dto.getOlDesc().setVisus(entity.getOlDesc().getVisus());

        return dto;
    }

    public static Correction dto2Entity(CorrectionDto dto, final Correction entity) {
        if (dto == null) {
            dto = new CorrectionDto();
        }

        entity.setCategory(dto.getCategory());
        entity.setNotes(dto.getNotes());
        entity.setVd(dto.getVd());
        entity.setVisusOu(dto.getVisusOu());

        entity.getOpDesc().setSf(dto.getOpDesc().getSf());
        entity.getOpDesc().setCyl(dto.getOpDesc().getCyl());
        entity.getOpDesc().setAxis(dto.getOpDesc().getAxis());
        entity.getOpDesc().setAdd(dto.getOpDesc().getAdd());
        entity.getOpDesc().setPd(dto.getOpDesc().getPd());
        entity.getOpDesc().setPryzm(dto.getOpDesc().getPryzm());
        entity.getOpDesc().setBase(dto.getOpDesc().getBase());
        entity.getOpDesc().setBaseValue(dto.getOpDesc().getBaseValue());
        entity.getOpDesc().setVisus(dto.getOpDesc().getVisus());

        entity.getOlDesc().setSf(dto.getOlDesc().getSf());
        entity.getOlDesc().setCyl(dto.getOlDesc().getCyl());
        entity.getOlDesc().setAxis(dto.getOlDesc().getAxis());
        entity.getOlDesc().setAdd(dto.getOlDesc().getAdd());
        entity.getOlDesc().setPd(dto.getOlDesc().getPd());
        entity.getOlDesc().setPryzm(dto.getOlDesc().getPryzm());
        entity.getOlDesc().setBase(dto.getOlDesc().getBase());
        entity.getOlDesc().setBaseValue(dto.getOlDesc().getBaseValue());
        entity.getOlDesc().setVisus(dto.getOlDesc().getVisus());

        return entity;
    }

}
