package pl.optyk.portal.examination.mapper;


import pl.optyk.portal.common.utils.DateUtils;
import pl.optyk.portal.examination.dto.BasicInformationDto;
import pl.optyk.portal.examination.model.BasicInformation;

public class BasicInformationMapper {

    static BasicInformation dto2Entity(BasicInformationDto dto, final BasicInformation entity) {
        if (dto == null) {
            dto = new BasicInformationDto();
        }

        entity.setDate(DateUtils.withoutTime(dto.getDate()));
        entity.setVisitReason(dto.getVisitReason());
        entity.setOtherAilments(dto.getOtherAilments());
        entity.setVisionCorrectionHistory(dto.getVisionCorrectionHistory());
        entity.setEyeDiseaseHistory(dto.getEyeDiseaseHistory());
        entity.setDiagnosis(dto.getDiagnosis());
        entity.setRecommendations(dto.getRecommendations());
        entity.setOverallHealth(dto.getOverallHealth());
        entity.setMedications(dto.getMedications());
        entity.setNotes(dto.getNotes());
        entity.setAmslerTestOP(dto.getAmslerTestOP());
        entity.setAmslerTestOL(dto.getAmslerTestOL());
        entity.setEyeDiseases(dto.getEyeDiseases());
        entity.setGeneralDiseases(dto.getGeneralDiseases());
        entity.setLastExaminationDate(dto.getLastExaminationDate() != null ? DateUtils.withoutTime(dto.getLastExaminationDate()) : null);
        entity.setLastExaminationDateNote(dto.getLastExaminationDateNote() == null || dto.getLastExaminationDateNote().trim().isEmpty() ? null : dto.getLastExaminationDateNote());

        entity.getNoCorrectionTest().setVisusOL(dto.getNoCorrectionTest().getVisusOL());
        entity.getNoCorrectionTest().setVisusOP(dto.getNoCorrectionTest().getVisusOP());
        entity.getNoCorrectionTest().setVisusOU(dto.getNoCorrectionTest().getVisusOU());

        return entity;
    }

    public static BasicInformationDto entity2Dto(BasicInformation entity, BasicInformationDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setDate(entity.getDate());
        dto.setMedications(entity.getMedications());
        dto.setNotes(entity.getNotes());
        dto.setAmslerTestOP(entity.getAmslerTestOP());
        dto.setAmslerTestOL(entity.getAmslerTestOL());
        dto.setDiagnosis(entity.getDiagnosis());
        dto.setRecommendations(entity.getRecommendations());
        dto.setEyeDiseaseHistory(entity.getEyeDiseaseHistory());
        dto.setEyeDiseases(entity.getEyeDiseases());
        dto.setGeneralDiseases(entity.getGeneralDiseases());
        dto.setLastExaminationDate(entity.getLastExaminationDate());
        dto.setLastExaminationDateNote(entity.getLastExaminationDateNote());
        dto.setOtherAilments(entity.getOtherAilments());
        dto.setGeneralDiseases(entity.getGeneralDiseases());
        dto.setVisionCorrectionHistory(entity.getVisionCorrectionHistory());
        dto.setVisitReason(entity.getVisitReason());
        dto.setOverallHealth(entity.getOverallHealth());

        dto.getNoCorrectionTest().setVisusOL(entity.getNoCorrectionTest().getVisusOL());
        dto.getNoCorrectionTest().setVisusOP(entity.getNoCorrectionTest().getVisusOP());
        dto.getNoCorrectionTest().setVisusOU(entity.getNoCorrectionTest().getVisusOU());

        return dto;
    }
}
