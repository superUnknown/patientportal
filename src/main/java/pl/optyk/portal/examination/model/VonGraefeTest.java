package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Test Von Graefe'a.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "VonGraefeTest")
public class VonGraefeTest {

    /**
     * Id.
     */
    private UUID vonGraefeTestId = UUID.randomUUID();

    /**
     * Wynik badania "w bliż".
     */
    private VonGraefeScore nearScore = new VonGraefeScore();

    /**
     * Wynik badania "w dal".
     */
    private VonGraefeScore farScore = new VonGraefeScore();

    @Id
    @Type(type = "uuid-char")
    public UUID getVonGraefeTestId() {
        return vonGraefeTestId;
    }

    public void setVonGraefeTestId(UUID vonGraefeTestId) {
        this.vonGraefeTestId = vonGraefeTestId;
    }

    @JoinColumn(name = "nearScoreId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public VonGraefeScore getNearScore() {
        return nearScore;
    }

    public void setNearScore(VonGraefeScore nearScore) {
        this.nearScore = nearScore;
    }

    @JoinColumn(name = "farScoreId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public VonGraefeScore getFarScore() {
        return farScore;
    }

    public void setFarScore(VonGraefeScore farScore) {
        this.farScore = farScore;
    }

}
