package pl.optyk.portal.examination.model;

import pl.optyk.portal.examination.utils.UnitNumber;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Wartość testu Krimsky'iego.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "KrimskyTest")
@Inheritance(strategy = InheritanceType.JOINED)
public class KrimskyTest extends UnitNumber {

    public KrimskyTest() {
    }

    public KrimskyTest(Double value, Unit unit) {
        super(value, unit);
    }
}
