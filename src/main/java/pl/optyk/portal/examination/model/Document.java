package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Encja załączonego dokumentu do badania.
 * Created by superunknown on 03.12.2016.
 */
@Entity
@Table(name = "Document")
public class Document {

    /**
     * Id.
     */
    private UUID documentId = UUID.randomUUID();

    /**
     * Nazwa/typ
     */
    private String name;

    /**
     * Content dokumentu.
     */
    private byte[] content;

    /**
     * Badanie
     */
    private Examination examination;

    public Document() {
    }

    public Document(String name, byte[] content, Examination examination) {
        this.name = name;
        this.content = content;
        this.examination = examination;
    }

    @Id
    @Type(type = "uuid-char")
    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(columnDefinition = "longblob")
    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @ManyToOne
    @JoinColumn(name = "examinationId")
    public Examination getExamination() {
        return examination;
    }

    public void setExamination(Examination examination) {
        this.examination = examination;
    }

}
