package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Encja informacji o badaniu z części widzenia obuocznego.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "BinocularVision")
public class BinocularVision {

    /**
     * Id.
     */
    private UUID binocularVisionId = UUID.randomUUID();

    /**
     * CT
     */
    private Boolean ct;

    /**
     * CUT
     */
    private Boolean cut;

    /**
     * Test Hirschberg'a.
     */
    private Boolean hirschbergTest;

    /**
     * Opis 9 kierunków spojrzenia.
     */
    private String nineDirectionsGlance;

    /**
     * Inne uwagi.
     */
    private String others;

    /**
     * Test Worth'a (grafika).
     */
    private WorthsTest worthsTest;

    /**
     * Test Schober'a (grafika).
     */
    private SchobersTest schobersTest;

    /**
     * Test Von Graefe'a.
     */
    private VonGraefeTest vonGraefeTest = new VonGraefeTest();

    /**
     * Test krzyża maddoxa.
     */
    private Maddox maddoxCross = new Maddox();

    /**
     * Test skrzydła maddoxa.
     */
    private Maddox maddoxWing = new Maddox();

    /**
     * Wartość testu Krimsky'iego.
     */
    private KrimskyTest krimskyTest = new KrimskyTest();

    /**
     * Wartość pkb (int/int).
     */
    private Pbk pbk = new Pbk();

    @Id
    @Type(type = "uuid-char")
    public UUID getBinocularVisionId() {
        return binocularVisionId;
    }

    public void setBinocularVisionId(UUID binocularVisionId) {
        this.binocularVisionId = binocularVisionId;
    }

    public Boolean getCt() {
        return ct;
    }

    public void setCt(Boolean ct) {
        this.ct = ct;
    }

    public Boolean getCut() {
        return cut;
    }

    public void setCut(Boolean cut) {
        this.cut = cut;
    }

    public Boolean getHirschbergTest() {
        return hirschbergTest;
    }

    public void setHirschbergTest(Boolean hirschbergTest) {
        this.hirschbergTest = hirschbergTest;
    }

    public String getNineDirectionsGlance() {
        return nineDirectionsGlance;
    }

    public void setNineDirectionsGlance(String nineDirectionsGlance) {
        this.nineDirectionsGlance = nineDirectionsGlance;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    @ManyToOne
    @JoinColumn(name = "worthsTestId")
    public WorthsTest getWorthsTest() {
        return worthsTest;
    }

    public void setWorthsTest(WorthsTest worthsTest) {
        this.worthsTest = worthsTest;
    }

    @ManyToOne
    @JoinColumn(name = "schobersTestId")
    public SchobersTest getSchobersTest() {
        return schobersTest;
    }

    public void setSchobersTest(SchobersTest schobersTest) {
        this.schobersTest = schobersTest;
    }

    @JoinColumn(name = "vonGraefeTestId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public VonGraefeTest getVonGraefeTest() {
        return vonGraefeTest;
    }

    public void setVonGraefeTest(VonGraefeTest vonGraefeTest) {
        this.vonGraefeTest = vonGraefeTest;
    }

    @JoinColumn(name = "maddoxCrossId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Maddox getMaddoxCross() {
        return maddoxCross;
    }

    public void setMaddoxCross(Maddox maddoxCross) {
        this.maddoxCross = maddoxCross;
    }

    @JoinColumn(name = "maddoxWingId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Maddox getMaddoxWing() {
        return maddoxWing;
    }

    public void setMaddoxWing(Maddox maddoxWing) {
        this.maddoxWing = maddoxWing;
    }

    @JoinColumn(name = "krimskyTestId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public KrimskyTest getKrimskyTest() {
        return krimskyTest;
    }

    public void setKrimskyTest(KrimskyTest krimskyTest) {
        this.krimskyTest = krimskyTest;
    }

    @JoinColumn(name = "pbkId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Pbk getPbk() {
        return pbk;
    }

    public void setPbk(Pbk pbk) {
        this.pbk = pbk;
    }

}
