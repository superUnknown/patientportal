package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.common.utils.NumberUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Wartość amplitudy akomodacji.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Amplitude")
public class Amplitude {

    private UUID amplitudeId = UUID.randomUUID();

    private Double op;

    private Double ol;

    private Double ou;

    @Id
    @Type(type = "uuid-char")
    public UUID getAmplitudeId() {
        return amplitudeId;
    }

    public void setAmplitudeId(UUID amplitudeId) {
        this.amplitudeId = amplitudeId;
    }

    public Double getOp() {
        return op;
    }

    public void setOp(Double op) {
        this.op = NumberUtils.round(op);
    }

    public Double getOl() {
        return ol;
    }

    public void setOl(Double ol) {
        this.ol = NumberUtils.round(ol);
    }

    public Double getOu() {
        return ou;
    }

    public void setOu(Double ou) {
        this.ou = NumberUtils.round(ou);
    }

}
