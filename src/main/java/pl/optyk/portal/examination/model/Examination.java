package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.patient.model.Patient;
import pl.optyk.portal.user.model.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Encja badania medycznego.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Examination")
@NamedQueries({
        @NamedQuery(
                name = Examination.GET_PATIENT_EXAMINATIONS,
                query = "SELECT e FROM Examination e " +
                        "   JOIN e.basicInformation bi " +
                        "WHERE " +
                        "   e.patient = :patient " +
                        "ORDER BY e.type ASC, bi.date DESC"
        ),
        @NamedQuery(
                name = Examination.GET_PATIENT_EXAMINATIONS_COUNT,
                query = "SELECT COUNT(*) FROM Examination e " +
                        "   JOIN e.basicInformation bi " +
                        "WHERE " +
                        "   e.patient = :patient " +
                        "ORDER BY e.type ASC, bi.date DESC"
        )
})
public class Examination {

    public static final String GET_PATIENT_EXAMINATIONS = "GET_PATIENT_EXAMINATIONS";
    public static final String GET_PATIENT_EXAMINATIONS_COUNT = "GET_PATIENT_EXAMINATIONS_COUNT";

    /**
     * Id.
     */
    private UUID examinationId = UUID.randomUUID();

    /**
     * Data utworzenia badania.
     */
    private Date created;

    /**
     * Typ badania.
     */
    private ExaminationType type;

    /**
     * Pacjent, dla którego badanie zostało założone.
     */
    private Patient patient;

    /**
     * Kto założył.
     */
    private User createdBy;

    /**
     * Data ostatniej modyfikacji badania.
     */
    private Date modified;

    /**
     * Kto ostatnio modyfikował.
     */
    private User modifiedBy;

    /**
     * Podstawowe informacje o badaniu.
     */
    private BasicInformation basicInformation = new BasicInformation();

    /**
     * Informacje o badaniu z części widzenia obuocznego.
     */
    private BinocularVision binocularVision = new BinocularVision();

    /**
     * Informacje o badaniu z części Synoptofor.
     */
    private SynoptoforTest synoptoforTest = new SynoptoforTest();

    /**
     * Informacje o badaniu z akomodacji.
     */
    private Accommodation accommodation = new Accommodation();

    /**
     * Lista badań korekcji.
     */
    private List<Correction> corrections = new ArrayList<>();

    /**
     * Załączone dokumenty do badania.
     */
    private List<Document> documents = new ArrayList<>();

    @Id
    @Type(type = "uuid-char")
    public UUID getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(UUID examinationId) {
        this.examinationId = examinationId;
    }

    @Enumerated(EnumType.STRING)
    public ExaminationType getType() {
        return type;
    }

    public void setType(ExaminationType type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @ManyToOne
    @JoinColumn(name = "patientId")
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @ManyToOne
    @JoinColumn(name = "createdBy")
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @ManyToOne
    @JoinColumn(name = "modifiedBy")
    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @JoinColumn(name = "basicInformationId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public BasicInformation getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformation basicInformation) {
        this.basicInformation = basicInformation;
    }

    @JoinColumn(name = "binocularVisionId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public BinocularVision getBinocularVision() {
        return binocularVision;
    }

    public void setBinocularVision(BinocularVision binocularVision) {
        this.binocularVision = binocularVision;
    }

    @JoinColumn(name = "synoptoforTestId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public SynoptoforTest getSynoptoforTest() {
        return synoptoforTest;
    }

    public void setSynoptoforTest(SynoptoforTest synoptoforTest) {
        this.synoptoforTest = synoptoforTest;
    }

    @JoinColumn(name = "accommodationId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

    @JoinColumn(name = "examinationId")
    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<Correction> getCorrections() {
        return corrections;
    }

    public void setCorrections(List<Correction> corrections) {
        this.corrections = corrections;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "examination", cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public enum ExaminationType {
        EXAMINATION, CORRECTION
    }

}
