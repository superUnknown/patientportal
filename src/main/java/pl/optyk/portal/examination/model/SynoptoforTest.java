package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Informacje o badaniu z części Synoptofor.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "SynoptoforTest")
public class SynoptoforTest {

    private UUID synoptoforTestId = UUID.randomUUID();

    private Integer objectiveAngle;

    private Integer subjectiveAngle;

    private Boolean jp;

    private Boolean f;

    private Boolean st;

    private String notes;

    @Id
    @Type(type = "uuid-char")
    public UUID getSynoptoforTestId() {
        return synoptoforTestId;
    }

    public void setSynoptoforTestId(UUID synoptoforTestId) {
        this.synoptoforTestId = synoptoforTestId;
    }

    public Integer getObjectiveAngle() {
        return objectiveAngle;
    }

    public void setObjectiveAngle(Integer objectiveAngle) {
        this.objectiveAngle = objectiveAngle;
    }

    public Integer getSubjectiveAngle() {
        return subjectiveAngle;
    }

    public void setSubjectiveAngle(Integer subjectiveAngle) {
        this.subjectiveAngle = subjectiveAngle;
    }

    public Boolean getJp() {
        return jp;
    }

    public void setJp(Boolean jp) {
        this.jp = jp;
    }

    public Boolean getF() {
        return f;
    }

    public void setF(Boolean f) {
        this.f = f;
    }

    public Boolean getSt() {
        return st;
    }

    public void setSt(Boolean st) {
        this.st = st;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
