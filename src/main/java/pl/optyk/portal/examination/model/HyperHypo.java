package pl.optyk.portal.examination.model;

import pl.optyk.portal.examination.utils.UnitNumber;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Wartość badania hyper/hypo;
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "HyperHypo")
@Inheritance(strategy = InheritanceType.JOINED)
public class HyperHypo extends UnitNumber {

    public HyperHypo() {
    }

    public HyperHypo(Double value, Unit unit) {
        super(value, unit);
    }
}
