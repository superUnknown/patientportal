package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Informacje o badaniu bez korekcji.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "NoCorrectionTest")
public class NoCorrectionTest {

    /**
     * Id.
     */
    private UUID noCorrectionTestId = UUID.randomUUID();

    /**
     * Informacje o oku prawym.
     */
    private String visusOP;

    /**
     * Informacje o oku lewym.
     */
    private String visusOL;

    /**
     * Informacje o obu oczach.
     */
    private String visusOU;

    @Id
    @Type(type = "uuid-char")
    public UUID getNoCorrectionTestId() {
        return noCorrectionTestId;
    }

    public void setNoCorrectionTestId(UUID noCorrectionTestId) {
        this.noCorrectionTestId = noCorrectionTestId;
    }

    public String getVisusOP() {
        return visusOP;
    }

    public void setVisusOP(String visusOP) {
        this.visusOP = visusOP;
    }

    public String getVisusOL() {
        return visusOL;
    }

    public void setVisusOL(String visusOL) {
        this.visusOL = visusOL;
    }

    public String getVisusOU() {
        return visusOU;
    }

    public void setVisusOU(String visusOU) {
        this.visusOU = visusOU;
    }

}
