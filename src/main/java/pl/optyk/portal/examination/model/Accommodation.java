package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.common.utils.NumberUtils;

import javax.persistence.*;
import java.util.UUID;

/**
 * Informacje o badaniu z akomodacji.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Accommodation")
public class Accommodation {

    /**
     * Id
     */
    private UUID accommodationId = UUID.randomUUID();

    private Double fcc;

    private Double pra;

    private Double nra;

    private String notes;

    private Flipper flipper = new Flipper();

    private Amplitude amplitude = new Amplitude();

    @Id
    @Type(type = "uuid-char")
    public UUID getAccommodationId() {
        return accommodationId;
    }

    public void setAccommodationId(UUID accommodationId) {
        this.accommodationId = accommodationId;
    }

    public Double getFcc() {
        return fcc;
    }

    public void setFcc(Double fcc) {
        this.fcc = NumberUtils.round(fcc);
    }

    public Double getPra() {
        return pra;
    }

    public void setPra(Double pra) {
        this.pra = NumberUtils.round(pra);
    }

    public Double getNra() {
        return nra;
    }

    public void setNra(Double nra) {
        this.nra = NumberUtils.round(nra);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JoinColumn(name = "flipperId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Flipper getFlipper() {
        return flipper;
    }

    public void setFlipper(Flipper flipper) {
        this.flipper = flipper;
    }

    @JoinColumn(name = "amplitudeId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Amplitude getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(Amplitude amplitude) {
        this.amplitude = amplitude;
    }

}
