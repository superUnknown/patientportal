package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Reprezentacja graficzna testu Schober'a.
 * Created by superunknown on 14.11.2016.
 */
@Entity
@Table(name = "SchobersTest")
public class SchobersTest {

    /**
     * Id.
     */
    private UUID schobersTestId = UUID.randomUUID();

    /**
     * Grafika.
     */
    private byte[] content;

    @Id
    @Type(type = "uuid-char")
    public UUID getSchobersTestId() {
        return schobersTestId;
    }

    public void setSchobersTestId(UUID schobersTestId) {
        this.schobersTestId = schobersTestId;
    }

    @Column(columnDefinition = "longblob")
    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

}
