package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Wynik testu Von Graefe'a.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "VonGraefeScore")
public class VonGraefeScore {

    private UUID vonGraefeScoreId = UUID.randomUUID();

    private EsoExo esoExo = new EsoExo();

    private HyperHypo hyperHypo = new HyperHypo();

    private FusionalVergence nfv = new FusionalVergence();

    private FusionalVergence pfv = new FusionalVergence();

    @Id
    @Type(type = "uuid-char")
    public UUID getVonGraefeScoreId() {
        return vonGraefeScoreId;
    }

    public void setVonGraefeScoreId(UUID vonGraefeScoreId) {
        this.vonGraefeScoreId = vonGraefeScoreId;
    }

    @JoinColumn(name = "esoExoId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public EsoExo getEsoExo() {
        return esoExo;
    }

    public void setEsoExo(EsoExo esoExo) {
        this.esoExo = esoExo;
    }

    @JoinColumn(name = "hyperHypoId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public HyperHypo getHyperHypo() {
        return hyperHypo;
    }

    public void setHyperHypo(HyperHypo hyperHypo) {
        this.hyperHypo = hyperHypo;
    }

    @JoinColumn(name = "nfvId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public FusionalVergence getNfv() {
        return nfv;
    }

    public void setNfv(FusionalVergence nfv) {
        this.nfv = nfv;
    }

    @JoinColumn(name = "pfvId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public FusionalVergence getPfv() {
        return pfv;
    }

    public void setPfv(FusionalVergence pfv) {
        this.pfv = pfv;
    }

}
