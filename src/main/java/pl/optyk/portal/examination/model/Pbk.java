package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Wartość pkb (int/int).
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Pbk")
public class Pbk {

    /**
     * Id
     */
    private UUID pbkId = UUID.randomUUID();

    /**
     * Licznik
     */
    private Integer numerator;

    /**
     * Mianownik.
     */
    private Integer denominator;

    public Pbk() {
    }

    public Pbk(Integer numerator, Integer denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Id
    @Type(type = "uuid-char")
    public UUID getPbkId() {
        return pbkId;
    }

    public void setPbkId(UUID pbkId) {
        this.pbkId = pbkId;
    }

    public Integer getNumerator() {
        return numerator;
    }

    public void setNumerator(Integer numerator) {
        this.numerator = numerator;
    }

    public Integer getDenominator() {
        return denominator;
    }

    public void setDenominator(Integer denominator) {
        this.denominator = denominator;
    }

}
