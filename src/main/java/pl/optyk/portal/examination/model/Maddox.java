package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Test maddoxa.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Maddox")
public class Maddox {

    private UUID maddoxId = UUID.randomUUID();

    private EsoExo esoExo = new EsoExo();

    private HyperHypo hyperHypo = new HyperHypo();

    @Id
    @Type(type = "uuid-char")
    public UUID getMaddoxId() {
        return maddoxId;
    }

    public void setMaddoxId(UUID maddoxId) {
        this.maddoxId = maddoxId;
    }

    @JoinColumn(name = "esoExoId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public EsoExo getEsoExo() {
        return esoExo;
    }

    public void setEsoExo(EsoExo esoExo) {
        this.esoExo = esoExo;
    }

    @JoinColumn(name = "hyperHypoId")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public HyperHypo getHyperHypo() {
        return hyperHypo;
    }

    public void setHyperHypo(HyperHypo hyperHypo) {
        this.hyperHypo = hyperHypo;
    }

}
