package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Reprezentacja graficzna testu Worth'a.
 * Created by superunknown on 14.11.2016.
 */
@Entity
@Table(name = "WorthsTest")
public class WorthsTest {

    /**
     * Id.
     */
    private UUID worthsTestId = UUID.randomUUID();

    /**
     * Grafika.
     */
    private byte[] content;

    @Id
    @Type(type = "uuid-char")
    public UUID getWorthsTestId() {
        return worthsTestId;
    }

    public void setWorthsTestId(UUID worthsTestId) {
        this.worthsTestId = worthsTestId;
    }

    @Column(columnDefinition = "longblob")
    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

}
