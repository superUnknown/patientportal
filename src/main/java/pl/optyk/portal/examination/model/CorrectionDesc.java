package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.common.utils.NumberUtils;

import javax.persistence.*;
import java.util.UUID;

/**
 * Informacje korekcji oka.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "CorrectionDesc")
public class CorrectionDesc {

    private UUID correctionDescId = UUID.randomUUID();

    private Double sf;

    private Double cyl;

    private Integer axis;

    private Double add;

    private Double pd;

    private Double pryzm;

    private Base base;

    private Integer baseValue;

    private String visus;

    @Id
    @Type(type = "uuid-char")
    public UUID getCorrectionDescId() {
        return correctionDescId;
    }

    public void setCorrectionDescId(UUID correctionDescId) {
        this.correctionDescId = correctionDescId;
    }

    public Double getSf() {
        return sf;
    }

    public void setSf(Double sf) {
        this.sf = NumberUtils.round(sf);
    }

    public Double getCyl() {
        return cyl;
    }

    public void setCyl(Double cyl) {
        this.cyl = NumberUtils.round(cyl);
    }

    public Integer getAxis() {
        return axis;
    }

    public void setAxis(Integer axis) {
        this.axis = axis;
    }

    @Column(name = "`add`")
    public Double getAdd() {
        return add;
    }

    public void setAdd(Double add) {
        this.add = NumberUtils.round(add);
    }

    public Double getPd() {
        return pd;
    }

    public void setPd(Double pd) {
        this.pd = NumberUtils.round(pd);
    }

    public Double getPryzm() {
        return pryzm;
    }

    public void setPryzm(Double pryzm) {
        this.pryzm = NumberUtils.round(pryzm);
    }

    @Enumerated(EnumType.STRING)
    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public Integer getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(Integer baseValue) {
        this.baseValue = baseValue;
    }

    public String getVisus() {
        return visus;
    }

    public void setVisus(String visus) {
        this.visus = visus;
    }

    /**
     * Wartość bazy. Jeżeli INT: prezentowana jest wartość numeryczna baseValue.
     */
    public enum Base {
        BN, BS, BG, BD, INT
    }

}
