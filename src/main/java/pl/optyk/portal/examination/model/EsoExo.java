package pl.optyk.portal.examination.model;

import pl.optyk.portal.examination.utils.UnitNumber;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Wartość badania eso/exo;
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "EsoExo")
@Inheritance(strategy = InheritanceType.JOINED)
public class EsoExo extends UnitNumber {

    public EsoExo() {
    }

    public EsoExo(Double value, Unit unit) {
        super(value, unit);
    }

}
