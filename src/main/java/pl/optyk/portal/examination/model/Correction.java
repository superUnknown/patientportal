package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Badanie korekcji.
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "Correction")
public class Correction {

    private UUID correctionId = UUID.randomUUID();

    private Category category;

    private String notes;

    private Integer vd;

    private String visusOu;

    private CorrectionDesc opDesc = new CorrectionDesc();

    private CorrectionDesc olDesc = new CorrectionDesc();

    @Id
    @Type(type = "uuid-char")
    public UUID getCorrectionId() {
        return correctionId;
    }

    public void setCorrectionId(UUID correctionId) {
        this.correctionId = correctionId;
    }

    @Enumerated(EnumType.STRING)
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getVd() {
        return vd;
    }

    public void setVd(Integer vd) {
        this.vd = vd;
    }

    public String getVisusOu() {
        return visusOu;
    }

    public void setVisusOu(String visusOu) {
        this.visusOu = visusOu;
    }

    @JoinColumn(name = "opDescId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public CorrectionDesc getOpDesc() {
        return opDesc;
    }

    public void setOpDesc(CorrectionDesc opDesc) {
        this.opDesc = opDesc;
    }

    @JoinColumn(name = "olDescId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public CorrectionDesc getOlDesc() {
        return olDesc;
    }

    public void setOlDesc(CorrectionDesc olDesc) {
        this.olDesc = olDesc;
    }

    /**
     * Kategoria korekcji.
     */
    public enum Category {

        /**
         * Okulary
         */
        GLASSES,

        /**
         * Soczewki kontaktowe
         */
        CONTACT_LENSES,

        /**
         * Soczewki RGP
         */
        RGP_LENSES,

        /**
         * Autorefraktometr
         */
        AUTOREFRAKTOMETR

    }

}
