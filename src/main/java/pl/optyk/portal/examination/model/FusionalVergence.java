package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.common.utils.NumberUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Wartość badania typu NFV/PFV
 * Created by superUnknown on 14.10.2016.
 */
@Entity
@Table(name = "FusionalVergence")
public class FusionalVergence {

    private UUID fusionalVergenceId = UUID.randomUUID();

    /**
     * Zamglenie
     */
    private Double haze;

    /**
     * Zerwanie
     */
    private Double rupture;

    /**
     * Odtworzenie
     */
    private Double reconstruction;

    public FusionalVergence() {
    }

    public FusionalVergence(Double haze, Double rupture, Double reconstruction) {
        this.haze = haze;
        this.rupture = rupture;
        this.reconstruction = reconstruction;
    }

    @Id
    @Type(type = "uuid-char")
    public UUID getFusionalVergenceId() {
        return fusionalVergenceId;
    }

    public void setFusionalVergenceId(UUID fusionalVergenceId) {
        this.fusionalVergenceId = fusionalVergenceId;
    }

    public Double getHaze() {
        return haze;
    }

    public void setHaze(Double haze) {
        this.haze = NumberUtils.round(haze);
    }

    public Double getRupture() {
        return rupture;
    }

    public void setRupture(Double rupture) {
        this.rupture = NumberUtils.round(rupture);
    }

    public Double getReconstruction() {
        return reconstruction;
    }

    public void setReconstruction(Double reconstruction) {
        this.reconstruction = NumberUtils.round(reconstruction);
    }

}
