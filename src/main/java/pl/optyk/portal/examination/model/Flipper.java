package pl.optyk.portal.examination.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Flipper z badania akomodacji.
 * Created by superUnknown on 23.10.2016.
 */
@Entity
@Table(name = "Flipper")
public class Flipper {

    /**
     * Id
     */
    private UUID flipperId = UUID.randomUUID();

    /**
     * Wartość flipper'a.
     */
    private Integer value;

    /**
     * Flipper występuje przy PLUS/MINUS lub nieokreślony.
     */
    private Boolean problem;

    @Id
    @Type(type = "uuid-char")
    public UUID getFlipperId() {
        return flipperId;
    }

    public void setFlipperId(UUID flipperId) {
        this.flipperId = flipperId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Boolean getProblem() {
        return problem;
    }

    public void setProblem(Boolean problem) {
        this.problem = problem;
    }

}
