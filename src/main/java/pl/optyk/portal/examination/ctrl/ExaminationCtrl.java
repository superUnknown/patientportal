package pl.optyk.portal.examination.ctrl;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.examination.dto.*;
import pl.optyk.portal.examination.exception.ExaminationNotCorrectionException;
import pl.optyk.portal.examination.exception.ExaminationNotFoundException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Endpoint do zarządzania badaniami pacjentów.
 * Created by superUnknown on 15.10.2016.
 */
@Component
public interface ExaminationCtrl {

    /**
     * Wyciąganie badania po id.
     *
     * @param id
     * @return
     */
    <T extends ExaminationDto> T getExamination(UUID id, boolean extended) throws ExaminationNotFoundException;

    /**
     * Tworzenie nowego badania dla podanego pacjenta.
     *
     * @param patientId
     * @param dto
     * @return
     */
    ExaminationDto createExamination(UUID patientId, ExaminationSaveDto dto) throws ImageProcessingException, PatientNotFoundException;

    /**
     * Modyfikacja podanego badania pacjenta.
     *
     * @param examinationId
     * @param dto
     * @return
     */
    ExaminationDto modifyExamination(UUID examinationId, ExaminationSaveDto dto) throws ImageProcessingException, ExaminationNotFoundException;

    /**
     * Pobieranie badań pacjenta.
     *
     * @param patientId
     * @param page
     * @param qty
     * @return
     */
    Page<ExaminationDto> getPatientExaminations(UUID patientId, Integer page, Integer qty) throws PatientNotFoundException;

    /**
     * Pobieranie korekcji badania.
     *
     * @param examinationId
     * @return
     */
    List<CorrectionDto> getExaminationCorrections(UUID examinationId) throws ExaminationNotFoundException;

    /**
     * Pobiera datę ostatniego badania pacjenta.
     *
     * @param patientId
     * @return
     */
    Map<String, Date> getPatientsLastExaminationDate(UUID patientId) throws PatientNotFoundException;

    /**
     * Wyciąga test Schober'a po id.
     *
     * @param uuid
     * @return
     */
    SchobersTestDto getSchobersTest(UUID uuid);

    /**
     * Wyciąga test Worth'a po id.
     *
     * @param uuid
     * @return
     */
    WorthsTestDto getWorthsTest(UUID uuid);

    /**
     * Pobiera listę dostępnych testów Schober'a.
     *
     * @return
     */
    List<SchobersTestDto> getSchobersTests();

    /**
     * Pobiera listę dostępnych testów Worth'a.
     *
     * @return
     */
    List<WorthsTestDto> getWorthsTests();

    /**
     * Tworzenie nowego badania dla podanego pacjenta typu korekcja. Dostęp do operacji posiada
     * zalogowany użytkownik z prawami zapisu. Korekcja może zostać utworzona przed wypełnieniem
     * pełnego badania. Następnie może zostać przekształcone w normalne badanie (przez wywołanie
     * modyfikacji badania).
     *
     * @param patientId
     * @param corrections
     * @return
     */
    ExaminationDto createCorrection(UUID patientId, List<CorrectionDto> corrections) throws PatientNotFoundException;

    /**
     * Modyfikacja podanego badania typu korekcja pacjenta.
     *
     * @param examinationId
     * @param corrections
     * @return
     */
    ExaminationDto modifyCorrection(UUID examinationId, List<CorrectionDto> corrections) throws ExaminationNotCorrectionException, ExaminationNotFoundException;

    /**
     * Pobiera listę podpowiedzi nazw dokumentów, które wcześniej były dodane.
     *
     * @return
     */
    List<String> getAllDocumentNames();

}
