package pl.optyk.portal.examination.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import pl.optyk.portal.common.exception.ImageProcessingException;
import pl.optyk.portal.examination.dto.*;
import pl.optyk.portal.examination.exception.ExaminationNotCorrectionException;
import pl.optyk.portal.examination.exception.ExaminationNotFoundException;
import pl.optyk.portal.examination.service.ExaminationService;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

import java.util.*;

@RestController
@RequestMapping("/api")
public class ExaminationCtrlBean implements ExaminationCtrl {

    private final ExaminationService examinationService;

    @Autowired
    public ExaminationCtrlBean(ExaminationService examinationService) {
        this.examinationService = examinationService;
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/examinations/{id}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public <T extends ExaminationDto> T getExamination(@PathVariable UUID id, @RequestParam(required = false, defaultValue = "false") boolean extended)
            throws ExaminationNotFoundException {
        return examinationService.getExamination(id, extended);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}/examinations",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public ExaminationDto createExamination(@PathVariable UUID patientId, @RequestBody ExaminationSaveDto dto)
            throws ImageProcessingException, PatientNotFoundException {
        return examinationService.createExamination(patientId, dto);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/examinations/{examinationId}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public ExaminationDto modifyExamination(@PathVariable UUID examinationId, @RequestBody ExaminationSaveDto dto)
            throws ImageProcessingException, ExaminationNotFoundException {
        return examinationService.modifyExamination(examinationId, dto);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}/examinations",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public Page<ExaminationDto> getPatientExaminations(
            @PathVariable UUID patientId,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "50") Integer qty
    ) throws PatientNotFoundException {
        return examinationService.getPatientExaminations(patientId, page, qty);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/examinations/{examinationId}/corrections",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<CorrectionDto> getExaminationCorrections(@PathVariable UUID examinationId) throws ExaminationNotFoundException {
        return examinationService.getExaminationCorrections(examinationId);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}/examinations/last_examination_date",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public Map<String, Date> getPatientsLastExaminationDate(@PathVariable UUID patientId) throws PatientNotFoundException {
        return Collections.singletonMap("date", examinationService.getPatientsLastExaminationDate(patientId));
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/schobers_tests/{id}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public SchobersTestDto getSchobersTest(@PathVariable UUID id) {
        return examinationService.getSchobersTest(id);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/worths_tests/{id}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public WorthsTestDto getWorthsTest(@PathVariable UUID id) {
        return examinationService.getWorthsTest(id);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/schobers_tests",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<SchobersTestDto> getSchobersTests() {
        return examinationService.getSchobersTests();
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/worths_tests",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<WorthsTestDto> getWorthsTests() {
        return examinationService.getWorthsTests();
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}/corrections",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public ExaminationDto createCorrection(@PathVariable UUID patientId, @RequestBody List<CorrectionDto> corrections)
            throws PatientNotFoundException {
        return examinationService.createCorrection(patientId, corrections);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/corrections/{examinationId}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public ExaminationDto modifyCorrection(@PathVariable UUID examinationId, @RequestBody List<CorrectionDto> corrections)
            throws ExaminationNotCorrectionException, ExaminationNotFoundException {
        return examinationService.modifyCorrection(examinationId, corrections);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/documents/names",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<String> getAllDocumentNames() {
        return examinationService.getAllDocumentNames();
    }

}
