package pl.optyk.portal.examination.utils;

import org.hibernate.annotations.Type;
import pl.optyk.portal.common.utils.NumberUtils;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * Wartość numeryczna z określoną jednostką.
 * Created by superUnknown on 14.10.2016.
 */
@MappedSuperclass
public abstract class UnitNumber {

    private UUID id = UUID.randomUUID();

    private Double value;

    private Unit unit;

    public UnitNumber() {
    }

    public UnitNumber(Double value, Unit unit) {
        this.value = NumberUtils.round(value);
        this.unit = unit;
    }

    @Id
    @Type(type = "uuid-char")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = NumberUtils.round(value);
    }

    @Enumerated(EnumType.STRING)
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    /**
     * Jednostra miary.
     */
    public enum Unit {

        /**
         * Stopnie
         */
        DEGREES,

        /**
         * pdptr BN
         */
        PDPTR_BN,

        /**
         * pdptr BS
         */
        PDPTR_BS,

        /**
         * pdptr BG
         */
        PDPTR_BG,

        /**
         * pdptr BD
         */
        PDPTR_BD

    }

}
