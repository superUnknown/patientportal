package pl.optyk.portal.examination.utils;

/**
 * Wartość numeryczna z określoną jednostką - dto.
 * Created by superUnknown on 14.10.2016.
 */
public class UnitNumberDto {

    private Double value;

    private UnitNumber.Unit unit;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public UnitNumber.Unit getUnit() {
        return unit;
    }

    public void setUnit(UnitNumber.Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UnitNumberDto{");
        sb.append("value=").append(value);
        sb.append(", unit=").append(unit);
        sb.append('}');
        return sb.toString();
    }

}
