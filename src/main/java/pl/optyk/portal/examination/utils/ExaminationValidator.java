package pl.optyk.portal.examination.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.optyk.portal.common.utils.Assert;
import pl.optyk.portal.examination.model.*;

import java.util.Calendar;
import java.util.List;

/**
 * Narządzie do walidacji badania. Logika wyciągnięta do oddzielnej
 * klasy ze względu na rozbudowaną schemę.
 * Created by superUnknown on 15.10.2016.
 */
public class ExaminationValidator {

    private static final Logger log = LogManager.getLogger(ExaminationValidator.class);

    /**
     * Walidacja danych podanych w formularzu przy tworzeniu/modyfikacji badania dla pacjenta.
     * W przypadku błędu rzuca IllegalArgumentException.
     *
     * @param examination
     */
    public static void validate(Examination examination) {
        if (log.isDebugEnabled())
            log.debug("Weryfikuje poprawność danych badania id: {}", examination.getExaminationId());

        validateBasicInformation(examination.getBasicInformation());
        validateBinocularVision(examination.getBinocularVision());
        validateSynoptoforTest(examination.getSynoptoforTest());
        validateAccommodation(examination.getAccommodation());
        validateCorrections(examination.getCorrections());
        validateDocuments(examination.getDocuments());

    }

    private static void validateDocuments(List<Document> documents) {
        if (documents.isEmpty()) {
            return;
        }

        int i = 0;
        for (Document document : documents) {
            Assert.isNullOrEmpty(document.getName(), String.format("Required document name is missing (document %s).", i));
            Assert.notNull(document.getContent(), String.format("Required document content is missing (document '%s').", document.getName()));
            i++;

        }

    }

    private static void validateBasicInformation(BasicInformation basicInformation) {
        Assert.notNull(basicInformation.getDate(), "Required examination date is missing.");
        Assert.isTrue(basicInformation.getDate().before(Calendar.getInstance().getTime()), "Examination date cannot be in the future.");
        Assert.isNullOrEmpty(basicInformation.getVisitReason(), "Required examination visit reason is missing.");
        Assert.isNullOrEmpty(basicInformation.getVisionCorrectionHistory(), "Required visit correction history is missing.");
        Assert.isNullOrEmpty(basicInformation.getEyeDiseaseHistory(), "Required eye disease history is missing.");
        Assert.isNullOrEmpty(basicInformation.getDiagnosis(), "Required diagnosis is missing.");
        Assert.isNullOrEmpty(basicInformation.getRecommendations(), "Required recommendations are missing.");
        Assert.isNullOrEmpty(basicInformation.getOverallHealth(), "Required overall health info is missing.");
        Assert.isNullOrEmpty(basicInformation.getMedications(), "Required medications are missing.");

        Assert.isTrue(basicInformation.getLastExaminationDate() == null || basicInformation.getLastExaminationDate().before(Calendar.getInstance().getTime()), "Last examination date cannot be in the future.");
        if (basicInformation.getLastExaminationDateNote() == null) {
            Assert.notNull(basicInformation.getLastExaminationDate(), "Last examination date cannot be empty.");

        }


    }

    private static void validateBinocularVision(BinocularVision binocularVision) {
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getEsoExo().getValue(), 0.25, 0, 30, "Von Graefe's far test eso/exo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getHyperHypo().getValue(), 0.25, 0, 30, "Von Graefe's far test hyper/hypo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getNfv().getHaze(), 0.25, 0, 30, "Von Graefe's far NFV test haze value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getNfv().getRupture(), 0.25, 0, 30, "Von Graefe's far NFV test rupture value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getNfv().getReconstruction(), 0.25, 0, 30, "Von Graefe's far NFV test reconstruction value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getPfv().getHaze(), 0.25, 0, 30, "Von Graefe's far PFV test haze value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getPfv().getRupture(), 0.25, 0, 30, "Von Graefe's far PFV test rupture value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getFarScore().getPfv().getReconstruction(), 0.25, 0, 30, "Von Graefe's far PFV test reconstruction value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getEsoExo().getValue(), 0.25, 0, 30, "Von Graefe's near test eso/exo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getHyperHypo().getValue(), 0.25, 0, 30, "Von Graefe's near test hyper/hypo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getNfv().getHaze(), 0.25, 0, 30, "Von Graefe's near NFV test haze value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getNfv().getRupture(), 0.25, 0, 30, "Von Graefe's near NFV test rupture value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getNfv().getReconstruction(), 0.25, 0, 30, "Von Graefe's near NFV test reconstruction value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getPfv().getHaze(), 0.25, 0, 30, "Von Graefe's near PFV test haze value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getPfv().getRupture(), 0.25, 0, 30, "Von Graefe's near PFV test rupture value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getVonGraefeTest().getNearScore().getPfv().getReconstruction(), 0.25, 0, 30, "Von Graefe's near PFV test reconstruction value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(binocularVision.getMaddoxCross().getEsoExo().getValue(), 0.25, -20, 20, "Maddox Cross test eso/exo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getMaddoxCross().getHyperHypo().getValue(), 0.25, -20, 20, "Maddox Cross test hyper/hypo value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(binocularVision.getMaddoxWing().getEsoExo().getValue(), 0.25, -20, 20, "Maddox Wing test eso/exo value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getMaddoxWing().getHyperHypo().getValue(), 0.25, -20, 20, "Maddox Wing test hyper/hypo value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(binocularVision.getKrimskyTest().getValue(), 1, -50, 50, "Krimsky test value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(binocularVision.getPbk().getNumerator(), 1, 0, 40, "PBK test value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(binocularVision.getPbk().getDenominator(), 1, 0, 40, "PBK test value is not valid.");

    }

    private static void validateSynoptoforTest(SynoptoforTest synoptoforTest) {
        Assert.valueNotMultipledByOrNotInRange(synoptoforTest.getObjectiveAngle(), 1, -50, 50, "Synoptofor test objective angle value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(synoptoforTest.getSubjectiveAngle(), 1, -50, 50, "Synoptofor test subjective angle value is not valid.");

    }

    private static void validateAccommodation(Accommodation accommodation) {
        Assert.valueNotMultipledByOrNotInRange(accommodation.getFcc(), 0.25, -1, 3, "Accommodation FCC value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(accommodation.getPra(), 0.25, 0, 4, "Accommodation PRA value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(accommodation.getNra(), 0.25, 0, 4, "Accommodation NRA value is not valid.");

        Assert.valueNotMultipledByOrNotInRange(accommodation.getAmplitude().getOl(), 0.25, 0, 20, "Accommodation amplitude OL value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(accommodation.getAmplitude().getOp(), 0.25, 0, 20, "Accommodation amplitude OP value is not valid.");
        Assert.valueNotMultipledByOrNotInRange(accommodation.getAmplitude().getOu(), 0.25, 0, 20, "Accommodation amplitude OU value is not valid.");

    }

    public static void validateCorrections(List<Correction> corrections) {
        if (corrections.isEmpty()) {
            return;
        }

        for (Correction correction : corrections) {
            Assert.notNull(correction.getCategory(), "Required correction's category is missing.");
            Assert.valueNotMultipledByOrNotInRange(correction.getVd(), 1, 0, 20, "Correction's VD value is not valid.");

            Assert.notNull(correction.getOlDesc().getSf(), "Required correction's OL Sf value is missing.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getSf(), 0.25, -35, 35, "Correction's OL Sf value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getCyl(), 0.25, -10, 10, "Correction's OL Cyl value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getAdd(), 0.25, 0, 4, "Correction's OL Add value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getPd(), 0.1, 20, 40, "Correction's OL PD value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getPryzm(), 0.25, 0, 20, "Correction's OL Pryzm value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getAxis(), 1, 1, 180, "Correction's OL Axis value is not valid.");

            if (correction.getOlDesc().getBase() != null && correction.getOlDesc().getBase().equals(CorrectionDesc.Base.INT)) {
                Assert.notNull(correction.getOlDesc().getBaseValue(), "Correction's OL Base value is missing.");
                Assert.valueNotMultipledByOrNotInRange(correction.getOlDesc().getBaseValue(), 1, 1, 180, "Correction's OL Base value is not valid.");
            }


            Assert.notNull(correction.getOpDesc().getSf(), "Required correction's OP Sf value is missing.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getSf(), 0.25, -35, 35, "Correction's OP Sf value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getCyl(), 0.25, -10, 10, "Correction's OP Cyl value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getAdd(), 0.25, 0, 4, "Correction's OP Add value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getPd(), 0.1, 20, 40, "Correction's OP PD value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getPryzm(), 0.25, 0, 20, "Correction's OP Pryzm value is not valid.");
            Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getAxis(), 1, 1, 180, "Correction's OP Axis value is not valid.");

            if (correction.getOpDesc().getBase() != null && correction.getOpDesc().getBase().equals(CorrectionDesc.Base.INT)) {
                Assert.notNull(correction.getOpDesc().getBaseValue(), "Correction's OP Base value is missing.");
                Assert.valueNotMultipledByOrNotInRange(correction.getOpDesc().getBaseValue(), 1, 1, 180, "Correction's OP Base value is not valid.");
            }

        }

    }

}
