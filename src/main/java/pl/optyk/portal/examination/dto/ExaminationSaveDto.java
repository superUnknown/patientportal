package pl.optyk.portal.examination.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto z danymi badania medycznego do zapisu.
 * Created by superUnknown on 14.10.2016.
 */
public class ExaminationSaveDto {

    /**
     * Podstawowe informacje o badaniu.
     */
    private BasicInformationDto basicInformation = new BasicInformationDto();

    /**
     * Informacje o badaniu z części widzenia obuocznego.
     */
    private BinocularVisionDto binocularVision = new BinocularVisionDto();

    /**
     * Informacje o badaniu z części Synoptofor.
     */
    private SynoptoforTestDto synoptoforTest = new SynoptoforTestDto();

    /**
     * Informacje o badaniu z akomodacji.
     */
    private AccommodationDto accommodation = new AccommodationDto();

    /**
     * Lista badań korekcji.
     */
    private List<CorrectionDto> corrections = new ArrayList<>();

    /**
     * Załączone dokumenty do badania.
     */
    private List<DocumentSaveDto> documents = new ArrayList<>();

    public BasicInformationDto getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformationDto basicInformation) {
        this.basicInformation = basicInformation;
    }

    public BinocularVisionDto getBinocularVision() {
        return binocularVision;
    }

    public void setBinocularVision(BinocularVisionDto binocularVision) {
        this.binocularVision = binocularVision;
    }

    public SynoptoforTestDto getSynoptoforTest() {
        return synoptoforTest;
    }

    public void setSynoptoforTest(SynoptoforTestDto synoptoforTest) {
        this.synoptoforTest = synoptoforTest;
    }

    public AccommodationDto getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(AccommodationDto accommodation) {
        this.accommodation = accommodation;
    }

    public List<CorrectionDto> getCorrections() {
        return corrections;
    }

    public void setCorrections(List<CorrectionDto> corrections) {
        this.corrections = corrections;
    }

    public List<DocumentSaveDto> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentSaveDto> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExaminationSaveDto{");
        sb.append("basicInformation=").append(basicInformation);
        sb.append(", binocularVision=").append(binocularVision);
        sb.append(", synoptoforTest=").append(synoptoforTest);
        sb.append(", accommodation=").append(accommodation);
        sb.append(", corrections=").append(corrections);
        sb.append(", documents=").append(documents);
        sb.append('}');
        return sb.toString();
    }

}
