package pl.optyk.portal.examination.dto;

/**
 * Dto informacji o badaniu z części Synoptofor.
 * Created by superUnknown on 14.10.2016.
 */
public class SynoptoforTestDto {

    private Integer objectiveAngle;

    private Integer subjectiveAngle;

    private Boolean jp;

    private Boolean f;

    private Boolean st;

    private String notes;

    public Integer getObjectiveAngle() {
        return objectiveAngle;
    }

    public void setObjectiveAngle(Integer objectiveAngle) {
        this.objectiveAngle = objectiveAngle;
    }

    public Integer getSubjectiveAngle() {
        return subjectiveAngle;
    }

    public void setSubjectiveAngle(Integer subjectiveAngle) {
        this.subjectiveAngle = subjectiveAngle;
    }

    public Boolean getJp() {
        return jp;
    }

    public void setJp(Boolean jp) {
        this.jp = jp;
    }

    public Boolean getF() {
        return f;
    }

    public void setF(Boolean f) {
        this.f = f;
    }

    public Boolean getSt() {
        return st;
    }

    public void setSt(Boolean st) {
        this.st = st;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SynoptoforTestDto{");
        sb.append("objectiveAngle=").append(objectiveAngle);
        sb.append(", subjectiveAngle=").append(subjectiveAngle);
        sb.append(", jp=").append(jp);
        sb.append(", f=").append(f);
        sb.append(", st=").append(st);
        sb.append(", notes='").append(notes).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
