package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.model.CorrectionDesc;

/**
 * Dto informacji korekcji oka.
 * Created by superUnknown on 14.10.2016.
 */
public class CorrectionDescDto {

    private Double sf;

    private Double cyl;

    private Integer axis;

    private Double add;

    private Double pd;

    private Double pryzm;

    private CorrectionDesc.Base base;

    private Integer baseValue;

    private String visus;

    public Double getSf() {
        return sf;
    }

    public void setSf(Double sf) {
        this.sf = sf;
    }

    public Double getCyl() {
        return cyl;
    }

    public void setCyl(Double cyl) {
        this.cyl = cyl;
    }

    public Integer getAxis() {
        return axis;
    }

    public void setAxis(Integer axis) {
        this.axis = axis;
    }

    public Double getAdd() {
        return add;
    }

    public void setAdd(Double add) {
        this.add = add;
    }

    public Double getPd() {
        return pd;
    }

    public void setPd(Double pd) {
        this.pd = pd;
    }

    public Double getPryzm() {
        return pryzm;
    }

    public void setPryzm(Double pryzm) {
        this.pryzm = pryzm;
    }

    public CorrectionDesc.Base getBase() {
        return base;
    }

    public void setBase(CorrectionDesc.Base base) {
        this.base = base;
    }

    public Integer getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(Integer baseValue) {
        this.baseValue = baseValue;
    }

    public String getVisus() {
        return visus;
    }

    public void setVisus(String visus) {
        this.visus = visus;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CorrectionDescDto{");
        sb.append("sf=").append(sf);
        sb.append(", cyl=").append(cyl);
        sb.append(", axis=").append(axis);
        sb.append(", add=").append(add);
        sb.append(", pd=").append(pd);
        sb.append(", pryzm=").append(pryzm);
        sb.append(", base=").append(base);
        sb.append(", baseValue=").append(baseValue);
        sb.append(", visus='").append(visus).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
