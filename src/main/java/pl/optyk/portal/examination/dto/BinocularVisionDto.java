package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.utils.UnitNumberDto;

import java.util.UUID;

/**
 * Dto informacji o badaniu z części widzenia obuocznego.
 * Created by superUnknown on 14.10.2016.
 */
public class BinocularVisionDto {

    /**
     * CT
     */
    private Boolean ct;

    /**
     * CUT
     */
    private Boolean cut;

    /**
     * Test Hirschberg'a.
     */
    private Boolean hirschbergTest;

    /**
     * Opis 9 kierunków spojrzenia.
     */
    private String nineDirectionsGlance;

    /**
     * Inne uwagi.
     */
    private String notes;

    /**
     * Test Worth'a (id grafiki).
     */
    private UUID worthsTestId;

    /**
     * Test Schober'a (id grafiki).
     */
    private UUID schobersTestId;

    /**
     * Test Von Graefe'a.
     */
    private VonGraefeTestDto vonGraefeTest = new VonGraefeTestDto();

    /**
     * Test krzyża maddoxa.
     */
    private MaddoxDto maddoxCross = new MaddoxDto();

    /**
     * Test skrzydła maddoxa.
     */
    private MaddoxDto maddoxWing = new MaddoxDto();

    /**
     * Wartość testu Krimsky'iego.
     */
    private UnitNumberDto krimskyTest = new UnitNumberDto();

    /**
     * Wartość pkb (int/int).
     */
    private PbkDto pbk = new PbkDto();

    public Boolean getCt() {
        return ct;
    }

    public void setCt(Boolean ct) {
        this.ct = ct;
    }

    public Boolean getCut() {
        return cut;
    }

    public void setCut(Boolean cut) {
        this.cut = cut;
    }

    public Boolean getHirschbergTest() {
        return hirschbergTest;
    }

    public void setHirschbergTest(Boolean hirschbergTest) {
        this.hirschbergTest = hirschbergTest;
    }

    public String getNineDirectionsGlance() {
        return nineDirectionsGlance;
    }

    public void setNineDirectionsGlance(String nineDirectionsGlance) {
        this.nineDirectionsGlance = nineDirectionsGlance;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public UUID getWorthsTestId() {
        return worthsTestId;
    }

    public void setWorthsTestId(UUID worthsTestId) {
        this.worthsTestId = worthsTestId;
    }

    public UUID getSchobersTestId() {
        return schobersTestId;
    }

    public void setSchobersTestId(UUID schobersTestId) {
        this.schobersTestId = schobersTestId;
    }

    public VonGraefeTestDto getVonGraefeTest() {
        return vonGraefeTest;
    }

    public void setVonGraefeTest(VonGraefeTestDto vonGraefeTest) {
        this.vonGraefeTest = vonGraefeTest;
    }

    public MaddoxDto getMaddoxCross() {
        return maddoxCross;
    }

    public void setMaddoxCross(MaddoxDto maddoxCross) {
        this.maddoxCross = maddoxCross;
    }

    public MaddoxDto getMaddoxWing() {
        return maddoxWing;
    }

    public void setMaddoxWing(MaddoxDto maddoxWing) {
        this.maddoxWing = maddoxWing;
    }

    public UnitNumberDto getKrimskyTest() {
        return krimskyTest;
    }

    public void setKrimskyTest(UnitNumberDto krimskyTest) {
        this.krimskyTest = krimskyTest;
    }

    public PbkDto getPbk() {
        return pbk;
    }

    public void setPbk(PbkDto pbk) {
        this.pbk = pbk;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BinocularVisionDto{");
        sb.append("ct=").append(ct);
        sb.append(", cut=").append(cut);
        sb.append(", hirschbergTest=").append(hirschbergTest);
        sb.append(", nineDirectionsGlance='").append(nineDirectionsGlance).append('\'');
        sb.append(", notes='").append(notes).append('\'');
        sb.append(", worthsTestId=").append(worthsTestId);
        sb.append(", schobersTestId=").append(schobersTestId);
        sb.append(", vonGraefeTest=").append(vonGraefeTest);
        sb.append(", maddoxCross=").append(maddoxCross);
        sb.append(", maddoxWing=").append(maddoxWing);
        sb.append(", krimskyTest=").append(krimskyTest);
        sb.append(", pbk=").append(pbk);
        sb.append('}');
        return sb.toString();
    }

}
