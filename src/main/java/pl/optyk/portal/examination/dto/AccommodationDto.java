package pl.optyk.portal.examination.dto;

/**
 * Dto informacji o badaniu z akomodacji.
 * Created by superUnknown on 14.10.2016.
 */
public class AccommodationDto {

    private Double fcc;

    private Double pra;

    private Double nra;

    private String notes;

    private FlipperDto flipper = new FlipperDto();

    private AmplitudeDto amplitude = new AmplitudeDto();

    public Double getFcc() {
        return fcc;
    }

    public void setFcc(Double fcc) {
        this.fcc = fcc;
    }

    public Double getPra() {
        return pra;
    }

    public void setPra(Double pra) {
        this.pra = pra;
    }

    public Double getNra() {
        return nra;
    }

    public void setNra(Double nra) {
        this.nra = nra;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public FlipperDto getFlipper() {
        return flipper;
    }

    public void setFlipper(FlipperDto flipper) {
        this.flipper = flipper;
    }

    public AmplitudeDto getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(AmplitudeDto amplitude) {
        this.amplitude = amplitude;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AccommodationDto{");
        sb.append("fcc=").append(fcc);
        sb.append(", pra=").append(pra);
        sb.append(", nra=").append(nra);
        sb.append(", notes='").append(notes).append('\'');
        sb.append(", flipper=").append(flipper);
        sb.append(", amplitude=").append(amplitude);
        sb.append('}');
        return sb.toString();
    }

}
