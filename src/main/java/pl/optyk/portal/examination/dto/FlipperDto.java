package pl.optyk.portal.examination.dto;

/**
 * Dto flipper'a z badania akomodacji.
 * Created by superUnknown on 23.10.2016.
 */
public class FlipperDto {

    /**
     * Wartość flipper'a.
     */
    private Integer value;

    /**
     * Flipper występuje przy PLUS/MINUS lub nieokreślony.
     */
    private Boolean problem;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Boolean getProblem() {
        return problem;
    }

    public void setProblem(Boolean problem) {
        this.problem = problem;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FlipperDto{");
        sb.append("value=").append(value);
        sb.append(", problem=").append(problem);
        sb.append('}');
        return sb.toString();
    }

}
