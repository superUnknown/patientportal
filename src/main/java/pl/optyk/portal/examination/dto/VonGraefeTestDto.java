package pl.optyk.portal.examination.dto;

/**
 * Dto test'u Von Graefe'a.
 * Created by superUnknown on 14.10.2016.
 */
public class VonGraefeTestDto {

    /**
     * Wynik badania "w bliż".
     */
    private VonGraefeScoreDto nearScore = new VonGraefeScoreDto();

    /**
     * Wynik badania "w dal".
     */
    private VonGraefeScoreDto farScore = new VonGraefeScoreDto();

    public VonGraefeScoreDto getNearScore() {
        return nearScore;
    }

    public void setNearScore(VonGraefeScoreDto nearScore) {
        this.nearScore = nearScore;
    }

    public VonGraefeScoreDto getFarScore() {
        return farScore;
    }

    public void setFarScore(VonGraefeScoreDto farScore) {
        this.farScore = farScore;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VonGraefeTestDto{");
        sb.append("nearScore=").append(nearScore);
        sb.append(", farScore=").append(farScore);
        sb.append('}');
        return sb.toString();
    }

}
