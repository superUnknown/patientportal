package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.utils.UnitNumberDto;

/**
 * Dto wyniku testu Von Graefe'a.
 * Created by superUnknown on 14.10.2016.
 */
public class VonGraefeScoreDto {

    private UnitNumberDto esoExo = new UnitNumberDto();

    private UnitNumberDto hyperHypo = new UnitNumberDto();

    private FusionalVergenceDto nfv = new FusionalVergenceDto();

    private FusionalVergenceDto pfv = new FusionalVergenceDto();

    public UnitNumberDto getEsoExo() {
        return esoExo;
    }

    public void setEsoExo(UnitNumberDto esoExo) {
        this.esoExo = esoExo;
    }

    public UnitNumberDto getHyperHypo() {
        return hyperHypo;
    }

    public void setHyperHypo(UnitNumberDto hyperHypo) {
        this.hyperHypo = hyperHypo;
    }

    public FusionalVergenceDto getNfv() {
        return nfv;
    }

    public void setNfv(FusionalVergenceDto nfv) {
        this.nfv = nfv;
    }

    public FusionalVergenceDto getPfv() {
        return pfv;
    }

    public void setPfv(FusionalVergenceDto pfv) {
        this.pfv = pfv;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VonGraefeScoreDto{");
        sb.append("esoExo=").append(esoExo);
        sb.append(", hyperHypo=").append(hyperHypo);
        sb.append(", nfv=").append(nfv);
        sb.append(", pfv=").append(pfv);
        sb.append('}');
        return sb.toString();
    }

}
