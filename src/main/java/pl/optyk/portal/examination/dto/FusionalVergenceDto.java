package pl.optyk.portal.examination.dto;

/**
 * Dto wartości badania typu NFV/PFV
 * Created by superUnknown on 14.10.2016.
 */
public class FusionalVergenceDto {

    private Double haze;

    private Double rupture;

    private Double reconstruction;

    public Double getHaze() {
        return haze;
    }

    public void setHaze(Double haze) {
        this.haze = haze;
    }

    public Double getRupture() {
        return rupture;
    }

    public void setRupture(Double rupture) {
        this.rupture = rupture;
    }

    public Double getReconstruction() {
        return reconstruction;
    }

    public void setReconstruction(Double reconstruction) {
        this.reconstruction = reconstruction;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FusionalVergenceDto{");
        sb.append("left=").append(haze);
        sb.append(", center=").append(rupture);
        sb.append(", right=").append(reconstruction);
        sb.append('}');
        return sb.toString();
    }

}
