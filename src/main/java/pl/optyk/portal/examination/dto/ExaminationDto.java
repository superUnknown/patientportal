package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.model.Examination;

import java.util.Date;
import java.util.UUID;

/**
 * Dto z podstawowymi danymi badania medycznego.
 * Created by superUnknown on 14.10.2016.
 */
public class ExaminationDto {

    /**
     * Id.
     */
    protected UUID examinationId;

    /**
     * Typ badania.
     */
    private Examination.ExaminationType type;

    /**
     * Data utworzenia badania.
     */
    protected Date created;

    /**
     * Kto założył.
     */
    protected String createdBy;

    /**
     * Data ostatniej modyfikacji badania.
     */
    protected Date modified;

    /**
     * Kto ostatnio modyfikował.
     */
    protected String modifiedBy;

    /**
     * Podstawowe informacje o badaniu.
     */
    protected BasicInformationDto basicInformation;

    public UUID getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(UUID examinationId) {
        this.examinationId = examinationId;
    }

    public Examination.ExaminationType getType() {
        return type;
    }

    public void setType(Examination.ExaminationType type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public BasicInformationDto getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformationDto basicInformation) {
        this.basicInformation = basicInformation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExaminationDto{");
        sb.append("examinationId=").append(examinationId);
        sb.append(", type=").append(type);
        sb.append(", created=").append(created);
        sb.append(", createdBy='").append(createdBy).append('\'');
        sb.append(", modified=").append(modified);
        sb.append(", modifiedBy='").append(modifiedBy).append('\'');
        sb.append(", basicInformation=").append(basicInformation);
        sb.append('}');
        return sb.toString();
    }

}
