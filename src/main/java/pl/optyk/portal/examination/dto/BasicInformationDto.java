package pl.optyk.portal.examination.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Dto podstawowych informacji o badaniu - rozszerza dane badania.
 * Created by superUnknown on 14.10.2016.
 */
public class BasicInformationDto {

    /**
     * Data wykonania badania.
     */
    private Date date;

    /**
     * Powód wizyty.
     */
    private String visitReason;

    /**
     * Pozostałe dolegliwości.
     */
    private String otherAilments;

    /**
     * Historia korekcji.
     */
    private String visionCorrectionHistory;

    /**
     * Data ostatniego badania wzroku.
     */
    private Date lastExaminationDate;

    /**
     * Data ostatniego badania wzroku - notatka (na wypadek, gdyby pacjent nie pamiętał, można tu wpisać przybliżoną datę).
     */
    private String lastExaminationDateNote;

    /**
     * Historia choroby oczu.
     */
    private String eyeDiseaseHistory;

    /**
     * Rozpoznanie.
     */
    private String diagnosis;

    /**
     * Zalecenia.
     */
    private String recommendations;

    /**
     * Ogólny stan zdrowia.
     */
    private String overallHealth;

    /**
     * Przyjmowane leki
     */
    private String medications;

    /**
     * Uwagi
     */
    private String notes;

    /**
     * Test Amslera OP
     */
    private Boolean amslerTestOP;

    /**
     * Test Amslera OL
     */
    private Boolean amslerTestOL;

    /**
     * Choroby oczu pacjenta.
     */
    private List<String> eyeDiseases = new ArrayList<>();

    /**
     * Choroby ogólne.
     */
    private List<String> generalDiseases = new ArrayList<>();

    /**
     * Informacje o badaniu bez korekcji.
     */
    private NoCorrectionTestDto noCorrectionTest = new NoCorrectionTestDto();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

    public String getOtherAilments() {
        return otherAilments;
    }

    public void setOtherAilments(String otherAilments) {
        this.otherAilments = otherAilments;
    }

    public String getVisionCorrectionHistory() {
        return visionCorrectionHistory;
    }

    public void setVisionCorrectionHistory(String visionCorrectionHistory) {
        this.visionCorrectionHistory = visionCorrectionHistory;
    }

    public Date getLastExaminationDate() {
        return lastExaminationDate;
    }

    public void setLastExaminationDate(Date lastExaminationDate) {
        this.lastExaminationDate = lastExaminationDate;
    }

    public String getLastExaminationDateNote() {
        return lastExaminationDateNote;
    }

    public void setLastExaminationDateNote(String lastExaminationDateNote) {
        this.lastExaminationDateNote = lastExaminationDateNote;
    }

    public String getEyeDiseaseHistory() {
        return eyeDiseaseHistory;
    }

    public void setEyeDiseaseHistory(String eyeDiseaseHistory) {
        this.eyeDiseaseHistory = eyeDiseaseHistory;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public String getOverallHealth() {
        return overallHealth;
    }

    public void setOverallHealth(String overallHealth) {
        this.overallHealth = overallHealth;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getAmslerTestOP() {
        return amslerTestOP;
    }

    public void setAmslerTestOP(Boolean amslerTestOP) {
        this.amslerTestOP = amslerTestOP;
    }

    public Boolean getAmslerTestOL() {
        return amslerTestOL;
    }

    public void setAmslerTestOL(Boolean amslerTestOL) {
        this.amslerTestOL = amslerTestOL;
    }

    public List<String> getEyeDiseases() {
        return eyeDiseases;
    }

    public void setEyeDiseases(List<String> eyeDiseases) {
        this.eyeDiseases = eyeDiseases;
    }

    public List<String> getGeneralDiseases() {
        return generalDiseases;
    }

    public void setGeneralDiseases(List<String> generalDiseases) {
        this.generalDiseases = generalDiseases;
    }

    public NoCorrectionTestDto getNoCorrectionTest() {
        return noCorrectionTest;
    }

    public void setNoCorrectionTest(NoCorrectionTestDto noCorrectionTest) {
        this.noCorrectionTest = noCorrectionTest;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BasicInformationDto{");
        sb.append("date=").append(date);
        sb.append(", visitReason='").append(visitReason).append('\'');
        sb.append(", otherAilments='").append(otherAilments).append('\'');
        sb.append(", visionCorrectionHistory='").append(visionCorrectionHistory).append('\'');
        sb.append(", lastExaminationDate=").append(lastExaminationDate);
        sb.append(", lastExaminationDateNote='").append(lastExaminationDateNote).append('\'');
        sb.append(", eyeDiseaseHistory='").append(eyeDiseaseHistory).append('\'');
        sb.append(", diagnosis='").append(diagnosis).append('\'');
        sb.append(", recommendations='").append(recommendations).append('\'');
        sb.append(", overallHealth='").append(overallHealth).append('\'');
        sb.append(", medications='").append(medications).append('\'');
        sb.append(", notes='").append(notes).append('\'');
        sb.append(", amslerTestOP=").append(amslerTestOP);
        sb.append(", amslerTestOL=").append(amslerTestOL);
        sb.append(", eyeDiseases=").append(eyeDiseases);
        sb.append(", generalDiseases=").append(generalDiseases);
        sb.append(", noCorrectionTest=").append(noCorrectionTest);
        sb.append('}');
        return sb.toString();
    }
}
