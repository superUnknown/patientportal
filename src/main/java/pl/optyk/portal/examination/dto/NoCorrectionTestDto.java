package pl.optyk.portal.examination.dto;

/**
 * Dto z informacjami o badaniu bez korekcji.
 * Created by superUnknown on 14.10.2016.
 */
public class NoCorrectionTestDto {

    /**
     * Informacje o oku prawym.
     */
    private String visusOP;

    /**
     * Informacje o oku lewym.
     */
    private String visusOL;

    /**
     * Informacje o obu oczach.
     */
    private String visusOU;

    public String getVisusOP() {
        return visusOP;
    }

    public void setVisusOP(String visusOP) {
        this.visusOP = visusOP;
    }

    public String getVisusOL() {
        return visusOL;
    }

    public void setVisusOL(String visusOL) {
        this.visusOL = visusOL;
    }

    public String getVisusOU() {
        return visusOU;
    }

    public void setVisusOU(String visusOU) {
        this.visusOU = visusOU;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NoCorrectionTestDto{");
        sb.append("visusOP='").append(visusOP).append('\'');
        sb.append(", visusOL='").append(visusOL).append('\'');
        sb.append(", visusOU='").append(visusOU).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
