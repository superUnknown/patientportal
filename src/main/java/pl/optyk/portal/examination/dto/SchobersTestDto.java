package pl.optyk.portal.examination.dto;

import java.util.UUID;

/**
 * Reprezentacja graficzna testu Schober'a.
 * Created by superunknown on 14.11.2016.
 */
public class SchobersTestDto {

    /**
     * Id.
     */
    private UUID schobersTestId;

    /**
     * Grafika.
     */
    private byte[] content;

    public SchobersTestDto() {
    }

    public SchobersTestDto(UUID schobersTestId, byte[] content) {
        this.schobersTestId = schobersTestId;
        this.content = content;
    }

    public UUID getSchobersTestId() {
        return schobersTestId;
    }

    public void setSchobersTestId(UUID schobersTestId) {
        this.schobersTestId = schobersTestId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

}
