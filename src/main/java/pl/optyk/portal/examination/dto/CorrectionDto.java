package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.model.Correction;

/**
 * Dto z badania korekcji.
 * Created by superUnknown on 14.10.2016.
 */
public class CorrectionDto {

    private Correction.Category category;

    private String notes;

    private Integer vd;

    private String visusOu;

    private CorrectionDescDto opDesc = new CorrectionDescDto();

    private CorrectionDescDto olDesc = new CorrectionDescDto();

    public Correction.Category getCategory() {
        return category;
    }

    public void setCategory(Correction.Category category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getVd() {
        return vd;
    }

    public void setVd(Integer vd) {
        this.vd = vd;
    }

    public String getVisusOu() {
        return visusOu;
    }

    public void setVisusOu(String visusOu) {
        this.visusOu = visusOu;
    }

    public CorrectionDescDto getOpDesc() {
        return opDesc;
    }

    public void setOpDesc(CorrectionDescDto opDesc) {
        this.opDesc = opDesc;
    }

    public CorrectionDescDto getOlDesc() {
        return olDesc;
    }

    public void setOlDesc(CorrectionDescDto olDesc) {
        this.olDesc = olDesc;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CorrectionDto{");
        sb.append("category=").append(category);
        sb.append(", notes='").append(notes).append('\'');
        sb.append(", vd=").append(vd);
        sb.append(", visusOu='").append(visusOu).append('\'');
        sb.append(", opDesc=").append(opDesc);
        sb.append(", olDesc=").append(olDesc);
        sb.append('}');
        return sb.toString();
    }

}
