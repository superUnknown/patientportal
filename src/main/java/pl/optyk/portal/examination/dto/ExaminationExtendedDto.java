package pl.optyk.portal.examination.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto ze wszystkimi danymi badania medycznego.
 * Created by superUnknown on 15.10.2016.
 */
public class ExaminationExtendedDto extends ExaminationDto {

    /**
     * Informacje o badaniu z części widzenia obuocznego.
     */
    private BinocularVisionDto binocularVision;

    /**
     * Informacje o badaniu z części Synoptofor.
     */
    private SynoptoforTestDto synoptoforTest;

    /**
     * Informacje o badaniu z akomodacji.
     */
    private AccommodationDto accommodation;

    /**
     * Lista badań korekcji.
     */
    private List<CorrectionDto> corrections = new ArrayList<>();

    /**
     * Załączone dokumenty do badania.
     */
    private List<DocumentDto> documents = new ArrayList<>();

    public BinocularVisionDto getBinocularVision() {
        return binocularVision;
    }

    public void setBinocularVision(BinocularVisionDto binocularVision) {
        this.binocularVision = binocularVision;
    }

    public SynoptoforTestDto getSynoptoforTest() {
        return synoptoforTest;
    }

    public void setSynoptoforTest(SynoptoforTestDto synoptoforTest) {
        this.synoptoforTest = synoptoforTest;
    }

    public AccommodationDto getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(AccommodationDto accommodation) {
        this.accommodation = accommodation;
    }

    public List<CorrectionDto> getCorrections() {
        return corrections;
    }

    public void setCorrections(List<CorrectionDto> corrections) {
        this.corrections = corrections;
    }

    public List<DocumentDto> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentDto> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExaminationExtendedDto{");
        sb.append("examinationId=").append(examinationId);
        sb.append(", created=").append(created);
        sb.append(", createdBy='").append(createdBy).append('\'');
        sb.append(", modified=").append(modified);
        sb.append(", modifiedBy='").append(modifiedBy).append('\'');
        sb.append(", basicInformation=").append(basicInformation);
        sb.append(", binocularVision=").append(binocularVision);
        sb.append(", synoptoforTest=").append(synoptoforTest);
        sb.append(", accommodation=").append(accommodation);
        sb.append(", corrections=").append(corrections);
        sb.append(", documents=").append(documents);
        sb.append('}');
        return sb.toString();
    }

}
