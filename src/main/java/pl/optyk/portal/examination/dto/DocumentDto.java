package pl.optyk.portal.examination.dto;

/**
 * Dto załączonego dokumentu do badania.
 * Created by superunknown on 03.12.2016.
 */
public class DocumentDto {

    private String name;

    private byte[] content;

    public DocumentDto(String name, byte[] content) {
        this.name = name;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DocumentDto{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
