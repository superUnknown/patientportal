package pl.optyk.portal.examination.dto;

/**
 * Dto wartośi pkb (int/int).
 * Created by superUnknown on 14.10.2016.
 */
public class PbkDto {

    /**
     * Licznik
     */
    private Integer numerator;

    /**
     * Mianownik.
     */
    private Integer denominator;

    public Integer getNumerator() {
        return numerator;
    }

    public void setNumerator(Integer numerator) {
        this.numerator = numerator;
    }

    public Integer getDenominator() {
        return denominator;
    }

    public void setDenominator(Integer denominator) {
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PbkDto{");
        sb.append("numerator=").append(numerator);
        sb.append(", denominator=").append(denominator);
        sb.append('}');
        return sb.toString();
    }

}
