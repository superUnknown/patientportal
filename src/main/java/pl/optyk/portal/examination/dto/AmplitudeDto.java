package pl.optyk.portal.examination.dto;

/**
 * Dto wartości amplitudy akomodacji.
 * Created by superUnknown on 14.10.2016.
 */
public class AmplitudeDto {

    private Double op;

    private Double ol;

    private Double ou;

    public Double getOp() {
        return op;
    }

    public void setOp(Double op) {
        this.op = op;
    }

    public Double getOl() {
        return ol;
    }

    public void setOl(Double ol) {
        this.ol = ol;
    }

    public Double getOu() {
        return ou;
    }

    public void setOu(Double ou) {
        this.ou = ou;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AmplitudeDto{");
        sb.append("op=").append(op);
        sb.append(", ol=").append(ol);
        sb.append(", ou=").append(ou);
        sb.append('}');
        return sb.toString();
    }

}
