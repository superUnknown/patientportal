package pl.optyk.portal.examination.dto;

import pl.optyk.portal.examination.utils.UnitNumberDto;

/**
 * Dto test'u maddoxa.
 * Created by superUnknown on 14.10.2016.
 */
public class MaddoxDto {

    private UnitNumberDto esoExo = new UnitNumberDto();

    private UnitNumberDto hyperHypo = new UnitNumberDto();

    public UnitNumberDto getEsoExo() {
        return esoExo;
    }

    public void setEsoExo(UnitNumberDto esoExo) {
        this.esoExo = esoExo;
    }

    public UnitNumberDto getHyperHypo() {
        return hyperHypo;
    }

    public void setHyperHypo(UnitNumberDto hyperHypo) {
        this.hyperHypo = hyperHypo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MaddoxDto{");
        sb.append("esoExo=").append(esoExo);
        sb.append(", hyperHypo=").append(hyperHypo);
        sb.append('}');
        return sb.toString();
    }

}
