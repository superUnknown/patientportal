package pl.optyk.portal.examination.dto;

/**
 * Dto załączonego dokumentu do badania.
 * Created by superunknown on 03.12.2016.
 */
public class DocumentSaveDto {

    private String name;

    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DocumentDto{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
