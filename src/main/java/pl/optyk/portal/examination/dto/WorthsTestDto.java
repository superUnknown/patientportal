package pl.optyk.portal.examination.dto;

import java.util.UUID;

/**
 * Reprezentacja graficzna testu Worth'a.
 * Created by superunknown on 14.11.2016.
 */
public class WorthsTestDto {

    /**
     * Id.
     */
    private UUID worthsTestId;

    /**
     * Grafika.
     */
    private byte[] content;

    public WorthsTestDto() {
    }

    public WorthsTestDto(UUID worthsTestId, byte[] content) {
        this.worthsTestId = worthsTestId;
        this.content = content;
    }

    public UUID getWorthsTestId() {
        return worthsTestId;
    }

    public void setWorthsTestId(UUID worthsTestId) {
        this.worthsTestId = worthsTestId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

}
