package pl.optyk.portal.examination.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.repository.AbstractRepository;
import pl.optyk.portal.examination.model.*;
import pl.optyk.portal.patient.model.Patient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class ExaminationRepositoryBean extends AbstractRepository implements ExaminationRepository {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Examination getExamination(UUID id) {
        return this.em.find(Examination.class, id);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void save(Examination examination) {
        this.em.persist(examination);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void delete(Correction correction) {
        this.em.remove(correction);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void delete(Document document) {
        this.em.remove(document);
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<Examination> getPatientExaminations(Patient patient, Integer page, Integer qty) {
        List results = this.em.createNamedQuery(Examination.GET_PATIENT_EXAMINATIONS)
                .setParameter("patient", patient)
                .setFirstResult(page * qty)
                .setMaxResults(qty)
                .getResultList();

        long totalCount = (long) this.em.createNamedQuery(Examination.GET_PATIENT_EXAMINATIONS_COUNT)
                .setParameter("patient", patient)
                .getSingleResult();

        return new PageImpl<>(results, new PageRequest(page, qty), totalCount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public SchobersTest getSchobersTest(UUID uuid) {
        return this.em.find(SchobersTest.class, uuid);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public WorthsTest getWorthsTest(UUID uuid) {
        return this.em.find(WorthsTest.class, uuid);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<SchobersTest> getSchobersTests() {
        List result = this.em.createQuery("SELECT s FROM SchobersTest s").getResultList();
        if (result == null || result.size() == 0) {
            return new ArrayList<>();
        }

        return (List<SchobersTest>) result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<WorthsTest> getWorthsTests() {
        List result = this.em.createQuery("SELECT w FROM WorthsTest w").getResultList();
        if (result == null || result.size() == 0) {
            return new ArrayList<>();
        }

        return (List<WorthsTest>) result;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllDocumentNames() {
        List result = this.em.createQuery("SELECT DISTINCT d.name FROM Document d").getResultList();
        if (result == null || result.isEmpty()) {
            return Collections.emptyList();
        }

        return result;
    }

}
