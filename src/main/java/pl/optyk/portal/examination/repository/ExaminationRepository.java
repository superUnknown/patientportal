package pl.optyk.portal.examination.repository;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.examination.model.*;
import pl.optyk.portal.patient.model.Patient;

import java.util.List;
import java.util.UUID;

/**
 * Dao z dostępem do badań pacjenta.
 * Created by superUnknown on 15.10.2016.
 */
@Component
public interface ExaminationRepository {

    /**
     * Wyciaganie badania po id.
     *
     * @param id
     * @return
     */
    Examination getExamination(UUID id);

    /**
     * Zapisywanie zmian w badaniu.
     *
     * @param examination
     */
    void save(Examination examination);

    /**
     * Usuwanie korekcji powiązanej z badaniem.
     *
     * @param correction
     */
    void delete(Correction correction);

    /**
     * Usuwanie dokumentu powiązanego z badaniem.
     *
     * @param document
     */
    void delete(Document document);

    /**
     * Pobieranie badań pacjenta.
     *
     * @param patient
     * @param page
     * @param qty
     * @return
     */
    Page<Examination> getPatientExaminations(Patient patient, Integer page, Integer qty);

    /**
     * Wyciąga test Schober'a po id.
     *
     * @param uuid
     * @return
     */
    SchobersTest getSchobersTest(UUID uuid);

    /**
     * Wyciąga test Worth'a po id.
     *
     * @param uuid
     * @return
     */
    WorthsTest getWorthsTest(UUID uuid);

    /**
     * Pobiera listę dostępnych testów Schober'a.
     *
     * @return
     */
    List<SchobersTest> getSchobersTests();

    /**
     * Pobiera listę dostępnych testów Worth'a.
     *
     * @return
     */
    List<WorthsTest> getWorthsTests();

    /**
     * Pobiera listę podpowiedzi nazw dokumentów, które wcześniej były dodane.
     *
     * @return
     */
    List<String> getAllDocumentNames();

}
