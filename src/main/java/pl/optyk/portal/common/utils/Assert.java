package pl.optyk.portal.common.utils;

import java.math.BigDecimal;

/**
 * Dodatkowe asserty, których nie było.
 * Created by superUnknown on 16.10.2016.
 */
public class Assert extends org.springframework.util.Assert {

    public static void isNullOrEmpty(String text, String message) {
        if (text == null || text.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Czy liczba nie jest wielokrotnością innej lub nie mieści się z zakresie.
     *
     * @param val     wartość liczby
     * @param check   wartość liczby musi być jej wielokrotnością
     * @param min     wartość min
     * @param max     wartość max
     * @param message info
     */
    public static void valueNotMultipledByOrNotInRange(Number val, double check, double min, double max, String message) {
        if (val == null) {
            return;
        }

        double value = val.doubleValue();

        if (BigDecimal.valueOf(value).remainder(BigDecimal.valueOf(check)).doubleValue() != 0.0 || !(min <= value && value <= max)) {
            throw new IllegalArgumentException(String.format("%s [ value: %s, check: %s, min: %s, max: %s ]", message, value, check, min, max));
        }
    }

}
