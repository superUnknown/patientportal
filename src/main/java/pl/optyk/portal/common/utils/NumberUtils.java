package pl.optyk.portal.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    private static final int SCALE = 2;

    public static Double round(Double val) {
        return (val != null) ? BigDecimal.valueOf(val).setScale(SCALE, RoundingMode.HALF_UP).doubleValue() : null;
    }

}