package pl.optyk.portal.common.utils;


import org.apache.commons.codec.binary.Base64;
import pl.optyk.portal.common.exception.ImageProcessingException;
import sun.misc.BASE64Decoder;

public class ImageUtils {

    public static byte[] decodeImage(String content) throws ImageProcessingException {
        if (content == null) {
            return null;
        }

        content = new String(Base64.decodeBase64(content));
        try {
            return new BASE64Decoder().decodeBuffer(content.contains(",") ? content.split(",")[1] : content);
        } catch (Exception e) {
            throw new ImageProcessingException(e.getMessage(), e);
        }

    }

}
