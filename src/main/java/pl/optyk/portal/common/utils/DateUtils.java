package pl.optyk.portal.common.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Narzędzie do przetwarzania daty.
 * Created by superUnknown on 09.10.2016.
 */
public class DateUtils {

    /**
     * Obcina godzinę z daty, np. 2016-08-09 14:53:43 => 2016-08-09 00:00:00
     *
     * @param date
     * @return
     */
    public static Date withoutTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.AM_PM);
        cal.clear(Calendar.HOUR_OF_DAY);
        cal.clear(Calendar.HOUR);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        return cal.getTime();
    }

}
