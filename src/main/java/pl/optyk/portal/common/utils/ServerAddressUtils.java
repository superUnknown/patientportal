package pl.optyk.portal.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * Narzędzie z dostępem do danych hosta.
 * Created by superunknown on 03.11.2016.
 */
public class ServerAddressUtils {

    /**
     * Buduje z zwraca adres host'a.
     *
     * @param request
     * @return
     */
    public static String getServerAddress(HttpServletRequest request) {
        return String.format("%s://%s%s",
                request.isSecure() ? "https" : "http",
                request.getServerName(),
                request.getServerPort() != 80 ? ":" + request.getServerPort() : ""
        );
    }

}
