package pl.optyk.portal.common.exception;

/**
 * Błąd przetwarzania zdjęcia.
 */
public class ImageProcessingException extends Exception {

    public ImageProcessingException(String msg) {
        super(msg);
    }

    public ImageProcessingException(String message, Exception e) {
        super(message, e);
    }
}
