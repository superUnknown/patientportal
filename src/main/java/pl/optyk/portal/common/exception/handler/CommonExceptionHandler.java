package pl.optyk.portal.common.exception.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.optyk.portal.common.exception.ImageProcessingException;

/**
 * Handler REST'owy wyjątków.
 */
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class CommonExceptionHandler {
    private static final Logger log = LogManager.getLogger(CommonExceptionHandler.class);

    /**
     * Błąd przetwarzania grafiki.
     */
    @ResponseBody
    @ExceptionHandler(ImageProcessingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestException ImageProcessingExceptionHandler(ImageProcessingException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.BAD_REQUEST.value(), "image_processing_error", "Requested image processing error.");
    }

    /**
     * Błędne parametry.
     */
    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestException illegalArgumentExceptionHandler(IllegalArgumentException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.BAD_REQUEST.value(), "illegal_arguments", "Given data is not valid.");
    }

    /**
     * Błędny stan.
     */
    @ResponseBody
    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public RestException illegalStateExceptionHandler(IllegalStateException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.NOT_ACCEPTABLE.value(), "illegal_state", "Operation is not acceptable for current state.");
    }

    /**
     * Wyjątki nieprzewidziane.
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public RestException unexpectedExceptionHandler(Exception ex) {
        log.error(ex.getMessage(), ex);

        return new RestException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "internal_server_error", "Unexpected error occurred.");
    }
}
