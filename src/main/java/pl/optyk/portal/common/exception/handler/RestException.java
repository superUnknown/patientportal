package pl.optyk.portal.common.exception.handler;

/**
 * Obiekt reprezentujący format błędu.
 */
public class RestException {

    private Integer status;

    private String code;

    private String message;

    public RestException(Integer status, String code, String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
