package pl.optyk.portal.common.repository;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Abstrakcyjne DAO tworzące sesję.
 */
@Component
public abstract class AbstractRepository {

    @PersistenceContext
    protected EntityManager em;

}
