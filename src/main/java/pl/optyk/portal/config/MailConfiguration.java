package pl.optyk.portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templatemode.StandardTemplateModeHandlers;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.IOException;
import java.util.Properties;

/**
 * Konfiguracja dla usługi wysyłającej maile.
 */
@Configuration
@EnableConfigurationProperties(MailConfiguration.MailProperties.class)
public class MailConfiguration {

    @Bean
    public JavaMailSender mailSender(MailConfiguration.MailProperties properties) throws IOException {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(properties.getServer().getHost());
        mailSender.setPort(properties.getServer().getPort());
        mailSender.setProtocol(properties.getServer().getProtocol());
        mailSender.setUsername(properties.getServer().getUsername());
        mailSender.setPassword(properties.getServer().getPassword());

        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.smtp.auth", Boolean.toString(properties.getSmtp().isAuth()));
        javaMailProperties.setProperty("mail.smtp.starttls.enable", Boolean.toString(properties.getSmtp().isStarttlsEnabled()));
        javaMailProperties.setProperty("mail.smtp.quitwait", Boolean.toString(properties.getSmtp().isQuitwait()));
        mailSender.setJavaMailProperties(javaMailProperties);

        return mailSender;
    }

    @Bean(name = "htmlMailTemplateEngine")
    public TemplateEngine htmlMailTemplateEngine() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("mails/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setCacheable(false);

        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        return templateEngine;
    }

    @Bean(name = "mailMessageSource")
    public MessageSource mailMessageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("Mail");
        return messageSource;
    }

    @ConfigurationProperties("mail")
    public static class MailProperties {

        private String formHeader;

        private Server server = new Server();

        private Smtp smtp = new Smtp();

        public String getFormHeader() {
            return formHeader;
        }

        public void setFormHeader(String formHeader) {
            this.formHeader = formHeader;
        }

        public Server getServer() {
            return server;
        }

        public void setServer(Server server) {
            this.server = server;
        }

        public Smtp getSmtp() {
            return smtp;
        }

        public void setSmtp(Smtp smtp) {
            this.smtp = smtp;
        }

        static class Server {

            private String host;

            private Integer port;

            private String protocol;

            private String username;

            private String password;

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public Integer getPort() {
                return port;
            }

            public void setPort(Integer port) {
                this.port = port;
            }

            public String getProtocol() {
                return protocol;
            }

            public void setProtocol(String protocol) {
                this.protocol = protocol;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

        }

        static class Smtp {

            private boolean auth;

            private boolean starttlsEnabled;

            private boolean quitwait;

            public boolean isAuth() {
                return auth;
            }

            public void setAuth(boolean auth) {
                this.auth = auth;
            }

            public boolean isStarttlsEnabled() {
                return starttlsEnabled;
            }

            public void setStarttlsEnabled(boolean starttlsEnabled) {
                this.starttlsEnabled = starttlsEnabled;
            }

            public boolean isQuitwait() {
                return quitwait;
            }

            public void setQuitwait(boolean quitwait) {
                this.quitwait = quitwait;
            }

        }
    }

}
