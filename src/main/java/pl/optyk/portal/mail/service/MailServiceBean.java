package pl.optyk.portal.mail.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import pl.optyk.portal.config.MailConfiguration;
import pl.optyk.portal.mail.model.MailTemplate;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

@Service
public class MailServiceBean implements MailService {

    private static final Logger log = LogManager.getLogger(MailServiceBean.class);

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailConfiguration.MailProperties properties;

    @Resource(name = "htmlMailTemplateEngine")
    private TemplateEngine templateEngine;

    @Resource(name = "mailMessageSource")
    private MessageSource messageSource;

    @Override
    public void sendMail(MailTemplate template, String to) throws MessagingException {
        if (log.isDebugEnabled()) log.debug("Wysyłam maila na adres {}. Template {}", to, template);

        final MimeMessage msg = mailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");
        helper.setText(templateEngine.process(template.getTemplate(), template.getContext()), true);
        helper.setFrom(properties.getFormHeader());
        helper.setSubject(messageSource.getMessage(template.getSubject().getKey(), template.getSubject().getValue(), LocaleContextHolder.getLocale()));
        helper.setTo(to);

        for (Map.Entry<String, MultipartFile> image : template.getImages().entrySet()) {
            helper.addInline(image.getKey(), image.getValue(), image.getValue().getContentType());
        }

        mailSender.send(msg);

        if (log.isDebugEnabled()) log.debug("Mail na adres {} wysłany poprawnie.", to);

    }

}
