package pl.optyk.portal.mail.service;

import org.springframework.stereotype.Component;
import pl.optyk.portal.mail.model.MailTemplate;

import javax.mail.MessagingException;

/**
 * Usługa do wysyłania maili.
 * <p>
 * Created by superunknown on 31.08.16.
 */
@Component
public interface MailService {

    /**
     * Wysyła maila na podany adres.
     *
     * @param template
     * @param to
     */
    void sendMail(MailTemplate template, String to) throws MessagingException;

}
