package pl.optyk.portal.mail.model;

import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import java.util.Map;

/**
 * Template maila.
 */
public interface MailTemplate {

    /**
     * Temat maila. Klucz to namiar na temat w pliku properies, a wartość to argumenty do personalizacji.
     *
     * @return
     */
    Map.Entry<String, Object[]> getSubject();

    /**
     * Namiar na plik z template'm HTML.
     *
     * @return
     */
    String getTemplate();

    /**
     * Kontekst maila zawierający wszystkie potrzebne do wstawienia wiadomości.
     *
     * @return
     */
    Context getContext();

    /**
     * Lista obrazków. Klucz mapy określa nazwę parametru, za który należy podstawić obrazek w template'ie.
     *
     * @return
     */
    Map<String, MultipartFile> getImages();

}
