package pl.optyk.portal.mail.model;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import pl.optyk.portal.common.utils.ServerAddressUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Template dla maila, który jest wysyłany przy żądaniu zresetowania hasła.
 */
public class PasswordResetTokenMailTemplate implements MailTemplate {

    private final Context context;

    private static final String SUBJECT_MSG = "password_reset_mail_template_subject";

    private static final String TEMPLATE_NAME = "password_reset_mail_template";

    public PasswordResetTokenMailTemplate(HttpServletRequest request, String firstname, String surname, UUID token) {
        this.context = new Context(LocaleContextHolder.getLocale());
        this.context.setVariable("token_url", String.format("%s/ui/auth/password_reset?token=%s", ServerAddressUtils.getServerAddress(request), token));
        this.context.setVariable("user_firstname", firstname);
        this.context.setVariable("user_surname", surname);

    }

    @Override
    public Map.Entry<String, Object[]> getSubject() {
        return new AbstractMap.SimpleEntry<>(SUBJECT_MSG, null);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE_NAME;
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public Map<String, MultipartFile> getImages() {
        Map<String, MultipartFile> images = new HashMap<>();
//        TODO
        return images;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PasswordResetTokenMailTemplate{");
        sb.append("template=").append(TEMPLATE_NAME);
        sb.append(", context=").append(context);
        sb.append('}');
        return sb.toString();
    }
}
