package pl.optyk.portal.user.service;

import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.optyk.portal.user.dto.UserCreateDto;
import pl.optyk.portal.user.dto.UserDto;
import pl.optyk.portal.user.dto.UserModifyDto;
import pl.optyk.portal.user.dto.UserSimpleDto;
import pl.optyk.portal.user.exception.*;

import javax.mail.MessagingException;

/**
 * Usługa do zarządzania kontami użytkowników.
 */
@Component
public interface UserService extends UserDetailsService {

    /**
     * Pobiera informacje użytkownika. Jeżeli user posiada status != ENABLED, zwraca pustą listę uprawnień.
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    /**
     * Operacja dozwolona tylko dla administratora. Tworzy nowego użytkownika, który otrzymuje status NEW.
     * Dodawanie administratora jest zabronione. Po dodaniu wysyłany jest mail z linkiem rejestracyjnym
     * kierującym do ustawienia hasła. Do tego czasu konto jest nieaktywne i nie można się na nie zalogować.
     * Link rejestracyjny zawiera token, który jest przypisany do użytkownika i ważny przez 48h od utworzenia.
     * Jeżeli użytkownik nie zdąży zweryfikować rejestracji, musi poprosić admin'a o wysłanie nowego.
     *
     * @param userCreateDto
     * @return
     */
    UserDto createUser(UserCreateDto userCreateDto) throws MessagingException, UserAlreadyExistsException;

    /**
     * Potwierdzenie rejestracji. Klikając w link wysłany wcześniej mailem, użytkownik jest kierowany do
     * tego mechanizmu. Weryfikowany jest token. Jeżeli okaże się poprawny, użytkownik powiązany z tokenem
     * dostaje status VERIFIED oraz token jest ustawiany jako zweryfikowany. Taki użytkownik może jedynie ustawić hasło.
     * Jeżeli sesja wygaśnie, a użytkownik nie zdąży tego wykonać, jedynym sposobem jest ponowne kliknięcie
     * w link. Token, jeżeli jeszcze nie wygasł, nie zostanie usunięty (stanie się to po poprawnym ustawieniu hasła).
     *
     * @param token
     * @return
     */
    void verifyRegistration(String token) throws VerificationTokenNotFoundException;

    /**
     * Finalizuje rejestrację użytkownika.
     * 1. Szuka tokena (jeżeli go nie znajdzie, wygasł lub nie został wcześniej zweryfikowany - zmiana jest odrzucona).
     * 2. User powiązany z tokenem musi posiadać status VERIFIED (wcześniej potwierdził rejestrację za pomocą tego tokena).
     * 3. Ustawieniu hasło, przełącza usera w status ENABLED i loguje go do sesji.
     * 4. Usuwa token.
     *
     * @param token
     * @param password
     */
    void finalizeRegistration(String token, String password);

    /**
     * Modyfikacja hasła użytkownika aktywnego (status ENABLED).
     *
     * @param newPassword
     * @param oldPassword
     * @return
     */
    UserDto modifyPassword(String newPassword, String oldPassword) throws OldPasswordNotMatchException;

    /**
     * Reset hasła użytkownika.
     * 1. Szuka użytkownika po mailu.
     * 2. Tworzy i zapisuje nowy token.
     * 3. Wysyła ten token w linku na podanego maila.
     *
     * @param email
     */
    void resetPassword(String email) throws UserNotFoundException, MessagingException;

    /**
     * Weryfikacja linka resetującego hasło.
     * 1. Waliduje token i sprawdza, czy token należy do aktywnego użytkownika.
     * 2. Ustawia token jako zweryfikowany.
     * 3. Po poprawnym wykonaniu, user powinien zostać przekierowany do strony, gdzie może ustawić nowe hasło.
     *
     * @param token
     */
    void verifyPasswordResetToken(String token) throws PasswordResetTokenNotFoundException;

    /**
     * Finalizacja obsługi resetowania hasła.
     * 1. Szuka tokena resetującego hasło (jeżeli go nie znajdzie, wygasł lub nie został wcześniej potwierdzony - zmiana jest odrzucona).
     * 2. Aktualizuje hasło.
     * 3. Usuwa token powiązany z aktualnie zalogowanym userem.
     *
     * @param password
     */
    void finalizePasswordReset(String token, String password);

    /**
     * Pobiera aktualnie zalogowanego użytkownika.
     *
     * @return
     */
    UserDto getCurrentUser();

    /**
     * Wyszukiwanie użytkowników po podanym query (imię, nazwisko, username, email). Wyniki posortowane po nazwisku.
     *
     * @param query
     * @param page
     * @param qty
     * @return
     */
    Page<UserDto> findUsers(String query, Integer page, Integer qty);

    /**
     * Modyfikacja użytkownika. Zmiana loginu jest niedostępna. Metoda dostępna jest dla admina - może tym
     * zmodyfikować dane każdego użytkownika, w tym swoje.
     *
     * @param username
     * @param userDto
     * @return
     */
    UserDto modifyUser(String username, UserModifyDto userDto) throws UserNotFoundException, UserAlreadyExistsException;

    /**
     * Dezaktywacja konta:
     * - dostęp ma jedynie admin,
     * - dezaktywacja admina niemożliwa,
     * - dezaktywacja jedynie dla użytkownika aktywnego (przed aktywacją można usunąć),
     * - dezaktywacja wylogowuje użytkownika.
     *
     * @param username
     * @return
     */
    UserDto deactivateUser(String username) throws UserNotFoundException;

    /**
     * Reaktywacja konta wcześniej aktywowanego:
     * - dostęp ma jedynie admin,
     * - reaktywacja jedynie dla użytkownika nieaktywnego,
     * - operacja zmienia status na ENABLED,
     * - po zakończeniu, user może się zalogować lub zresetować hasło, jeżeli je zapomniał.
     *
     * @param username
     * @return
     */
    UserDto reactivateUser(String username) throws UserNotFoundException;

    /**
     * Usuwanie konta:
     * - brak możliwości cofnięcia,
     * - dostęp ma jedynie admin,
     * - usunięcie admina niemożliwa,
     * - usunięcie jedynie dla użytkownika przed aktywacją, później można dezaktywoć,
     * - operacja permanentnie usuwa użytkownika i wylogowuje go.
     *
     * @param username
     * @return
     */
    void deleteUser(String username) throws UserNotFoundException;

    /**
     * Wyciąganie użytkownika po loginie (tylko dane publiczne).
     *
     * @param username
     * @return
     */
    UserSimpleDto getUser(String username) throws UserNotFoundException;

}
