package pl.optyk.portal.user.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.mail.model.PasswordResetTokenMailTemplate;
import pl.optyk.portal.mail.model.VerificationTokenMailTemplate;
import pl.optyk.portal.mail.service.MailService;
import pl.optyk.portal.user.dto.UserCreateDto;
import pl.optyk.portal.user.dto.UserDto;
import pl.optyk.portal.user.dto.UserModifyDto;
import pl.optyk.portal.user.dto.UserSimpleDto;
import pl.optyk.portal.user.exception.*;
import pl.optyk.portal.user.mapper.UserMapper;
import pl.optyk.portal.user.model.PasswordResetToken;
import pl.optyk.portal.user.model.User;
import pl.optyk.portal.user.model.VerificationToken;
import pl.optyk.portal.user.repository.UserRepository;
import pl.optyk.portal.user.repository.UserTokensRepository;
import pl.optyk.portal.user.utils.SessionUtils;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserServiceBean implements UserService {

    private static final Logger log = LogManager.getLogger(UserServiceBean.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserTokensRepository userTokensRepository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private MailService mailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam dane usera {} do logowania.", username);

        User user = userRepository.getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with given username not found.");
        }

        boolean enabled = user.getStatus().equals(User.Status.ENABLED);

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), enabled, true, true, true, enabled ? user.getAuthorities() : Collections.EMPTY_LIST
        );
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public UserDto createUser(UserCreateDto userCreateDto) throws MessagingException, UserAlreadyExistsException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Tworzenie nowego użytkownika przez {}: {}", currentUser.getUsername(), userCreateDto);

        if (userCreateDto.getUsername() == null || userCreateDto.getUsername().isEmpty() ||
                userCreateDto.getEmail() == null || userCreateDto.getEmail().isEmpty() ||
                userCreateDto.getFirstname() == null || userCreateDto.getFirstname().isEmpty() ||
                userCreateDto.getSurname() == null || userCreateDto.getSurname().isEmpty() ||
                userCreateDto.getAuthority() == null || userCreateDto.getAuthority().equals(User.Authority.SUPER_USER)) {
            throw new IllegalArgumentException("Required data is not valid.");
        }

        if (userRepository.getUser(userCreateDto.getUsername()) != null ||
                userRepository.getUserByEmail(userCreateDto.getEmail()) != null) {
            throw new UserAlreadyExistsException("User with given email or username already exists.");
        }

        User user = new User(
                userCreateDto.getUsername(),
                userCreateDto.getEmail(),
                userCreateDto.getFirstname(),
                userCreateDto.getSurname(),
                this.getAuthorities(userCreateDto.getAuthority())
        );

        userRepository.save(user);
        mailService.sendMail(new VerificationTokenMailTemplate(
                request, user.getFirstname(), user.getSurname(), user.getVerificationToken().getToken()), user.getEmail()
        );

        UserDto result = UserMapper.entity2dto(user, new UserDto());

        log.info("Rejestracja zakończona. Link wysłany na adres {}. Użytkownik poprawnie dodany: {}", result.getEmail(), result);

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void verifyRegistration(String sToken) throws VerificationTokenNotFoundException {
        log.info("Potwierdzenie rejestracji użytkownika, token: {}", sToken);

        UUID token;
        try {
            token = UUID.fromString(sToken);
        } catch (Exception e) {
            if (log.isDebugEnabled()) log.debug(e.getMessage(), e);
            throw new InvalidUserTokenException("Given token is not valid.");
        }

        VerificationToken verificationToken = userTokensRepository.getVerificationToken(token);
        if (verificationToken == null) {
            throw new VerificationTokenNotFoundException("Given token not found.");
        }

        if (verificationToken.isExpired()) {
            throw new InvalidUserTokenException("Given token is expired.");
        }

        if (verificationToken.isVerified()) {
            if (log.isDebugEnabled()) log.debug("Token jest aktualny i był wcześniej zweryfikowany. Pomijam.");
            return;
        }

        User user = userTokensRepository.getVerificationTokenOwner(verificationToken);

        log.info("Potwierdzenie rejestracji dla usera: {}.", user.getUsername());

        if (!user.getStatus().equals(User.Status.NEW)) {
            throw new IllegalStateException("Confirmation with given token is impossible.");
        }

        user.getVerificationToken().setVerified(true);
        user.setStatus(User.Status.VERIFIED);
        userRepository.save(user);

        log.info("Potwierdzenie rejestracji usera {} zakończone poprawnie.", user.getUsername());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void finalizeRegistration(String sToken, String password) {
        log.info("Finalizacja rejestracji przez użytkownika dla tokena {}.", sToken);

        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Required password is not given.");
        }

        UUID token;
        try {
            token = UUID.fromString(sToken);
        } catch (Exception e) {
            if (log.isDebugEnabled()) log.debug(e.getMessage(), e);
            throw new IllegalArgumentException("Given token is not valid.");
        }

        VerificationToken verificationToken = userTokensRepository.getVerificationToken(token);
        if (verificationToken == null || verificationToken.isExpired() || !verificationToken.isVerified()) {
            throw new InvalidUserTokenException("Verification token is not valid.");
        }

        User user = userTokensRepository.getVerificationTokenOwner(verificationToken);

        log.info("Finalizacja rejestracji dla usera {}.", user.getUsername());

        if (!user.getStatus().equals(User.Status.VERIFIED)) {
            throw new IllegalStateException("Finalization is impossible.");
        }

        user.setStatus(User.Status.ENABLED);
        user.setPassword(passwordEncoder.encode(password));
        user.setVerificationToken(null);

        userTokensRepository.delete(verificationToken);
        userRepository.save(user);

        SessionUtils.authenticate(user);

        log.info("Finalizacja rejestracji przez użytkownika {} zakończona sukcesem.", user.getUsername());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public UserDto modifyPassword(String newPassword, String oldPassword) throws OldPasswordNotMatchException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Modyfikacja hasła przez użytkownika {}.", currentUser.getUsername());

        if (newPassword == null || newPassword.isEmpty() || oldPassword == null || oldPassword.isEmpty()) {
            throw new IllegalArgumentException("Requested passwords missing.");
        }

        if (!passwordEncoder.matches(oldPassword, currentUser.getPassword())) {
            throw new OldPasswordNotMatchException("Old password does not match.");
        }

        currentUser.setPassword(newPassword);
        currentUser.setModified(Calendar.getInstance().getTime());

        userRepository.save(currentUser);

        log.info("Modyfikacja hasła przez użytkownika {} zakończona sukcesem.", currentUser.getUsername());

        return UserMapper.entity2dto(currentUser, new UserDto());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void resetPassword(String email) throws UserNotFoundException, MessagingException {
        log.info("Obsługa żądania zresetowania hasła dla użytkownika mail={}", email);

        if (email == null || email.isEmpty()) {
            throw new IllegalArgumentException("Requested email missing.");
        }

        User user = userRepository.getUserByEmail(email);
        if (user == null || !user.getStatus().equals(User.Status.ENABLED)) {
            throw new UserNotFoundException("User with given email not found.");
        }

        log.info("Mail {} należy do użytkownika {}", email, user.getUsername());

        user.setPasswordResetToken(new PasswordResetToken());
        userRepository.save(user);

        mailService.sendMail(new PasswordResetTokenMailTemplate(
                request, user.getFirstname(), user.getSurname(), user.getPasswordResetToken().getToken()), user.getEmail()
        );

        log.info("Obsługa żądania zresetowania hasła zakończona sukcesem. Użytkownik { username: {}, email: {}, token: {} }", user.getUsername(), email, user.getPasswordResetToken().getToken());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void verifyPasswordResetToken(String sToken) throws PasswordResetTokenNotFoundException {
        log.info("Weryfikacja linka resetującego hasło z tokenem {}", sToken);

        UUID token;
        try {
            token = UUID.fromString(sToken);
        } catch (Exception e) {
            if (log.isDebugEnabled()) log.debug(e.getMessage(), e);
            throw new IllegalArgumentException("Given token is not valid.");
        }

        PasswordResetToken passwordResetToken = userTokensRepository.getPasswordResetToken(token);
        if (passwordResetToken == null) {
            throw new PasswordResetTokenNotFoundException("Given token not found.");
        }

        if (passwordResetToken.isExpired()) {
            throw new InvalidUserTokenException("Given token is expired.");
        }

        if (passwordResetToken.isVerified()) {
            if (log.isDebugEnabled()) log.debug("Token jest aktualny i był wcześniej zweryfikowany. Pomijam.");
            return;
        }

        User user = userTokensRepository.getPasswordResetTokenOwner(passwordResetToken);

        log.info("Reset hasła dla usera {}.", user.getUsername());

        if (!user.getStatus().equals(User.Status.ENABLED)) {
            throw new IllegalStateException("Password reset for nonactive user is impossible.");
        }

        passwordResetToken.setVerified(true);
        userTokensRepository.saveToken(passwordResetToken);

        log.info("Weryfikacja linka resetującego hasło usera {} zakończone poprawnie.", user.getUsername());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void finalizePasswordReset(String sToken, String password) {
        log.info("Finalizacja obsługi resetowania hasła przez użytkownika dla tokena {}.", sToken);

        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Required password is not given.");
        }

        UUID token;
        try {
            token = UUID.fromString(sToken);
        } catch (Exception e) {
            if (log.isDebugEnabled()) log.debug(e.getMessage(), e);
            throw new IllegalArgumentException("Given token is not valid.");
        }

        PasswordResetToken passwordResetToken = userTokensRepository.getPasswordResetToken(token);
        if (passwordResetToken == null || passwordResetToken.isExpired() || !passwordResetToken.isVerified()) {
            throw new InvalidUserTokenException("Password reset token is not valid.");
        }

        User user = userTokensRepository.getPasswordResetTokenOwner(passwordResetToken);

        log.info("Reset hasła dla usera {}.", user.getUsername());

        if (!user.getStatus().equals(User.Status.ENABLED)) {
            throw new IllegalStateException("Password reset is impossible. User is nonactive.");
        }

        user.setPassword(passwordEncoder.encode(password));
        user.setPasswordResetToken(null);

        userTokensRepository.delete(passwordResetToken);
        userRepository.save(user);

        SessionUtils.authenticate(user);

        log.info("Finalizacja obsługi resetowania hasła przez użytkownika {} zakończona sukcesem.", user.getUsername());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDto getCurrentUser() {
        if (log.isDebugEnabled()) log.debug("Wyciągam aktualnie zalogowanego użytkownika.");

        return UserMapper.entity2dto(userRepository.getCurrentUser(), new UserDto());
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<UserDto> findUsers(String query, Integer page, Integer qty) {
        if (log.isDebugEnabled())
            log.debug("Wyszukiwanie użytkowników: { query: {}, page: {}, qty: {} }", query, page, qty);

        if (page == null || page < 0) {
            throw new IllegalArgumentException("Page number is missing.");
        }

        if (qty == null || qty < 1) {
            throw new IllegalArgumentException("Quantity number is missing.");
        }

        Page<User> users = userRepository.findUsers(query, page, qty);

        if (users.hasContent()) {
            List<UserDto> result = new ArrayList<>();
            for (User user : users.getContent()) {
                result.add(UserMapper.entity2dto(user, new UserDto()));
            }

            return new PageImpl<>(result, new PageRequest(page, qty), users.getTotalElements());
        }

        return new PageImpl<>(Collections.EMPTY_LIST);
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public UserDto modifyUser(String username, UserModifyDto userDto) throws UserNotFoundException, UserAlreadyExistsException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Modyfikacja danych użytkownika {} przez usera {}: {}", username, currentUser.getUsername(), userDto);

        if (username == null || username.isEmpty() ||
                userDto.getEmail() == null || userDto.getEmail().isEmpty() ||
                userDto.getFirstname() == null || userDto.getFirstname().isEmpty() ||
                userDto.getSurname() == null || userDto.getSurname().isEmpty() ||
                userDto.getAuthority() == null) {
            throw new IllegalArgumentException("Required data is not valid.");
        }

        User user = userRepository.getUserByEmail(userDto.getEmail());
        if (user != null && !user.getUsername().equals(username)) {
            throw new UserAlreadyExistsException("User with given email already exists.");
        }

        user = userRepository.getUser(username);
        if (user == null) {
            throw new UserNotFoundException("User with given username not found.");
        }

        user.setModified(Calendar.getInstance().getTime());
        user.setEmail(userDto.getEmail());
        user.setFirstname(userDto.getFirstname());
        user.setSurname(userDto.getSurname());

        if (!user.getAuthorities().contains(User.Authority.SUPER_USER)) {
            user.setAuthorities(this.getAuthorities(userDto.getAuthority()));
        }

        userRepository.save(user);

        UserDto result = UserMapper.entity2dto(user, new UserDto());

        log.info("Modyfikacja danych użytkownika {} zakończona sukcesem: {}", username, result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public UserDto deactivateUser(String username) throws UserNotFoundException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Dezaktywacja użytkownika {} przez usera {}.", username, currentUser.getUsername());

        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Required data is not valid.");
        }

        if (currentUser.getUsername().equals(username)) {
            throw new AccessDeniedException("Deactivation is forbidden for yourself.");
        }

        User user = userRepository.getUser(username);
        if (user == null) {
            throw new UserNotFoundException("User with given username not found.");
        }

        if (!user.getStatus().equals(User.Status.ENABLED)) {
            throw new IllegalStateException("Deactivation is forbidden for current user status.");
        }

        if (user.getAuthorities().contains(User.Authority.SUPER_USER)) {
            throw new AccessDeniedException("Admin deactivation is forbidden.");
        }

        user.setModified(Calendar.getInstance().getTime());
        user.setStatus(User.Status.DISABLED);

        userRepository.save(user);
        SessionUtils.logout(sessionRegistry, user);

        UserDto result = UserMapper.entity2dto(user, new UserDto());

        log.info("Dezaktywacja użytkownika {} zakończona sukcesem: {}", username, result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public UserDto reactivateUser(String username) throws UserNotFoundException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Reaktywacja użytkownika {} przez usera {}.", username, currentUser.getUsername());

        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Required username is not valid.");
        }

        User user = userRepository.getUser(username);
        if (user == null) {
            throw new UserNotFoundException("User with given username not found.");
        }

        if (!user.getStatus().equals(User.Status.DISABLED)) {
            throw new IllegalStateException("Reactivation is forbidden for current user status.");
        }

        user.setModified(Calendar.getInstance().getTime());
        user.setStatus(User.Status.ENABLED);
        userRepository.save(user);

        UserDto result = UserMapper.entity2dto(user, new UserDto());

        log.info("Reaktywacja użytkownika {} zakończona sukcesem: {}", username, result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_USER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteUser(String username) throws UserNotFoundException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Usuwanie użytkownika {} przez usera {}.", username, currentUser.getUsername());

        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Required data is not valid.");
        }

        if (currentUser.getUsername().equals(username)) {
            throw new AccessDeniedException("Delete yourself is forbidden.");
        }

        User user = userRepository.getUser(username);
        if (user == null) {
            throw new UserNotFoundException("User with given username not found.");
        }

        if (!Arrays.asList(User.Status.NEW, User.Status.VERIFIED).contains(user.getStatus())) {
            throw new IllegalStateException("Delete is forbidden for current user status.");
        }

        if (user.getAuthorities().contains(User.Authority.SUPER_USER)) {
            throw new AccessDeniedException("Admin deletion is forbidden.");
        }

        SessionUtils.logout(sessionRegistry, user);
        userRepository.delete(user);

        log.info("Usuwanie użytkownika {} zakończona sukcesem.", username);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserSimpleDto getUser(String username) throws UserNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam użytkownika o loginie {}.", username);

        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Required data is not valid.");
        }

        User user = userRepository.getUser(username);
        if (user == null) {
            throw new UserNotFoundException("User with given username not found.");
        }

        return UserMapper.entity2dto(user, new UserSimpleDto());
    }

    /**
     * Nadaje odpowiednie uprawnienia na zasadzie jeżeli mamy rozszerzone, uwzględnij też te niższe.
     *
     * @param authority
     * @return
     */
    private List<User.Authority> getAuthorities(User.Authority authority) {
        return authority.equals(User.Authority.READ_ONLY) ? Arrays.asList(User.Authority.READ_ONLY) : Arrays.asList(User.Authority.READ_ONLY, User.Authority.READ_WRITE);
    }

}
