package pl.optyk.portal.user.mapper;


import pl.optyk.portal.user.dto.UserDto;
import pl.optyk.portal.user.dto.UserSimpleDto;
import pl.optyk.portal.user.model.User;

/**
 * Mapper dla konta użytkownika.
 */
public class UserMapper {

    public static UserDto entity2dto(User entity, UserDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setUsername(entity.getUsername());
        dto.setEmail(entity.getEmail());
        dto.setFirstname(entity.getFirstname());
        dto.setSurname(entity.getSurname());
        dto.setCreated(entity.getCreated());
        dto.setModified(entity.getModified());
        dto.setAuthorities(entity.getAuthorities());
        dto.setStatus(entity.getStatus());

        return dto;
    }

    public static UserSimpleDto entity2dto(User entity, UserSimpleDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setUsername(entity.getUsername());
        dto.setFirstname(entity.getFirstname());
        dto.setSurname(entity.getSurname());

        return dto;
    }

}
