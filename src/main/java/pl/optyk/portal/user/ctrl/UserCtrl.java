package pl.optyk.portal.user.ctrl;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.user.dto.UserCreateDto;
import pl.optyk.portal.user.dto.UserDto;
import pl.optyk.portal.user.dto.UserModifyDto;
import pl.optyk.portal.user.dto.UserSimpleDto;
import pl.optyk.portal.user.exception.*;

import javax.mail.MessagingException;
import java.util.Map;

/**
 * Endpoint do zarządzania kontami użytkowników.
 */
@Component
public interface UserCtrl {

    /**
     * Tworzy nowego użytkownika
     *
     * @param userCreateDto
     * @return
     */
    UserDto createUser(UserCreateDto userCreateDto) throws MessagingException, UserAlreadyExistsException;

    /**
     * Potwierdzenie rejestracji.
     *
     * @param token
     * @return
     */
    void verifyRegistration(String token) throws VerificationTokenNotFoundException;

    /**
     * Finalizuje rejestrację użytkownika.
     *
     * @param payload
     */
    void finalizeRegistration(Map<String, String> payload);

    /**
     * Modyfikacja hasła użytkownika aktywnego (status ENABLED).
     *
     * @param password
     * @return
     */
    UserDto modifyPassword(Map<String, String> password) throws OldPasswordNotMatchException;

    /**
     * Reset hasła użytkownika.
     * 1. Szuka użytkownika po mailu.
     * 2. Tworzy i zapisuje nowy token.
     * 3. Wysyła ten token w linku na podanego maila.
     *
     * @param email
     */
    void resetPassword(Map<String, String> email) throws UserNotFoundException, MessagingException;

    /**
     * Weryfikacja linka resetującego hasło. Po poprawnym wykonaniu, user jest
     * przekierowany do strony, gdzie może ustawić nowe hasło.
     *
     * @param token
     */
    void verifyPasswordResetToken(String token) throws PasswordResetTokenNotFoundException;

    /**
     * Finalizacja obsługi resetowania hasła. Dostęp jedynie dla aktywnego, zalogowanego użytkownika.
     * 1. Szuka tokena resetującego hasło (jeżeli go nie znajdzie, wygasł lub nie został wcześniej potwierdzony - zmiana jest odrzucona).
     * 2. Aktualizuje hasło.
     * 3. Usuwa token powiązany z aktualnie zalogowanym userem.
     *
     * @param payload
     */
    void finalizePasswordReset(Map<String, String> payload);

    /**
     * Pobiera aktualnie zalogowanego użytkownika.
     *
     * @return
     */
    UserDto getCurrentUser();

    /**
     * Wyszukiwanie użytkowników po podanym query. Wyniki posortowane po nazwisku.
     *
     * @param query
     * @return
     */
    Page<UserDto> findUsers(String query, Integer page, Integer qty);

    /**
     * Modyfikacja użytkownika. Zmiana loginu jest niedostępna. Metoda dostępna jest dla admina - może tym
     * zmodyfikować dane każdego użytkownika, oprócz siebie. Zmiany danych własnych z wykorzystaniem innej metody.
     *
     * @param username
     * @param userDto
     * @return
     */
    UserDto modifyUser(String username, UserModifyDto userDto) throws UserNotFoundException, UserAlreadyExistsException;

    /**
     * Dezaktywacja konta:
     * - brak możliwości cofnięcia,
     * - dostęp ma jedynie admin,
     * - dezaktywacja admina niemożliwa,
     * - dezaktywacja jedynie dla użytkownika aktywnego (przed aktywacją można usunąć),
     * - dezaktywacja czyści uprawnienia.
     *
     * @param username
     * @return
     */
    UserDto deactivateUser(String username) throws UserNotFoundException;

    /**
     * Reaktywacja konta wcześniej aktywowanego:
     * - dostęp ma jedynie admin,
     * - reaktywacja jedynie dla użytkownika nieaktywnego,
     * - operacja zmienia status na ENABLED,
     * - po zakończeniu, user może się zalogować lub zresetować hasło, jeżeli je zapomniał.
     *
     * @param username
     * @return
     */
    UserDto reactivateUser(String username) throws UserNotFoundException;

    /**
     * Usuwanie konta:
     * - brak możliwości cofnięcia,
     * - dostęp ma jedynie admin,
     * - usunięcie admina niemożliwa,
     * - usunięcie jedynie dla użytkownika przed aktywacją, później można dezaktywoć,
     * - operacja permanentnie usuwa użytkownika i wylogowuje go.
     *
     * @param username
     * @return
     */
    void deleteUser(String username) throws UserNotFoundException;

    /**
     * Wyciąganie użytkownika po loginie (tylko dane publiczne).
     *
     * @param username
     * @return
     */
    UserSimpleDto getUser(String username) throws UserNotFoundException;

}
