package pl.optyk.portal.user.ctrl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.optyk.portal.user.dto.UserCreateDto;
import pl.optyk.portal.user.dto.UserDto;
import pl.optyk.portal.user.dto.UserModifyDto;
import pl.optyk.portal.user.dto.UserSimpleDto;
import pl.optyk.portal.user.exception.*;
import pl.optyk.portal.user.service.UserService;

import javax.mail.MessagingException;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserCtrlBean implements UserCtrl {

    private static final Logger log = LogManager.getLogger(UserCtrlBean.class);

    @Autowired
    private UserService userService;

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public UserDto createUser(@RequestBody UserCreateDto userCreateDto) throws MessagingException, UserAlreadyExistsException {
        return userService.createUser(userCreateDto);
    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/verify_registration",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public void verifyRegistration(@RequestParam(required = false) String token) throws VerificationTokenNotFoundException {
        userService.verifyRegistration(token);

    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/verify_registration",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public void finalizeRegistration(@RequestBody Map<String, String> payload) {
        userService.finalizeRegistration(payload.get("token"), payload.get("password"));

    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/me/password",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public UserDto modifyPassword(@RequestBody Map<String, String> password) throws OldPasswordNotMatchException {
        return userService.modifyPassword(password.get("newPassword"), password.get("oldPassword"));
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/reset_password",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public void resetPassword(@RequestBody Map<String, String> email) throws UserNotFoundException, MessagingException {
        userService.resetPassword(email.get("email"));
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/reset_password",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public void verifyPasswordResetToken(@RequestParam(required = false) String token) throws PasswordResetTokenNotFoundException {
        userService.verifyPasswordResetToken(token);

    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/reset_password",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public void finalizePasswordReset(@RequestBody Map<String, String> payload) {
        userService.finalizePasswordReset(payload.get("token"), payload.get("password"));

    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/me",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public UserDto getCurrentUser() {
        return userService.getCurrentUser();
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public Page<UserDto> findUsers(
            @RequestParam(required = false) String query,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "20") Integer qty
    ) {
        return userService.findUsers(query, page, qty);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/{username}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public UserDto modifyUser(@PathVariable String username, @RequestBody UserModifyDto userDto) throws UserNotFoundException, UserAlreadyExistsException {
        return userService.modifyUser(username, userDto);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/{username}/deactivate",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public UserDto deactivateUser(@PathVariable String username) throws UserNotFoundException {
        return userService.deactivateUser(username);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/{username}/reactivate",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public UserDto reactivateUser(@PathVariable String username) throws UserNotFoundException {
        return userService.reactivateUser(username);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/{username}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.DELETE
    )
    public void deleteUser(@PathVariable String username) throws UserNotFoundException {
        userService.deleteUser(username);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/users/{username}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public UserSimpleDto getUser(@PathVariable String username) throws UserNotFoundException {
        return userService.getUser(username);
    }

}
