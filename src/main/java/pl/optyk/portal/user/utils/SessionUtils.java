package pl.optyk.portal.user.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import pl.optyk.portal.user.model.User;

/**
 * Narzędzie ręcznie logujące użytkownika do sesji.
 */
public class SessionUtils {
    private static final Logger log = LogManager.getLogger(SessionUtils.class);

    public static void authenticate(User user) {
        log.info("Loguję użytkownika {} do sesji.", user.getUsername());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                user.getUsername(), user.getPassword(), user.getAuthorities()
        ));

        if (log.isDebugEnabled()) log.debug("Użytkownik {} poprawnie zalogowany.", user.getUsername());

    }

    public static void logout(SessionRegistry sessionRegistry, User user) {
        log.info("Wylogowuję użytkownika {} z sesji.", user.getUsername());

        boolean logout = false;
        try {
            for (Object principal : sessionRegistry.getAllPrincipals()) {
                if (principal instanceof org.springframework.security.core.userdetails.User) {
                    UserDetails userDetails = (UserDetails) principal;
                    if (userDetails.getUsername().equals(user.getUsername())) {
                        logout = true;
                        for (SessionInformation information : sessionRegistry.getAllSessions(userDetails, true)) {
                            information.expireNow();
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Błąd podczas wylogowania użytkownika %s z sesji: %s", user.getUsername(), e.getMessage()), e);
        }

        log.info("Obsługa wylogowania użytkownika {} z sesji zakończona poprawnie. Wylogowano: {}", user.getUsername(), logout);

    }

}
