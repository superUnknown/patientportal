package pl.optyk.portal.user.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Encja tokena do zresetowania hasła przez użytkownika.
 */
@Entity
@Table(name = "PasswordResetToken")
@NamedQueries({
        @NamedQuery(
                name = PasswordResetToken.GET_PASSWORD_RESET_TOKEN_OWNER,
                query = "SELECT u FROM User u " +
                        "   JOIN u.passwordResetToken t " +
                        "WHERE t = :token"
        )
})
public class PasswordResetToken extends Token {

    public final static String GET_PASSWORD_RESET_TOKEN_OWNER = "GET_PASSWORD_RESET_TOKEN_OWNER";

    private static final int TOKEN_ACTIVITY_TIME_IN_HOURS = 24;

    /**
     * Wygenerowany token
     */
    private UUID token;

    /**
     * Kiedy wygasa
     */
    private Date expires;

    /**
     * Czy jest zweryfikowany. Flaga ustawiana po kliknięciu w link resetujący hasło.
     * Tylko zweryfikowanym tokenem można zmienić hasło bez podania poprzedniego.
     */
    private boolean verified = false;

    public PasswordResetToken() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, TOKEN_ACTIVITY_TIME_IN_HOURS);
        this.expires = cal.getTime();
        this.token = UUID.randomUUID();

    }

    @Id
    @Override
    @Type(type = "uuid-char")
    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    @Override
    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    @Override
    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

}
