package pl.optyk.portal.user.model;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Token użytkownika.
 * Created by superUnknown on 29.09.2016.
 */
public abstract class Token {

    public abstract UUID getToken();

    public abstract Date getExpires();

    public abstract boolean isVerified();

    public boolean isExpired() {
        return this.getExpires().before(Calendar.getInstance().getTime());

    }

}
