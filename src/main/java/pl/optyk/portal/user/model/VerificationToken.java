package pl.optyk.portal.user.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Encja tokenu weryfikacyjnego użytkownika.
 */
@Entity
@Table(name = "VerificationToken")
@NamedQueries({
        @NamedQuery(
                name = VerificationToken.GET_VERIFICATION_TOKEN_OWNER,
                query = "SELECT u FROM User u " +
                        "   JOIN u.verificationToken t " +
                        "WHERE t = :token"
        )
})
public class VerificationToken extends Token {

    public final static String GET_VERIFICATION_TOKEN_OWNER = "GET_VERIFICATION_TOKEN_OWNER";

    private static final int TOKEN_ACTIVITY_TIME_IN_HOURS = 48;

    private UUID token;

    private Date expires;

    /**
     * Czy jest zweryfikowany. Flaga ustawiana po kliknięciu w link weryfikujący rejestrację.
     * Tylko zweryfikowanym tokenem można ustawić hasło i zfinalizować dodanie nowego konta.
     */
    private boolean verified = false;

    public VerificationToken() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, TOKEN_ACTIVITY_TIME_IN_HOURS);
        this.expires = cal.getTime();
        this.token = UUID.randomUUID();

    }

    @Id
    @Type(type = "uuid-char")
    @Override
    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    @Override
    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    @Override
    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

}
