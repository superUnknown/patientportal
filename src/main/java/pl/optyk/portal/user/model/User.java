package pl.optyk.portal.user.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Encja użytkownika.
 * Created by superUnknown on 28.09.2016.
 */
@Entity
@Table(name = "User")
@NamedQueries({
        @NamedQuery(
                name = User.GET_USER_BY_EMAIL,
                query = "SELECT u FROM User u WHERE u.email = :email"
        ),
        @NamedQuery(
                name = User.FIND_USERS,
                query = "SELECT u FROM User u " +
                        "WHERE " +
                        "   (:query IS NULL AND u IS NOT NULL) OR (:query IS NOT NULL AND (" +
                        "       u.username LIKE :query OR " +
                        "       u.email LIKE :query OR " +
                        "       u.firstname LIKE :query OR " +
                        "       u.surname LIKE :query" +
                        "   )) " +
                        "ORDER BY u.surname ASC"
        ),
        @NamedQuery(
                name = User.FIND_USERS_COUNT,
                query = "SELECT COUNT(*) FROM User u " +
                        "WHERE " +
                        "   (:query IS NULL AND u IS NOT NULL) OR (:query IS NOT NULL AND (" +
                        "       u.username LIKE :query OR " +
                        "       u.email LIKE :query OR " +
                        "       u.firstname LIKE :query OR " +
                        "       u.surname LIKE :query" +
                        "   )) " +
                        "ORDER BY u.surname ASC"
        )
})
public class User {

    public static final String GET_USER_BY_EMAIL = "GET_USER_BY_EMAIL";

    public static final String FIND_USERS = "FIND_USERS";

    public static final String FIND_USERS_COUNT = "FIND_USERS_COUNT";

    private String username;

    private String email;

    private String firstname;

    private String surname;

    private Status status = Status.NEW;

    private String password;

    private Date created = new Date();

    private Date modified;

    private List<Authority> authorities = new ArrayList<>();

    private VerificationToken verificationToken;

    private PasswordResetToken passwordResetToken;

    public User() {
    }

    public User(String username, String email, String firstname, String surname, List<Authority> authorities) {
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.surname = surname;
        this.authorities = authorities;
        this.verificationToken = new VerificationToken();

    }

    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Enumerated(EnumType.STRING)
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "username")
    @CollectionTable(name = "Authority")
    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    @JoinColumn(name = "verificationTokenId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public VerificationToken getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(VerificationToken verificationToken) {
        this.verificationToken = verificationToken;
    }

    @JoinColumn(name = "passwordResetTokenId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public PasswordResetToken getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(PasswordResetToken passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public enum Status {
        NEW, VERIFIED, ENABLED, DISABLED
    }

    public enum Authority implements GrantedAuthority {
        SUPER_USER, READ_ONLY, READ_WRITE;

        @Override
        public String getAuthority() {
            return this.name();
        }

    }
}
