package pl.optyk.portal.user.dto;

/**
 * Dto z danymi użytkownika (tylko dane publiczne).
 */
public class UserSimpleDto {

    private String username;

    private String firstname;

    private String surname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserSimpleDto{");
        sb.append("firstname='").append(firstname).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
