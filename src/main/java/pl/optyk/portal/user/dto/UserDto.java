package pl.optyk.portal.user.dto;

import pl.optyk.portal.user.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Dto z danymi użytkownika.
 */
public class UserDto {

    private String username;

    private String email;

    private String firstname;

    private String surname;

    private User.Status status;

    private Date created;

    private Date modified;

    private List<User.Authority> authorities = new ArrayList<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public User.Status getStatus() {
        return status;
    }

    public void setStatus(User.Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public List<User.Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<User.Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserDto{");
        sb.append("username='").append(username).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", status=").append(status);
        sb.append(", created=").append(created);
        sb.append(", modified=").append(modified);
        sb.append(", authorities=").append(authorities);
        sb.append('}');
        return sb.toString();
    }

}
