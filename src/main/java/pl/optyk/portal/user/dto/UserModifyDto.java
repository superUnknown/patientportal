package pl.optyk.portal.user.dto;

import pl.optyk.portal.user.model.User;

/**
 * Dto z danymi do modyfikacji konta użytkownika.
 */
public class UserModifyDto {

    private String email;

    private String firstname;

    private String surname;

    private User.Authority authority;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public User.Authority getAuthority() {
        return authority;
    }

    public void setAuthority(User.Authority authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserCreateDto{");
        sb.append("email='").append(email).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", authorities=").append(authority);
        sb.append('}');
        return sb.toString();
    }

}
