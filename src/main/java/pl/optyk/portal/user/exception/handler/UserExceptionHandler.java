package pl.optyk.portal.user.exception.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.optyk.portal.common.exception.handler.RestException;
import pl.optyk.portal.user.exception.*;

/**
 * Handler RESTowy wyjątków konta.
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class UserExceptionHandler {
    private static final Logger log = LogManager.getLogger(UserExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(VerificationTokenNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestException verificationTokenNotFoundExceptionHandler(VerificationTokenNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.NOT_FOUND.value(), "verification_token_not_found", "Token not found.");
    }

    @ResponseBody
    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public RestException userAlreadyExistsExceptionHandler(UserAlreadyExistsException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.CONFLICT.value(), "user_already_exists", "User with given email or username already exists.");
    }

    @ResponseBody
    @ExceptionHandler(OldPasswordNotMatchException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public RestException oldPasswordNotMatchExceptionHandler(OldPasswordNotMatchException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.CONFLICT.value(), "old_password_not_match", "Old password does not match.");
    }

    @ResponseBody
    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public RestException usernameNotFoundExceptionHandler(UsernameNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.UNAUTHORIZED.value(), "user_not_logged_in", "User is not logged in.");
    }

    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public RestException accessDeniedExceptionHandler(AccessDeniedException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.FORBIDDEN.value(), "access_denied", "Access denied.");
    }

    @ResponseBody
    @ExceptionHandler(PasswordResetTokenNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestException passwordResetTokenNotFoundExceptionHandler(PasswordResetTokenNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.NOT_FOUND.value(), "password_reset_token_not_found", "Token not found.");
    }

    @ResponseBody
    @ExceptionHandler(InvalidUserTokenException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestException invalidUserTokenExceptionHandler(InvalidUserTokenException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.BAD_REQUEST.value(), "invalid_user_token", "Token is invalid.");
    }

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestException userNotFoundExceptionHandler(UserNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.NOT_FOUND.value(), "user_not_found", "User not found.");
    }

}
