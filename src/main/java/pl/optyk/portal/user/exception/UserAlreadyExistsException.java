package pl.optyk.portal.user.exception;

/**
 * Created by superUnknown on 29.09.2016.
 */
public class UserAlreadyExistsException extends Exception {
    public UserAlreadyExistsException(String msg) {
        super(msg);
    }
}
