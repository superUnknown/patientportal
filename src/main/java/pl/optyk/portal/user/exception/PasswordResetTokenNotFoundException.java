package pl.optyk.portal.user.exception;

public class PasswordResetTokenNotFoundException extends Exception {
    public PasswordResetTokenNotFoundException(String msg) {
        super(msg);
    }
}
