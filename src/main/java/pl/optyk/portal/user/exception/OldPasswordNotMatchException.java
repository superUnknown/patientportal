package pl.optyk.portal.user.exception;

/**
 * Created by superUnknown on 29.09.2016.
 */
public class OldPasswordNotMatchException extends Exception {
    public OldPasswordNotMatchException(String msg) {
        super(msg);
    }
}
