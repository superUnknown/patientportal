package pl.optyk.portal.user.exception;

public class InvalidUserTokenException extends IllegalArgumentException {
    public InvalidUserTokenException(String msg) {
        super(msg);
    }
}
