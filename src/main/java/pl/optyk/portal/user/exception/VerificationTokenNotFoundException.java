package pl.optyk.portal.user.exception;

/**
 * Created by superUnknown on 29.09.2016.
 */
public class VerificationTokenNotFoundException extends Exception {
    public VerificationTokenNotFoundException(String msg) {
        super(msg);
    }
}
