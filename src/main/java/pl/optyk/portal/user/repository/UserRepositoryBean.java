package pl.optyk.portal.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.repository.AbstractRepository;
import pl.optyk.portal.user.model.User;

import java.util.List;

@Repository
public class UserRepositoryBean extends AbstractRepository implements UserRepository {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User getUser(String username) {
        return (User) this.em.find(User.class, username);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User getCurrentUser() {
        User currentUser = this.getUser(SecurityContextHolder.getContext().getAuthentication().getName());

        if (currentUser == null) {
            throw new UsernameNotFoundException("User is not logged in.");
        }

        return currentUser;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User getUserByEmail(String email) {
        return (User) this.em.createNamedQuery(User.GET_USER_BY_EMAIL)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public User save(User user) {
        this.em.persist(user);

        return user;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<User> findUsers(String query, Integer page, Integer qty) {
        List results = this.em.createNamedQuery(User.FIND_USERS)
                .setParameter("query", query == null ? null : "%" + query + "%")
                .setFirstResult(page * qty)
                .setMaxResults(qty)
                .getResultList();

        long totalCount = (long) this.em.createNamedQuery(User.FIND_USERS_COUNT)
                .setParameter("query", query == null ? null : "%" + query + "%")
                .getSingleResult();

        return new PageImpl<>(results, new PageRequest(page, qty), totalCount);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void delete(User user) {
        this.em.remove(user);
    }

}
