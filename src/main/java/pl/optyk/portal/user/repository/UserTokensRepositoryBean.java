package pl.optyk.portal.user.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.repository.AbstractRepository;
import pl.optyk.portal.user.model.PasswordResetToken;
import pl.optyk.portal.user.model.Token;
import pl.optyk.portal.user.model.User;
import pl.optyk.portal.user.model.VerificationToken;

import java.util.UUID;

@Repository
public class UserTokensRepositoryBean extends AbstractRepository implements UserTokensRepository {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public VerificationToken getVerificationToken(UUID token) {
        return this.em.find(VerificationToken.class, token);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User getVerificationTokenOwner(VerificationToken token) {
        return (User) this.em.createNamedQuery(VerificationToken.GET_VERIFICATION_TOKEN_OWNER)
                .setParameter("token", token)
                .getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PasswordResetToken getPasswordResetToken(UUID token) {
        return (PasswordResetToken) this.em.find(PasswordResetToken.class, token);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User getPasswordResetTokenOwner(PasswordResetToken token) {
        return (User) this.em.createNamedQuery(PasswordResetToken.GET_PASSWORD_RESET_TOKEN_OWNER)
                .setParameter("token", token)
                .getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void saveToken(Token token) {
        this.em.persist(token);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void delete(Token token) {
        this.em.remove(token);
    }

}
