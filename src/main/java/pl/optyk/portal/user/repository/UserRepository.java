package pl.optyk.portal.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.optyk.portal.user.model.User;

/**
 * DAO dla kont użytkowników.
 */
@Component
public interface UserRepository {

    /**
     * Pobiera konto po loginie. Jeżeli brak, return NULL.
     *
     * @param username
     * @return
     */
    User getUser(String username);

    /**
     * Pobiera konto aktualnie zalogowanego użytkownika.
     *
     * @return
     * @throws UsernameNotFoundException
     */
    User getCurrentUser();

    /**
     * Pobiera konto po mailu. Jeżeli brak, return NULL.
     *
     * @param email
     * @return
     */
    User getUserByEmail(String email);

    /**
     * Persystuje konto.
     *
     * @param user
     * @return
     */
    User save(User user);

    /**
     * Wyszukiwanie użytkowników po podanym query. Wyniki posortowane po nazwisku.
     *
     * @param query
     * @param page
     * @param qty
     * @return
     */
    Page<User> findUsers(String query, Integer page, Integer qty);

    /**
     * Permanentnie usuwa użytkownika.
     *
     * @param user
     */
    void delete(User user);

}
