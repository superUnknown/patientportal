package pl.optyk.portal.user.repository;

import org.springframework.stereotype.Component;
import pl.optyk.portal.user.model.PasswordResetToken;
import pl.optyk.portal.user.model.Token;
import pl.optyk.portal.user.model.User;
import pl.optyk.portal.user.model.VerificationToken;

import java.util.UUID;

/**
 * DAO dla tokenów użytkownika.
 */
@Component
public interface UserTokensRepository {

    /**
     * Pobiera token weryfikacyjny rejestracji użytkownika.
     *
     * @param token
     * @return
     */
    VerificationToken getVerificationToken(UUID token);

    /**
     * Wyciąga właściciela tokena weryfikacyjnego.
     *
     * @param token
     * @return
     */
    User getVerificationTokenOwner(VerificationToken token);

    /**
     * Pobiera token do zresetowania hasła.
     *
     * @param token
     * @return
     */
    PasswordResetToken getPasswordResetToken(UUID token);

    /**
     * Wyciąga właściciela tokena do zresetowania hasła.
     *
     * @param token
     * @return
     */
    User getPasswordResetTokenOwner(PasswordResetToken token);

    /**
     * Zapisuje zmiany dla tokena użytkownika.
     *
     * @param token
     */
    void saveToken(Token token);

    /**
     * Usuwa token weryfikacyjny.
     *
     * @param token
     */
    void delete(Token token);

}
