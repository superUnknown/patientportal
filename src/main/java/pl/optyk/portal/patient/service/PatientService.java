package pl.optyk.portal.patient.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.patient.dto.PatientDto;
import pl.optyk.portal.patient.dto.PatientSaveDto;
import pl.optyk.portal.patient.exception.PatientAlreadyExistsException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

import java.util.Date;
import java.util.UUID;

/**
 * Usługa do zarządzania pacjentami.
 * Created by superUnknown on 08.10.2016.
 */
@Component
public interface PatientService {

    /**
     * Pobieranie danych pacjenta po jego id.
     *
     * @param patientId
     * @return
     */
    PatientDto getPatient(UUID patientId) throws PatientNotFoundException;

    /**
     * Tworzenie nowego pacjenta. Operacja dostępna dla użytkownika posiadającego prawa zapisu.
     * Zakładając nowego pacjenta, trzeba wypełnić wymagane dane:
     * - firstname,
     * - surname,
     * - dateOfBirth,
     * - address (pełny).
     *
     * @param patientDto
     * @return
     */
    PatientDto createPatient(PatientSaveDto patientDto) throws PatientAlreadyExistsException;

    /**
     * Modyfikacja danych istniejącego pacjenta. Operacja dostępna dla użytkownika posiadającego prawa
     * zapisu. Modyfikując pacjenta, trzeba wypełnić wymagane dane:
     * - firstname,
     * - surname,
     * - dateOfBirth,
     * - address (pełny).
     *
     * @param patientDto
     * @return
     */
    PatientDto modifyPatient(UUID patientId, PatientSaveDto patientDto) throws PatientNotFoundException, PatientAlreadyExistsException;

    /**
     * Wyszukiwanie pacjentow po podanym query (imię, nazwisko). Wyniki posortowane po nazwisku. Znaki dozwolone w query: [a-zA-Z].
     *
     * @param query
     * @param page
     * @param qty
     * @return
     */
    Page<PatientDto> findPatients(String query, Integer page, Integer qty);

    /**
     * Pobieranie danych pacjenta po jego imieniu, nazwisku i dacie urodzenia - dla importu.
     *
     * @param firstname
     * @param surname
     * @param dateOfBirth
     * @return
     */
    PatientDto getPatient(String firstname, String surname, Date dateOfBirth);

}
