package pl.optyk.portal.patient.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.patient.repository.AddressRepository;

import java.util.List;

@Service
public class AddressServiceBean implements AddressService {

    private static final Logger log = LogManager.getLogger(AddressServiceBean.class);

    @Autowired
    private AddressRepository addressRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllCities() {
        if (log.isDebugEnabled()) log.debug("Wyciągam listę dostępnych miast.");

        return addressRepository.getAllCities();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllCountries() {
        if (log.isDebugEnabled()) log.debug("Wyciągam listę dostępnych krajów.");

        return addressRepository.getAllCountries();
    }
}
