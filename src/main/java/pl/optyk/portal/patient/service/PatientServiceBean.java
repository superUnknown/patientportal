package pl.optyk.portal.patient.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import pl.optyk.portal.patient.dto.PatientDto;
import pl.optyk.portal.patient.dto.PatientSaveDto;
import pl.optyk.portal.patient.exception.PatientAlreadyExistsException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;
import pl.optyk.portal.patient.mapper.PatientMapper;
import pl.optyk.portal.patient.model.Patient;
import pl.optyk.portal.patient.repository.PatientRepository;
import pl.optyk.portal.user.model.User;
import pl.optyk.portal.user.repository.UserRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PatientServiceBean implements PatientService {

    private static final Logger log = LogManager.getLogger(PatientServiceBean.class);

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PatientDto getPatient(UUID patientId) throws PatientNotFoundException {
        if (log.isDebugEnabled()) log.debug("Wyciągam dane pacjenta id: {}", patientId);

        if (patientId == null) {
            throw new IllegalArgumentException("Required patient id is missing.");
        }

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        return PatientMapper.entity2dto(patient, new PatientDto());
    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PatientDto createPatient(PatientSaveDto patientDto) throws PatientAlreadyExistsException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Dodawanie pacjenta przez usera {}: {}", currentUser.getUsername(), patientDto);

        this.validatePatientData(patientDto);

        Patient patient = PatientMapper.dto2entity(patientDto, new Patient());
        patient.setCreated(Calendar.getInstance().getTime());
        patient.setCreatedBy(currentUser);
        patientRepository.save(patient);

        PatientDto result = PatientMapper.entity2dto(patient, new PatientDto());

        log.info("Dodawanie pacjenta zakończone sukcesem: {}", result);

        return result;
    }

    @Override
    @PreAuthorize("hasAuthority('READ_WRITE')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public PatientDto modifyPatient(UUID patientId, PatientSaveDto patientDto) throws PatientNotFoundException, PatientAlreadyExistsException {
        User currentUser = userRepository.getCurrentUser();

        log.info("Modyfikacja danych pacjenta id: {} przez usera {}: {}", patientId, currentUser.getUsername(), patientDto);

        if (patientId == null) {
            throw new IllegalArgumentException("Required patient id is missing.");
        }

        this.validatePatientData(patientDto);

        Patient patient = patientRepository.getPatient(patientId);
        if (patient == null) {
            throw new PatientNotFoundException("Required patient not found.");
        }

        PatientMapper.dto2entity(patientDto, patient);

        patient.setModified(Calendar.getInstance().getTime());
        patient.setModifiedBy(currentUser);
        patientRepository.save(patient);

        PatientDto result = PatientMapper.entity2dto(patient, new PatientDto());

        log.info("Modyfikacja danych pacjenta zakończona sukcesem: {}", result);

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<PatientDto> findPatients(String query, Integer page, Integer qty) {
        if (log.isDebugEnabled())
            log.debug("Wyszukiwanie pacjentów: { query: {}, page: {}, qty: {} }", query, page, qty);

        if (page == null || page < 0) {
            throw new IllegalArgumentException("Page number is missing.");
        }

        if (qty == null || qty < 1) {
            throw new IllegalArgumentException("Quantity number is missing.");
        }

        query = query.trim().replaceAll("\\s","");
        if (query.isEmpty()) {
            return new PageImpl<>(Collections.<PatientDto>emptyList());
        }
        Page<Patient> patients = patientRepository.findPatients(query, page, qty);

        if (log.isDebugEnabled())
            log.debug("Znaleziono pacjentów: { query: {}, page: {}, qty: {}, found: {} }", query, page, qty, patients.getTotalElements());

        if (patients.hasContent()) {
            List<PatientDto> result = new ArrayList<>();
            for (Patient patient : patients.getContent()) {
                result.add(PatientMapper.entity2dto(patient, new PatientDto()));
            }

            return new PageImpl<>(result, new PageRequest(page, qty), patients.getTotalElements());
        }

        return new PageImpl<>(Collections.EMPTY_LIST);
    }

    private static boolean isUUID(String query) {
        try {
            UUID.fromString(query.trim());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PatientDto getPatient(String firstname, String surname, Date dateOfBirth) {
        return PatientMapper.entity2dto(patientRepository.getPatient(firstname, surname, dateOfBirth), new PatientDto());
    }

    /**
     * Walizacja danych użytkownika przed zapisaniem zmian.
     *
     * @param patient
     */
    private void validatePatientData(PatientSaveDto patient) {
        if (patient.getDateOfBirth() == null || StringUtils.isEmpty(patient.getFirstname()) || StringUtils.isEmpty(patient.getSurname())) {
            throw new IllegalArgumentException("Required data is not valid.");
        }
        if (patient.getRepresentative() != null) {
            if (StringUtils.isEmpty(patient.getRepresentative().getFirstname()) || StringUtils.isEmpty(patient.getRepresentative().getSurname())) {
                throw new IllegalArgumentException("Required data is not valid.");
            }
        }
    }

}
