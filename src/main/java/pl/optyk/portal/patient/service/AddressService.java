package pl.optyk.portal.patient.service;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Usługa danych adresowych.
 * Created by superUnknown on 08.10.2016.
 */
@Component
public interface AddressService {

    /**
     * Pobiera listę podpowiedzi miast, które wcześniej były wpisywane.
     *
     * @return
     */
    List<String> getAllCities();

    /**
     * Pobiera listę podpowiedzi krajów, które wcześniej były wpisywane.
     *
     * @return
     */
    List<String> getAllCountries();

}
