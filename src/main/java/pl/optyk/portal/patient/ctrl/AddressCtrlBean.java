package pl.optyk.portal.patient.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.optyk.portal.patient.service.AddressService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AddressCtrlBean implements AddressCtrl {

    @Autowired
    private AddressService addressService;

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/addresses/cities",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<String> getAllCities() {
        return addressService.getAllCities();
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/addresses/countries",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<String> getAllCountries() {
        return addressService.getAllCountries();
    }
}
