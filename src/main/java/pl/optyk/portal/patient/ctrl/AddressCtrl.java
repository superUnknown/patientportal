package pl.optyk.portal.patient.ctrl;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Endpoint danych adresowych.
 */
@Component
public interface AddressCtrl {

    /**
     * Pobiera listę podpowiedzi miast, które wcześniej były wpisywane.
     *
     * @return
     */
    List<String> getAllCities();

    /**
     * Pobiera listę podpowiedzi krajów, które wcześniej były wpisywane.
     *
     * @return
     */
    List<String> getAllCountries();

}
