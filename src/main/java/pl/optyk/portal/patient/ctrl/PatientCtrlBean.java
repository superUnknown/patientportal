package pl.optyk.portal.patient.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import pl.optyk.portal.patient.dto.PatientDto;
import pl.optyk.portal.patient.dto.PatientSaveDto;
import pl.optyk.portal.patient.exception.PatientAlreadyExistsException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;
import pl.optyk.portal.patient.service.PatientService;

import java.util.UUID;

@RestController
@RequestMapping("/api")
public class PatientCtrlBean implements PatientCtrl {

    @Autowired
    private PatientService patientService;

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public PatientDto getPatient(@PathVariable UUID patientId) throws PatientNotFoundException {
        return patientService.getPatient(patientId);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public PatientDto createPatient(@RequestBody PatientSaveDto patientDto) throws PatientAlreadyExistsException {
        return patientService.createPatient(patientDto);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients/{patientId}",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.PUT
    )
    public PatientDto modifyPatient(@PathVariable UUID patientId, @RequestBody PatientSaveDto patientDto)
            throws PatientAlreadyExistsException, PatientNotFoundException {
        return patientService.modifyPatient(patientId, patientDto);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/patients",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public Page<PatientDto> findPatients(
            @RequestParam(required = false) String query,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "50") Integer qty
    ) {
        return patientService.findPatients(query, page, qty);
    }

}
