package pl.optyk.portal.patient.ctrl;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.patient.dto.PatientDto;
import pl.optyk.portal.patient.dto.PatientSaveDto;
import pl.optyk.portal.patient.exception.PatientAlreadyExistsException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

import java.util.UUID;

/**
 * Endpoint do zarządzania pacjentami.
 */
@Component
public interface PatientCtrl {

    /**
     * Pobieranie danych pacjenta.
     *
     * @param patientId
     * @return
     */
    PatientDto getPatient(UUID patientId) throws PatientNotFoundException;

    /**
     * Tworzenie nowego pacjenta.
     *
     * @param patientDto
     * @return
     */
    PatientDto createPatient(PatientSaveDto patientDto) throws PatientAlreadyExistsException;

    /**
     * Modyfikacja danych pacjenta.
     *
     * @param patientId
     * @param patientDto
     * @return
     */
    PatientDto modifyPatient(UUID patientId, PatientSaveDto patientDto) throws PatientAlreadyExistsException, PatientNotFoundException;

    /**
     * Wyszukiwanie użytkowników po podanym query. Wyniki posortowane po nazwisku.
     *
     * @param query
     * @return
     */
    Page<PatientDto> findPatients(String query, Integer page, Integer qty);

}
