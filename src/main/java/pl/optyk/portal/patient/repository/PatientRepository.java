package pl.optyk.portal.patient.repository;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.optyk.portal.patient.model.Patient;

import java.util.Date;
import java.util.UUID;

/**
 * Dao do zarządzania pacjentami.
 * Created by superUnknown on 08.10.2016.
 */
@Component
public interface PatientRepository {

    /**
     * Pobieranie danych pacjenta po jego id.
     *
     * @param patientId
     * @return
     */
    Patient getPatient(UUID patientId);

    /**
     * Pobieranie danych pacjenta po jego imieniu, nazwisku i dacie urodzenia - dla importu.
     *
     * @param firstname
     * @param surname
     * @param dateOfBirth
     * @return
     */
    Patient getPatient(String firstname, String surname, Date dateOfBirth);

    /**
     * Zapisywanie zmian pacjenta na bazie.
     *
     * @param patient
     * @return
     */
    void save(Patient patient);

    /**
     * Sprawdza, czy pacjent o podanych danych już istnieje.
     *
     * @param patient
     * @return
     */
    boolean isPatientExists(Patient patient);

    /**
     * Wyszukiwanie pacjentów po podanym query. Wyniki posortowane po nazwisku.
     *
     * @param query
     * @param page
     * @param qty
     * @return
     */
    Page<Patient> findPatients(String query, Integer page, Integer qty);

}
