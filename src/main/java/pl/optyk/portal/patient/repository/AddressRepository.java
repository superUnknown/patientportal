package pl.optyk.portal.patient.repository;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Dao do danymi adresowymi.
 * Created by superUnknown on 08.10.2016.
 */
@Component
public interface AddressRepository {

    /**
     * Pobiera listę podpowiedzi miast, które wcześniej były wpisywane.
     *
     * @return
     */
    List getAllCities();

    /**
     * Pobiera listę podpowiedzi krajów, które wcześniej były wpisywane.
     *
     * @return
     */
    List<String> getAllCountries();

}
