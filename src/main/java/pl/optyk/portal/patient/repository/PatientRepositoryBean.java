package pl.optyk.portal.patient.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.repository.AbstractRepository;
import pl.optyk.portal.patient.model.Patient;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class PatientRepositoryBean extends AbstractRepository implements PatientRepository {

    private static final Logger log = LogManager.getLogger(PatientRepository.class);

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Patient getPatient(UUID patientId) {
        return (Patient) this.em.find(Patient.class, patientId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Patient getPatient(String firstname, String surname, Date dateOfBirth) {
        return (Patient) this.em.createNamedQuery(Patient.FIND_PATIENT_FOR_IMPORT_OPERATION)
                .setParameter("firstname", firstname)
                .setParameter("surname", surname)
                .setParameter("dateOfBirth", dateOfBirth)
                .getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void save(Patient patient) {
        this.em.persist(patient);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isPatientExists(Patient patient) {
        Patient p = (Patient) this.em.createNamedQuery(Patient.CHECK_IF_PATIENT_EXISTS)
                .setParameter("patientId", patient.getPatientId())
                .setParameter("firstname", patient.getFirstname())
                .setParameter("surname", patient.getSurname())
                .setParameter("dateOfBirth", patient.getDateOfBirth())
                .getSingleResult();

        if (p != null) {
            return true;
        }

        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<Patient> findPatients(String query, Integer page, Integer qty) {
        log.debug("Wyszukiwanie pacjentów: { query: {}, page: {}, qty: {} }", query, page, qty);
        List results = this.em.createNamedQuery(Patient.FIND_PATIENTS)
                .setParameter("query", query == null ? null : "%" + query + "%")
                .setFirstResult(page * qty)
                .setMaxResults(qty)
                .getResultList();
        long totalCount = (long) this.em.createNamedQuery(Patient.FIND_PATIENTS_COUNT)
                .setParameter("query", query == null ? null : "%" + query + "%")
                .getSingleResult();
        log.debug("Znaleziono pacjentów: { query: {}, page: {}, qty: {}, found: {} }", query, page, qty, results.size());
        return new PageImpl<>(results, new PageRequest(page, qty), totalCount);
    }

}
