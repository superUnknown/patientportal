package pl.optyk.portal.patient.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.optyk.portal.common.repository.AbstractRepository;

import java.util.Collections;
import java.util.List;

@Repository
public class AddressRepositoryBean extends AbstractRepository implements AddressRepository {

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllCities() {
        List result = this.em.createQuery("SELECT DISTINCT a.city FROM Address a").getResultList();
        if (result == null || result.isEmpty()) {
            return Collections.emptyList();
        }

        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getAllCountries() {
        List result = this.em.createQuery("SELECT DISTINCT a.country FROM Address a").getResultList();
        if (result == null || result.isEmpty()) {
            return Collections.emptyList();
        }

        return result;
    }
}
