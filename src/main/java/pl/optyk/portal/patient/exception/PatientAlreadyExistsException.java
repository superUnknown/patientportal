package pl.optyk.portal.patient.exception;

public class PatientAlreadyExistsException extends Exception {
    public PatientAlreadyExistsException(String msg) {
        super(msg);
    }
}
