package pl.optyk.portal.patient.exception;

public class PatientNotFoundException extends Exception {
    public PatientNotFoundException(String msg) {
        super(msg);
    }
}
