package pl.optyk.portal.patient.exception.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.optyk.portal.common.exception.handler.RestException;
import pl.optyk.portal.patient.exception.PatientAlreadyExistsException;
import pl.optyk.portal.patient.exception.PatientNotFoundException;

/**
 * Handler RESTowy wyjątków pacjentów.
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class PatientExceptionHandler {
    private static final Logger log = LogManager.getLogger(PatientExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(PatientNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestException patientNotFoundExceptionHandler(PatientNotFoundException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());


        return new RestException(HttpStatus.NOT_FOUND.value(), "patient_not_found", "Required patient not found.");
    }

    @ResponseBody
    @ExceptionHandler(PatientAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public RestException patientAlreadyExistsExceptionHandler(PatientAlreadyExistsException ex) {
        if (log.isDebugEnabled()) log.debug(ex.getMessage(), ex);
        else log.warn(ex.getMessage());

        return new RestException(HttpStatus.CONFLICT.value(), "patient_already_exists", "Patient with given data already exists.");
    }

}
