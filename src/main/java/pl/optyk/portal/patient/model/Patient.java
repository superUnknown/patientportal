package pl.optyk.portal.patient.model;

import org.hibernate.annotations.Type;
import pl.optyk.portal.examination.model.Examination;
import pl.optyk.portal.user.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * Encja pacjenta.
 * Created by superUnknown on 07.10.2016.
 */
@Entity
@Table(name = "Patient")
@NamedQueries({
        @NamedQuery(
                name = Patient.CHECK_IF_PATIENT_EXISTS,
                query = "SELECT p FROM Patient p " +
                        "   JOIN p.address a " +
                        "WHERE " +
                        "   p.patientId != :patientId AND " +
                        "   p.firstname = :firstname AND " +
                        "   p.surname = :surname AND " +
                        "   DATE(p.dateOfBirth) = DATE(:dateOfBirth)"
        ),
        @NamedQuery(
                name = Patient.FIND_PATIENTS,
                query = "SELECT p FROM Patient p " +
                        "WHERE " +
                        "   (:query IS NULL AND p IS NOT NULL) OR (:query IS NOT NULL AND (" +
                        "       CAST(p.patientId as string) LIKE :query OR " +
                        "       CONCAT(p.firstname, p.surname) LIKE :query OR " +
                        "       CONCAT(p.surname, p.firstname) LIKE :query OR " +
                        "       p.personalNumber LIKE :query " +
                        "   )) " +
                        "ORDER BY p.surname ASC"
        ),
        @NamedQuery(
                name = Patient.FIND_PATIENTS_COUNT,
                query = "SELECT COUNT(*) FROM Patient p " +
                        "WHERE " +
                        "   (:query IS NULL AND p IS NOT NULL) OR (:query IS NOT NULL AND (" +
                        "       CAST(p.patientId as string) LIKE :query OR " +
                        "       CONCAT(p.firstname, p.surname) LIKE :query OR " +
                        "       CONCAT(p.surname, p.firstname) LIKE :query OR " +
                        "       p.personalNumber LIKE :query " +
                        "   )) " +
                        "ORDER BY p.surname ASC"
        ),
        @NamedQuery(
                name = Patient.FIND_PATIENT_FOR_IMPORT_OPERATION,
                query = "SELECT p FROM Patient p " +
                        "WHERE " +
                        "   p.firstname = :firstname AND " +
                        "   p.surname = :surname AND " +
                        "   DATE(p.dateOfBirth) = DATE(:dateOfBirth)"
        )
})
public class Patient {

    public static final String CHECK_IF_PATIENT_EXISTS = "CHECK_IF_PATIENT_EXISTS";
    public static final String FIND_PATIENTS = "FIND_PATIENTS";
    public static final String FIND_PATIENTS_COUNT = "FIND_PATIENTS_COUNT";
    public static final String FIND_PATIENT_FOR_IMPORT_OPERATION = "FIND_PATIENT_FOR_IMPORT_OPERATION";

    private UUID patientId = UUID.randomUUID();

    private String firstname;

    private String surname;

    private String personalNumber;

    private Date dateOfBirth;

    private String email;

    private String phone;

    private Date created;

    private User createdBy;

    private Date modified;

    private User modifiedBy;

    private Representative representative;

    private Address address = new Address();

    private List<Examination> examinations = new ArrayList<>();

    @Id
    @Type(type = "uuid-char")
    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @ManyToOne
    @JoinColumn(name = "createdBy")
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @ManyToOne
    @JoinColumn(name = "modifiedBy")
    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @JoinColumn(name = "representativeId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Representative getRepresentative() {
        return representative;
    }

    public void setRepresentative(Representative representative) {
        this.representative = representative;
    }

    @JoinColumn(name = "addressId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<Examination> getExaminations() {
        return examinations;
    }

    public void setExaminations(List<Examination> examinations) {
        this.examinations = examinations;
    }

}
