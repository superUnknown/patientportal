package pl.optyk.portal.patient.model;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "Representative")
public class Representative {

    private UUID representativeId = UUID.randomUUID();

    private String firstname;

    private String surname;

    private Address address = new Address();

    @Id
    @Type(type = "uuid-char")
    public UUID getRepresentativeId() {
        return representativeId;
    }

    public void setRepresentativeId(UUID representativeId) {
        this.representativeId = representativeId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JoinColumn(name = "addressId")
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}