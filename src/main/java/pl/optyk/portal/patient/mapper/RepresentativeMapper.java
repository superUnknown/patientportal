package pl.optyk.portal.patient.mapper;


import pl.optyk.portal.patient.dto.AddressDto;
import pl.optyk.portal.patient.dto.RepresentativeDto;
import pl.optyk.portal.patient.model.Representative;

/**
 * Mapper dla danych pacjenta.
 */
public class RepresentativeMapper {

    public static RepresentativeDto entity2dto(Representative entity, RepresentativeDto dto) {
        if (entity == null) {
            return null;
        }
        dto.setFirstname(entity.getFirstname());
        dto.setSurname(entity.getSurname());
        dto.setAddress(AddressMapper.entity2Dto(entity.getAddress(), new AddressDto()));
        return dto;
    }

    public static Representative dto2entity(RepresentativeDto dto, Representative entity) {
        if (dto == null) {
            return null;
        }
        entity.setFirstname(dto.getFirstname());
        entity.setSurname(dto.getSurname());
        entity.setAddress(AddressMapper.dto2entity(dto.getAddress(), entity.getAddress()));
        return entity;
    }

}
