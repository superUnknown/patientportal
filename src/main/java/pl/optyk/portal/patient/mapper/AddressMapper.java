package pl.optyk.portal.patient.mapper;


import pl.optyk.portal.patient.dto.AddressDto;
import pl.optyk.portal.patient.model.Address;

/**
 * Mapper dla danych adresu.
 */
public class AddressMapper {

    public static Address dto2entity(AddressDto dto, Address entity) {
        if (dto == null) {
            return null;
        }

        entity.setCity(dto.getCity());
        entity.setCountry(dto.getCountry());
        entity.setHouseNumber(dto.getHouseNumber());
        entity.setPostalCode(dto.getPostalCode());
        entity.setStreet(dto.getStreet());

        return entity;
    }

    public static AddressDto entity2Dto(Address entity, AddressDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setCity(entity.getCity());
        dto.setCountry(entity.getCountry());
        dto.setHouseNumber(entity.getHouseNumber());
        dto.setPostalCode(entity.getPostalCode());
        dto.setStreet(entity.getStreet());

        return dto;
    }

}
