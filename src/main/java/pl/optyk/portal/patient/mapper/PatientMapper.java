package pl.optyk.portal.patient.mapper;


import pl.optyk.portal.common.utils.DateUtils;
import pl.optyk.portal.patient.dto.AddressDto;
import pl.optyk.portal.patient.dto.PatientDto;
import pl.optyk.portal.patient.dto.PatientSaveDto;
import pl.optyk.portal.patient.dto.RepresentativeDto;
import pl.optyk.portal.patient.model.Patient;
import pl.optyk.portal.patient.model.Representative;

/**
 * Mapper dla danych pacjenta.
 */
public class PatientMapper {

    public static PatientDto entity2dto(Patient entity, PatientDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setPatientId(entity.getPatientId());
        dto.setFirstname(entity.getFirstname());
        dto.setSurname(entity.getSurname());
        dto.setPersonalNumber(entity.getPersonalNumber());
        dto.setCreated(entity.getCreated());
        dto.setModified(entity.getModified());
        dto.setDateOfBirth(entity.getDateOfBirth());
        dto.setEmail(entity.getEmail());
        dto.setPhone(entity.getPhone());
        dto.setCreatedBy(entity.getCreatedBy().getUsername());
        dto.setRepresentative(RepresentativeMapper.entity2dto(entity.getRepresentative(), new RepresentativeDto()));
        dto.setAddress(AddressMapper.entity2Dto(entity.getAddress(), new AddressDto()));
        dto.setModifiedBy(entity.getModifiedBy() != null ? entity.getModifiedBy().getUsername() : null);

        return dto;
    }

    public static Patient dto2entity(PatientSaveDto dto, Patient entity) {
        if (dto == null) {
            return null;
        }

        entity.setFirstname(dto.getFirstname());
        entity.setSurname(dto.getSurname());
        entity.setPersonalNumber(dto.getPersonalNumber());
        entity.setDateOfBirth(DateUtils.withoutTime(dto.getDateOfBirth()));
        entity.setEmail(dto.getEmail());
        entity.setPhone(dto.getPhone());
        entity.setRepresentative(RepresentativeMapper.dto2entity(dto.getRepresentative(), new Representative()));
        entity.setAddress(AddressMapper.dto2entity(dto.getAddress(), entity.getAddress()));

        return entity;
    }

}
