package pl.optyk.portal.patient.dto;

import java.util.Date;

/**
 * Dto z danymi do dodania nowego pacjenta oraz do modyfikacji instniejącego.
 * Created by superUnknown on 07.10.2016.
 */
public class PatientSaveDto {

    private String firstname;

    private String surname;

    private String personalNumber;

    private Date dateOfBirth;

    private String email;

    private String phone;

    private RepresentativeDto representative;

    private AddressDto address = new AddressDto();

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public RepresentativeDto getRepresentative() {
        return representative;
    }

    public void setRepresentative(RepresentativeDto representative) {
        this.representative = representative;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PatientSaveDto{");
        sb.append("firstname='").append(firstname).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", personalNumber='").append(personalNumber).append('\'');
        sb.append(", dateOfBirth=").append(dateOfBirth);
        sb.append(", email='").append(email).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", representative=").append(representative);
        sb.append(", address=").append(address);
        sb.append('}');
        return sb.toString();
    }

}
