package pl.optyk.portal.patient.dto;

public class RepresentativeDto {

    private String firstname;

    private String surname;

    private AddressDto address;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "RepresentativeDto{" +
                "firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                '}';
    }

}